const settings = {
  "name": "call-page",
  "state": {
    "frontity": {
      "url": "https://wp.callpage.pl",
      "title": "CallPage | Widget kontaktowy - CallPage®",
      "description": "Some text here",
    }
  },
  "packages": [
    {
      "name": "call-page",
    },
    {
      "name": "@frontity/wp-source",
      "state": {
        "source": {
          "url": "https://wp.callpage.pl",
          params: {
            per_page: 9,
          },
          "postTypes": [
            {
              type: "blog",
              endpoint: "blog",
              archive: "/blog",
            },
            {
              type: "wiedza",
              endpoint: "wiedza",
              archive: "/wiedza",
            },
            {
              type: "casestudies",
              endpoint: "casestudies",
              archive: "/casestudies"
            },
            {
              type: "campaigns",
              endpoint: "campaigns",
              archive: "/campaigns"
            },
            {
              type: "branze",
              endpoint: "branze",
              archive: "/branze"
            }
          ],
          taxonomies: [
            {
              taxonomy: "category_casestudies",
              endpoint: "category_casestudies",
              postTypeEndpoint: "casestudies",
              params: {
                per_page: 10,
                _embed: true,
              },
            },
            {
              taxonomy: "category_campaigns",
              endpoint: "category_campaigns",
              postTypeEndpoint: "campaigns",
              params: {
                per_page: 10,
                _embed: true,
              },
            },
            {
              taxonomy: "category_blog",
              endpoint: "category_blog",
              postTypeEndpoint: "blog",
              params: {
                per_page: 10,
                _embed: true,
              },
            },
          ]
        }
      },
    },
    {
      name: "@frontity/google-tag-manager-analytics",
      state: {
        googleTagManagerAnalytics: {
          containerIds: ["GTM-PPFXRVZ"],
        },
      },
    },
    // {
    //   name: "@frontity/google-analytics",
    //   state: {
    //     googleAnalytics: {
    //       trackingId: "UA-202531233-1",
    //     },
    //   },
    // },
    "@frontity/tiny-router",
    "@frontity/html2react"
  ]
};

export default settings;
