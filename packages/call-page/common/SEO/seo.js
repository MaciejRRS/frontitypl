import React from 'react'
import { Head } from "frontity";

const SEO = (props) => {
    return (
        <>
            {
                props.seo
                    ? <Head>
                        <title>{props.seo.title}</title>
                        <meta name="description" content={props.seo.description} />

                        <meta http-equiv="Last-Modified" content={props.date} />

                        <meta property="og:description" content={props.seo.og_description} />
                        <meta property="og:type" content={props.seo.og_type} />
                        <meta property="og:title" content={props.seo.og_title} />
                        <meta property="og:image" content={props.seo.og_image} />
                        <meta property="og:image:width" content={props.seo.og_image_width} />
                        <meta property="og:image:height" content={props.seo.og_image_height} />
                        <meta property="og:url" content={props.seo.og_url} />

                        <link rel="alternate" hreflang="pl" href={props.seo.hreflang_pl} />
                        <link rel="alternate" hreflang="en" href={props.seo.hreflang_en} />
                        <link rel="alternate" hreflang="x-default" href={props.seo.hreflang_default} />

                        {props.seo.canonical
                            ? <link rel="canonical" href={props.seo.canonical} />
                            : null
                        }


                        {props.seo.twitter_description
                            ? <meta name="twitter:description" content={props.seo.twitter_description} />
                            : props.seo.og_description
                                ? <meta name="twitter:description" content={props.seo.og_description} />
                                : <meta name="twitter:description" content={props.seo.description} />
                        }
                        {props.seo.twitter_title
                            ? <meta name="twitter:title" content={props.seo.twitter_title} />
                            : props.seo.og_description
                                ? <meta name="twitter:title" content={props.seo.og_title} />
                                : <meta name="twitter:title" content={props.seo.title} />
                        }
                        {props.seo.image_twitter
                            ? <meta name="twitter:image" content={props.seo.image_twitter} />
                            : props.seo.og_image
                                ? <meta name="twitter:image" content={props.seo.og_image} />
                                : null
                        }

                        <script type="application/ld+json">
                            {
                            `
                                {
                                "@context": "https://schema.org/",
                                "@type": "WebSite",
                                "name": "Callpage",
                                "url": "https://www.callpage.pl/",
                                "potentialAction": {
                                    "@type": "SearchAction",
                                    "target": "{search_term_string}",
                                    "query-input": "required name=search_term_string"
                                }
                                }
                            `
                            }
                        </script>

                    </Head>
                    : null
            }
        </>
    )
}

export default SEO