import React from 'react'
import { styled } from "frontity"
import { Container, Text } from '../styles'

const Article = styled.article`
    padding-top: ${props => props.marginDesktop ? props.marginDesktop : 'clamp(140px, 12vw, 240px);'};
    @media(max-width: 1198px){
        padding-top: ${props => props.marginTablet ? props.marginTablet : 'clamp(90px, 12vw, 140px);'};
    }
    @media(max-width: 764px){
        padding-top: ${props => props.marginPhone ? props.marginPhone : '90px'};
    }
`

const Title = styled.h2`
    max-width: 1082px;
    width: 100%;
    margin: 0 auto;
    text-align: center;
    font-size: 45px;
    line-height: 60px;
    font-weight: bold;
    @media(max-width: 1198px){
        font-size: 28px;
        line-height: 40px;
    }
    @media(max-width: 764px){
        font-size: 22px;
        line-height: 30px;
    }
`

const LocText = styled(Text)`
    max-width: 1082px;
    width: 100%;
    text-align: center;
    font-size: 28px;
    line-height: 40px;
    margin: 40px auto 60px;
    @media(max-width: 1198px){
        font-size: 22px;
        line-height: 30px;
        margin: 30px auto 50px;
    }
    @media(max-width: 764px){
        font-size: 18px;
        line-height: 28px;
        margin: 20px auto 30px;
    }
`

const Img = styled.img`
    max-width: 1082px;
    width: 100%;
    margin: 0 auto;
    display: block;
`

const BestAlternative = (props) => {
    return (
        <Article marginDesktop={props.marginDesktop} marginTablet={props.marginTablet} marginPhone={props.marginPhone}>
            <Container>
                <Title>{props.acf.title}</Title>
                <LocText>{props.acf.text}</LocText>
                <Img alt={props.acf.img_alt} src={props.acf.img} />
            </Container>
        </Article>
    )
}

export default BestAlternative