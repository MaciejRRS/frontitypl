import React from 'react'
import { styled } from "frontity"
import { Container } from '../styles'

const Article = styled.article`
    padding-top: ${props => props.marginDesktop ? props.marginDesktop : 'clamp(140px, 12vw, 240px);'};
    @media(max-width: 1198px){
        padding-top: ${props => props.marginTablet ? props.marginTablet : 'clamp(90px, 12vw, 140px);'};
    }
    @media(max-width: 764px){
        padding-top: ${props => props.marginPhone ? props.marginPhone : '90px'};
    }
`

const Title = styled.h2`
    text-align: center;
    font-size: 45px;
    font-weight: bold;
    line-height: 60px;
    margin: 0 auto 110px;
    max-width: 1080px;
    @media(max-width: 1198px){
        margin-bottom: 90px;
    }
    @media(max-width: 764px){
        font-size: 28px;
        line-height: 40px;
        margin-bottom: 60px;
    }
`

const Grid = styled.div`
    display: grid;
    grid-template-columns: 1fr 1fr 1fr;
    grid-column-gap: 30px;
    grid-row-gap: 30px;
    @media(max-width: 1198px){
        grid-template-columns: 1fr;
    }
`

const BlocksList = styled.div`
    min-height: 400px;
    box-shadow: 0 3px 6px #00000016;
    border-radius: 16px;
    padding: 15px 30px 30px;
    @media(max-width: 1198px){
       max-width: 507px;
       width: 100%;
       margin: 0 auto;
       box-sizing: border-box;
    }
`

const Img = styled.img`
    margin-right: 30px;
    @media(max-width: 1198px){
        width: 90px;
        height: 90px;
    }
`

const FlexTitleIcon = styled.div`
    display: flex;
    align-items: center;
    margin-bottom: 30px;
`

const ListTitle = styled.h3`
    font-size: 28px;
    font-weight: bold;
    line-height: 40px;
    @media(max-width: 1198px){
        font-size: 22px;
        line-height: 30px;
    }
`

const ListText = styled.p`
    color: #6E7276;
    font-size: 16px;
    line-height: 24px;
    margin-bottom: 30px;
    @media(max-width: 764px){
        font-size: 14px;
        line-height: 24px;
    }
`

const TitleRepeater = styled.h4`
    font-size: 18px;
    font-weight: bold;
    line-height: 28px;
    margin-bottom: 15px;
`

const ListItem = styled.li`
    padding-left: 30px;
    font-size: 16px;
    line-height: 24px;
    margin-bottom: 18px;
    position: relative;
    color: #6E7276;
    @media(max-width: 764px){
        font-size: 14px;
        line-height: 24px;
    }
    &::before{
        content: "•";
        position: absolute;
        left: 0;
        color: #377DFF;
        font-size: 28px;
        font-weight: bold;
        line-height: 24px;
    }
`

const CustomerMotivation = (props) => {
    return (
        <Article marginDesktop={props.marginDesktop} marginTablet={props.marginTablet} marginPhone={props.marginPhone}>
            <Container>
                <Title>{props.acf.title}</Title>
                <Grid>
                    {props.acf.repeater.map((el, index) =>
                        <BlocksList key={index}>
                            <FlexTitleIcon>
                                <Img alt={el.img_alt} src={el.img} />
                                <ListTitle>{el.title}</ListTitle>
                            </FlexTitleIcon>
                            <ListText>{el.text}</ListText>
                            <TitleRepeater>{el.title_repeater}</TitleRepeater>

                            <ul>
                                {
                                    el.repeater.map((innerEl, innerIndex) =>
                                        <ListItem key={innerIndex}>{innerEl.text}</ListItem>
                                    )
                                }
                            </ul>
                        </BlocksList>
                    )}
                </Grid>
            </Container>
        </Article>
    )
}

export default CustomerMotivation