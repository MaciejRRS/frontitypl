import React from 'react'
import { styled } from "frontity"
import { Container } from '../styles'
import Background from '../sprites/developBG.png'

const Article = styled.article`
    position: relative;
    padding-top: ${props => props.marginDesktop ? props.marginDesktop : 'clamp(140px, 12vw, 240px);'};
    margin-bottom: ${props => props.marginBottomDesktop};
    @media(max-width: 1198px){
        padding-top: ${props => props.marginTablet ? props.marginTablet : 'clamp(90px, 12vw, 140px);'};
        margin-bottom: ${props => props.marginBottomTablet};
    }
    @media(max-width: 764px){
        padding-top: ${props => props.marginPhone ? props.marginPhone : '90px'};
        margin-bottom: ${props => props.marginBottomPhone};
    }
`

const BgArticle = styled.div`
    position: relative;
    z-index: 10;
    background-image: url(${Background});
    background-position: bottom;
    background-size: contain;
    background-repeat: no-repeat;
    width: 100%;
    padding-bottom: 30px;

    @media(max-width: 1198px){
        background-size: 0;
    }
`

const TextWrapper = styled.div`
    width: 655px;
    padding: 45px 0;
    margin-left: 30px;
    background-color: transparent;
    position: relative;
    color: #000;
    background-color: #fff;
    box-sizing: border-box;
    padding: 45px 90px;
    box-shadow: 0 3px 6px #00000016;

    p.title{
        padding-top: 22px;
        font-size: 22px;
        line-height: 30px;
        font-weight: bold;
    }

    &::before{
        position: absolute;
        content: '„';
        top: 30px;
        left: 40px;
        font-size: 60px;
        font-weight: bold;
        color: #FFE04F;
    }


    @media(max-width: 1198px){  
        width: 500px;
        background-color: #fff;
        box-sizing: border-box;
        padding: 45px 90px 80px;
        display: flex;
        flex-direction: column;



        &::before{
            top: 0px;
            left: 40px;
        }

        &::after{
            bottom: 50px;
            right: 60px;
        }

        p{
            font-size: 16px;
            line-height: 24px;
        }
    }

    @media(max-width: 680px){  
        width: 100%;
        margin: 0;

        p{
            font-size: 14px;
            line-height: 20px;
        }
    }

    @media(max-width: 460px){  
        padding: 45px 30px 45px;
        &::before{
            top: -10px;
            left: 20px;
        }
    }
`

const Flex = styled.div`
    display: flex;
    justify-content: space-evenly;
    flex-direction: row;
    align-items: center;
    position: relative;
    z-index: 10;
    @media(max-width: 1198px){  
        flex-direction: row;
        justify-content: flex-start;
    }
`

const Img = styled.img`
    @media(max-width: 1198px){  
        position: absolute;
        transform: translate(125%, 65%);
        max-width: 300px;
        bottom: -15px;
    }
    @media(max-width: 764px){  
        max-width: 240px;
        bottom: 0;
        right: 0;
        transform: translate(0, 60%);
    }
    @media(max-width: 460px){  
        bottom: 0;
        right: 0;
        left: 50%;
        transform: translate(-50%, 80%);
    }
`

const Title = styled.h2`
    font-size: clamp(45px, 3.5vw, 60px);
    line-height: clamp(60px, 3.5vw, 80px);
    text-align: center;
    font-weight: bold;
    margin-bottom: 120px;

    @media(max-width: 1198px) {
        max-width: 510px;
        font-size: 45px;
        line-height: 60px;
        margin: 0 auto 80px;
    }

    @media(max-width: 768px) {
        max-width: 100%;
        font-size: 28px;
        line-height: 40px;
        margin: 0 auto 55px;
    }
`

const Develope = (props) => {
    return (
        <Article marginBottomDesktop={props.marginBottomDesktop} marginBottomTablet={props.marginBottomTablet} marginBottomPhone={props.marginBottomPhone} marginDesktop={props.marginDesktop} marginTablet={props.marginTablet} marginPhone={props.marginPhone}>
            <Container>
                {props.acf.title
                    ? <Title>{props.acf.title}</Title>
                    : null
                }
            </Container>
            <BgArticle>
                <Container>
                    <Flex>
                        <TextWrapper>
                            <p>{props.acf.quote}</p>
                            <p className='title'>{props.acf.quote_author}</p>
                        </TextWrapper>
                        <Img alt={props.acf.img_alt} src={props.acf.img} />
                    </Flex>
                </Container>
            </BgArticle>
        </Article>
    )
}
export default Develope