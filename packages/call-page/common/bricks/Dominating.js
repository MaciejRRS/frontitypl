import React from 'react'
import { styled } from "frontity"
import { Container } from '../styles'
import Background from '../sprites/solve_bg.png'

const Article = styled.article`
    position: relative;
    padding-top: ${props => props.marginDesktop ? props.marginDesktop : 'clamp(140px, 12vw, 240px);'};
    @media(max-width: 1198px){
        padding-top: ${props => props.marginTablet ? props.marginTablet : 'clamp(90px, 12vw, 140px);'};
    }
    @media(max-width: 764px){
        padding-top: ${props => props.marginPhone ? props.marginPhone : '90px'};
    }
`

const BottomBG = styled.div`
    position: absolute;
    background-image: url(${Background});
    transform: translateY(30px);
    background-repeat: no-repeat;
    background-size: cover;
    width: 100%;
    height: 350px;
    bottom: 0;
    left: 0;
    z-index: 0;
    @media(max-width: 1398px){
        display: none;
    }
`

const Title = styled.h2`
    text-align: center;
    font-size: clamp(45px, 3.5vw, 60px);
    line-height: clamp(60px, 3.5vw, 80px);
    font-weight: bold;
    margin: 0 auto 48px;
    max-width: 1082px;
    @media(max-width: 1198px){
        font-size: 45px;
        line-height: 60px;
    }
    @media(max-width: 764px){
        font-size: 28px;
        line-height: 40px;
    }
`

const Subtitle = styled.p`
    text-align: center;
    margin-bottom: 120px;
    color: #6E7276;
    font-size: 28px;
    line-height: 40px;
    @media(max-width: 1198px){
        font-size: 22px;
        line-height: 30px;
    }
    @media(max-width: 764px){
        font-size: 18px;
        line-height: 28px;
    }
`

const Grid = styled.div`
    display: grid;
    grid-template-columns: 1fr 1fr 1fr;
    grid-column-gap: 30px;
    grid-row-gap: 30px;
    @media(max-width: 1398px){
        grid-template-columns: 1fr;
    }
`

const BlockWrap = styled.div`
    box-shadow: 0 3px 6px #00000016;
    border-radius: 12px;
    padding: 15px 30px;
    background: #FFFFFF;
    @media(max-width: 1398px){
        max-width: 807px;
        width: 100%;
        margin: 0 auto;
        box-sizing: border-box;
    }
    @media(max-width: 998px){
        max-width: 507px;
    }
`

const HeadBlock = styled.div`
    display: flex;
    align-items: center;
    margin-bottom: 30px;
`

const TitleBlock = styled.div`
    font-size: 28px;
    font-weight: bold;
    line-height: 40px;
    @media(max-width: 1198px){
        font-size: 22px;
        line-height: 30px;
    }
`

const Img = styled.img`
    margin-right: 30px;
    width: 120px;
    height: 120px;
    @media(max-width: 1198px){
        width: 90px;
        height: 90px;
    }
    @media(max-width: 764px){
        width: 60px;
        height: 60px;
    }
`

const TitleRepeater = styled.h3`
    margin-bottom: 15px;
`

const List = styled.ul`
    margin-bottom: 30px;
`

const ListItem = styled.li`
    padding-left: 25px;
    font-size: 16px;
    line-height: 24px;
    margin-bottom: 15px;
    position: relative;
    color: #6E7276;
    @media(max-width: 764px){
        font-size: 14px;
    }
    &::before{
        content: "•";
        position: absolute;
        left: 0;
        color: #FFE04F;
        font-size: 18px;
        font-weight: 900;
        line-height: 24px;
    }
`

const Dominating = (props) => {
    return (
        <Article marginDesktop={props.marginDesktop} marginTablet={props.marginTablet} marginPhone={props.marginPhone}>
            <Container>
                <Title>{props.acf.title}</Title>
                <Subtitle>{props.acf.subtitle}</Subtitle>
                <Grid>
                    {
                        props.acf.repeater.map((el, index) =>
                            <BlockWrap key={index}>
                                <HeadBlock>
                                    <Img alt={el.icon_alt} src={el.icon} />
                                    <TitleBlock>{el.title}</TitleBlock>
                                </HeadBlock>
                                <TitleRepeater>{el.text}</TitleRepeater>
                                {
                                    el.repeater.map((innerEl, innerIndex) =>
                                        <>
                                            <TitleRepeater>{innerEl.title}</TitleRepeater>
                                            <List key={innerIndex}>
                                                {

                                                    innerEl.repeater.map((innerElin, innerIndexin) =>
                                                        <ListItem key={innerIndexin}>{innerElin.text}</ListItem>
                                                    )

                                                }
                                            </List>
                                        </>
                                    )
                                }
                            </BlockWrap>
                        )
                    }
                </Grid>
            </Container>
            <BottomBG></BottomBG>
        </Article>
    )
}

export default Dominating