import React from 'react'
import { styled } from "frontity"
import { Container } from '../styles'
import Background from '../sprites/bg_features.png'

const Article = styled.article`
    position: relative;
    margin-top: ${props => props.marginDesktop ? props.marginDesktop : 'clamp(140px, 12vw, 240px);'};
    @media(max-width: 1198px){
        margin-top: ${props => props.marginTablet ? props.marginTablet : 'clamp(90px, 12vw, 140px);'};
    }
    @media(max-width: 764px){
        margin-top: ${props => props.marginPhone ? props.marginPhone : '90px'};
    }
`

const Title = styled.h2`
    text-align: center;
    font-size: 45px;
    font-weight: bold;
    line-height: 60px;
    margin-bottom: 195px;

    @media(max-width: 1199px) {
        max-width: 510px;
        margin: 0 auto 110px;
    }

    @media(max-width: 764px) {
        max-width: 100%;
        margin: 0 auto 85px;
        font-size: 28px;
        line-height: 40px;
    }
`
const Img = styled.img`
    position: absolute;
    left: 15px;
    top: -30px;
    max-width: 90px;
    height: auto;
`
const List = styled.ul`
    display: grid;
    grid-template-columns: 1fr 1fr 1fr;
    grid-column-gap: 30px;

    @media(max-width: 1199px) {
        grid-template-columns: 1fr 1fr;
        grid-column-gap: 15px;
    }

    @media(max-width: 764px) {
        grid-template-columns: 1fr;
    }
`
const ListItem = styled.li`
    box-sizing:border-box;
    min-height: 215px;
    position: relative;
    box-shadow:0 3px 6px rgba(0, 0, 0, 16%);
    padding: 75px 40px 30px 30px;
    background: #FFFFFF;
    margin-bottom: 60px;
`
const ListTitle = styled.h3`
    font-size: 18px;
    font-weight: bold;
    color: #000000;
    margin-bottom: 15px;
  `
const ListText = styled.p`
    font-size: 16px;
    line-height:24px ;
    color: #6E7276;
`
const BottomBG = styled.div`
    position: absolute;
    background-image: url(${Background});
    background-repeat: no-repeat;
    background-size: cover;
    width: 100%;
    height: 305px;
    bottom: 30%;
    left: 0;
    z-index: -1;
    @media(max-width: 1199px) {
        display: none;
    }
`
const Features = (props) => {
    return (
        <Article marginDesktop={props.marginDesktop} marginTablet={props.marginTablet} marginPhone={props.marginPhone}>
            <Container>
                <Title>{props.acf.title}</Title>
                <List>
                    {
                        props.acf.repeater.map((el, index) =>
                            <ListItem key={index}>
                                <Img alt={el.icon_alt} src={el.icon} />
                                <ListTitle>{el.title}</ListTitle>
                                <ListText>{el.text}</ListText>
                            </ListItem>
                        )
                    }
                </List>
            </Container>
            <BottomBG></BottomBG>
        </Article>
    )
}
export default Features