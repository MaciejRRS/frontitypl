import React from 'react'
import { styled } from "frontity"
import { Container } from '../styles'

const Article = styled.article`
    position: relative;
    padding-top: ${props => props.marginDesktop ? props.marginDesktop : 'clamp(140px, 12vw, 240px);'};
    @media(max-width: 1198px){
        padding-top: ${props => props.marginTablet ? props.marginTablet : 'clamp(90px, 12vw, 140px);'};
    }
    @media(max-width: 764px){
        padding-top: ${props => props.marginPhone ? props.marginPhone : '90px'};
    }
`

const Title = styled.h2`
    text-align: center;
    font-size: clamp(45px, 3.5vw, 60px);
    line-height: clamp(60px, 3.5vw, 80px);
    font-weight: bold;
    max-width:800px;
    margin: 0 auto 120px;
    @media(max-width: 1198px){
        font-size: 45px;
        line-height: 60px;
        margin-bottom: 60px;
    }
    @media(max-width: 764px){
        font-size: 28px;
        line-height: 40px;
        margin-bottom: 40px;
    }
  `

const Flex = styled.div`
    display: flex;
    justify-content: space-evenly;
    @media(max-width: 1198px){
        flex-direction: column-reverse;
    }
`

const ColumnLeft = styled.div`
    max-width: 665px;
    margin-right: 30px;
    @media(max-width: 1198px){
        max-width: 507px;
        margin: 0 auto;
        width: 100%;
    }
`

const ColumnRight = styled.div`
    max-width: 507px;
    @media(max-width: 1198px){
        margin: 0 auto;
        width: 100%;

        img{
            width: 100%;
        }
    }

    img {
        max-height: 480px;
        max-width: 100%;
    }
`

const Subtitle = styled.h3`
    font-size: 28px;
    font-weight: bold;
    margin-bottom: 30px;
    @media(max-width: 1198px){
        margin-top: 30px;
        font-size: 22px;
        line-height: 30px;
    }
    @media(max-width: 764px){
        font-size: 18px;
        line-height: 28px;
    }
`
const List = styled.ul`

`

const ListItem = styled.li`
    padding-left: 40px;
    font-size: 22px;
    line-height: 30px;
    margin-bottom: 30px;
    position: relative;
    @media(max-width: 1198px){
        font-size: 16px;
        line-height: 24px;
    }
    @media(max-width: 764px){
        font-size: 14px;
        line-height: 20px;
    }
    &::before{
        content: "•";
        position: absolute;
        left: 0;
        color: #FFE04F;
        font-size: 22px;
        font-weight: 900;
        line-height: 30px;
    }
`
const Img = styled.img`

`



const Mixes = (props) => {
    return (
        <Article marginDesktop={props.marginDesktop} marginTablet={props.marginTablet} marginPhone={props.marginPhone}>
            <Container>
                <Title>{props.acf.title}</Title>
                <Flex>
                    <ColumnLeft>
                        <Subtitle>{props.acf.subtitle}</Subtitle>
                        <List>
                            {
                                props.acf.repeater.map((el, index) =>
                                    <ListItem key={index}>{el.text}</ListItem>
                                )
                            }
                        </List>
                    </ColumnLeft>
                    <ColumnRight>
                        <Img alt={props.acf.img_alt} src={props.acf.img} />
                    </ColumnRight>
                </Flex>
            </Container>
        </Article>
    )
}

export default Mixes