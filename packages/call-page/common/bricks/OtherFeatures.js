import React from 'react'
import { styled } from "frontity"
import { Container } from '../styles'

const Article = styled.article`
    position: relative;
    padding-top: ${props => props.marginDesktop ? props.marginDesktop : 'clamp(140px, 12vw, 240px);'};
    @media(max-width: 1198px){
        padding-top: ${props => props.marginTablet ? props.marginTablet : 'clamp(90px, 12vw, 140px);'};
    }
    @media(max-width: 764px){
        padding-top: ${props => props.marginPhone ? props.marginPhone : '90px'};
    }
`

const LocContainer = styled(Container)`
    max-width: 1082px;
`

const Title = styled.h2`
    text-align: center;
    font-size: clamp(45px, 3.5vw, 60px);
    line-height: clamp(60px, 3.5vw, 80px);
    font-weight: bold;
    padding: 0 0 60px;
    @media(max-width: 1198px){
        font-size: 45px;
        line-height: 60px;
        padding-bottom: 0;
    }
    @media(max-width: 764px){
        font-size: 28px;
        line-height: 40px;
    }
`

const TitleGrid = styled.div`
    display: grid;
    grid-template-columns: 2fr 1fr 1fr;
    grid-column-gap: 30px;
    position: relative;
    @media(max-width: 1198px){
        grid-template-columns: 4fr 1fr 1fr;   
    }
`

const TitleCell = styled.div` 
    width: 100%;
    font-size: 28px;
    color: ${props => props.alt ? '#000' : '#fff'};
    font-weight: bold;
    line-height: 60px;
    background-color: ${props => props.alt ? 'transparent' : '#377DFF'};
    border: 2px solid #377DFF;
    text-align: center;
    border-radius: 12px;
    box-shadow: 0 3px 6px 0 #D7E5FF;
    @media(max-width: 1198px){
        display: ${props => props.alt ? 'none' : 'block'};
        background: transparent;
        border: unset;
        color: #377DFF;
        box-shadow: unset;
        text-align: left;
        font-size: 28px;
        line-height: 40px;
        padding-left: 30px;
        padding: 30px 0;
    }

`

const ImgTitleCell = styled.div`
    display: none;
    @media(max-width: 1198px){
        display: flex;
        width: 100%;
        height: 100%;
        align-items: center;
        img{
            max-width: 103px;
            min-width: 51px;
            width: 100%;
        }
    }
`

const Grid = styled(TitleGrid)`
    border-radius: 12px;
    box-shadow: 0 3px 6px 0 #E4E7EA;
    margin-top: 30px;
    padding: 30px;
    box-sizing: border-box;
    @media(max-width: 1198px){
        margin-top: 0;
        padding: 15px;
    }
`

const True = styled.span`
    font-size: 22px;
    font-weight: bold;
    line-height: 60px;
    color: #377DFF;
    text-align: center;
    @media(max-width: 1198px){
        font-size: 18px;
        line-height: 45px;
    }
    @media(max-width: 764px){
        font-size: 16px;
        line-height: 28px;
        padding: 5px 0;
    }
`

const False = styled.span`
    font-size: 22px;
    font-weight: bold;
    line-height: 60px;
    color: #707070;
    text-align: center;
    @media(max-width: 1198px){
        font-size: 18px;
        line-height: 45px;
    }
    @media(max-width: 764px){
        font-size: 16px;
        line-height: 28px;
        padding: 5px 0;
    }
`

const Cell = styled.div`
    text-align: center;
    @media(max-width: 764px){
        padding: 0 0 5px 0;
    }
`

const NameCell = styled.div`
    font-size: 22px;
    line-height: 60px;
    font-weight: bold;
    @media(max-width: 1198px){
        font-size: 16px;
        line-height: 45px;
    }
    @media(max-width: 764px){
        font-size: 14px;
        line-height: 24px;
        padding: 5px 0;
    }
`

const OtherFeatures = (props) => {
    return (
        <Article marginDesktop={props.marginDesktop} marginTablet={props.marginTablet} marginPhone={props.marginPhone}>
            <LocContainer>
                <Title>{props.acf.title}</Title>
                <TitleGrid>
                    <TitleCell>{props.acf.table_titles.title_1_col}</TitleCell>
                    <TitleCell alt={true}>{props.acf.table_titles.title_2_col}</TitleCell>
                    <TitleCell alt={true}>{props.acf.table_titles.title_3_col}</TitleCell>
                    <ImgTitleCell><img alt='firm logo' src={props.acf.table_titles.img_2_col} /></ImgTitleCell>
                    <ImgTitleCell><img alt='firm logo' src={props.acf.table_titles.img_3_col} /></ImgTitleCell>
                </TitleGrid>
                <Grid>
                    <NameCell>{props.acf.table_pricing.title_1_col}</NameCell>
                    <True>{props.acf.table_pricing.title_2_col}</True>
                    <False>{props.acf.table_pricing.title_3_col}</False>
                    {
                        props.acf.repeater.map((el, index) =>
                            <>
                                <NameCell key={index}>{el.col_1}</NameCell>
                                <Cell>{el.col_2 ? <True>✓</True> : <False>✗</False>}</Cell>
                                <Cell>{el.col_3 ? <False>✓</False> : <False>✗</False>}</Cell>
                            </>
                        )
                    }
                </Grid>
            </LocContainer>
        </Article>
    )
}

export default OtherFeatures