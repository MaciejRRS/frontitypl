import React from 'react'
import { Container, Link, Text } from '../styles'
import { styled } from "frontity"

const Article = styled.article`
    position: relative;
    padding-top: ${props => props.marginDesktop ? props.marginDesktop : 'clamp(140px, 12vw, 240px);'};
    @media(max-width: 1198px){
        padding-top: ${props => props.marginTablet ? props.marginTablet : 'clamp(90px, 12vw, 140px);'};
    }
    @media(max-width: 764px){
        padding-top: ${props => props.marginPhone ? props.marginPhone : '90px'};
    }
`

const LocContainer = styled(Container)`
    max-width: 1082px;
`

const Flex = styled.div`
    display: flex;
    justify-content: space-between;
    @media(max-width: 914px){
        display: grid;
        grid-template-columns: 1fr;
        grid-row-gap: 60px;
    }
`

const Item = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    width: 387px;
    text-align: center;
    position: relative;
    img{
        display: block;
        margin: 0 auto;
        max-width: 387px;
        width: 100%;

    }
    @media(max-width: 914px){
        margin: 0 auto;
        max-width: 387px;
        width: 100%;
    }
`

const Title = styled.h3`
    font-size: 28px;
    line-height: 40px;
    font-weight: bold;
    padding: 50px 0 30px;
    @media(max-width: 1198px){
        font-size: 22px;
        line-height: 30px;
    }
    @media(max-width: 764px){
        padding: 25px 0 15px;
    }
`

const LocText = styled(Text)`
    padding-bottom: 100px;
    @media(max-width: 1198px){
    }
    @media(max-width: 764px){
        font-size: 14px;
        line-height: 20px;
        padding-bottom: 80px;
    }
`

const LocLink = styled(Link)`
    padding: 15px 0;
    width: 100%;
    position: absolute;
    bottom: 0;
    @media(max-width: 1198px){
        font-size: 16px;
        padding: 5px 0;
    }
    @media(max-width: 764px){
    }
`

const PartnersType = (props) => {
    return (
        <Article marginDesktop={props.marginDesktop} marginTablet={props.marginTablet} marginPhone={props.marginPhone}>
            <LocContainer>
                <Flex>
                    {
                        props.acf.repeater.map((el, index) =>
                            <Item key={index}>
                                <img alt={el.img_alt} src={el.img} />
                                <Title>{el.title}</Title>
                                <LocText>{el.text}</LocText>
                                {el.button_type
                                    ? <LocLink link={el.button_link}>{el.button_text}</LocLink>
                                    : <LocLink as='a' rel="noreferrer" target='_blank' href={el.button_link}>{el.button_text}</LocLink>
                                }

                            </Item>
                        )
                    }
                </Flex>
            </LocContainer>
        </Article>
    )
}

export default PartnersType