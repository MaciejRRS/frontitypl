import React from 'react'
import { styled } from "frontity"
import { Container } from '../styles'
import Background from '../sprites/solve_bg.png'

const Article = styled.article`
    position: relative;
    padding-top: ${props => props.marginDesktop ? props.marginDesktop : 'clamp(140px, 12vw, 240px);'};
    @media(max-width: 1198px){
        padding-top: ${props => props.marginTablet ? props.marginTablet : 'clamp(90px, 12vw, 140px);'};
    }
    @media(max-width: 764px){
        padding-top: ${props => props.marginPhone ? props.marginPhone : '90px'};
    }
`

const BottomBG = styled.div`
    position: absolute;
    background-image: url(${Background});
    transform: translateY(90px);
    background-repeat: no-repeat;
    background-size: cover;
    background-position: bottom;
    width: 100%;
    height: 350px;
    bottom: 0;
    left: 0;
    z-index: 0;

    @media(max-width: 1500px){
        height: 300px;
    } 

    @media(max-width: 1320px){
        height: 250px;
    }

    @media(max-width: 1198px){
        display: none;
    }
`

const Title = styled.h2` 
    text-align: center;
    font-size: clamp(45px, 3.5vw, 60px);
    line-height: clamp(60px, 3.5vw, 80px);
    font-weight: bold;
    padding: 0 0 100px;
    @media(max-width: 1198px){
        font-size: 45px;
        line-height: 60px;
    }
    @media(max-width: 764px){
        font-size: 28px;
        line-height: 40px;
    }
`

const Item = styled.div`
    img{
        width: 100%;
    }
    @media(max-width: 1750px){
        img{
            display: block;
            margin: 0 auto;
            max-width: 527px;
            width: 100%;
        }
    }
`

const ItemTitle = styled.h3`
    text-align: center;
    padding-bottom: 60px;
    color: #377DFF;
    text-transform: uppercase;
    font-weight: bold;
    @media(max-width: 1750px){
        padding-bottom: 30px;
    }
`

const Grid = styled.div`
    display: grid;
    grid-template-columns: 1fr 1fr 1fr;
    grid-column-gap: 30px;
    grid-row-gap: 60px;
    @media(max-width: 1198px){
        grid-template-columns: 1fr;
    }
`

const ProblemsResolving = (props) => {
    return (
        <Article marginDesktop={props.marginDesktop} marginTablet={props.marginTablet} marginPhone={props.marginPhone}>
            <Container>
                <Title>{props.acf.title}</Title>
                <Grid>
                    {props.acf.repeater.map((el, index) =>
                        <Item key={index}>
                            <ItemTitle>{el.title}</ItemTitle>
                            <img alt={el.img_alt} src={el.img} />
                        </Item>
                    )}
                </Grid>
            </Container>
            <BottomBG></BottomBG>
        </Article>
    )
}

export default ProblemsResolving