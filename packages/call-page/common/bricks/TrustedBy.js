import React from 'react'
import { styled } from "frontity"
import { Container } from '../styles'
import { Bounce } from '../../common/styles'
import MiniForm from './../mini-form/Form'

const Article = styled.article`
    position: relative;
    padding-top: ${props => props.marginDesktop ? props.marginDesktop : '30px'};
    @media(max-width: 1198px){
        padding-top: ${props => props.marginTablet ? props.marginTablet : '60px'};
    }
    @media(max-width: 764px){
        padding-top: ${props => props.marginPhone ? props.marginPhone : '60px'};
    }
`

const Title = styled.h1`
    text-align: center;
    font-size: clamp(45px, 3.5vw, 60px);
    line-height: clamp(60px, 3.5vw, 80px);
    font-weight: bold;
    max-width: 1080px;
    margin: 0 auto 48px;

    @media(max-width: 1198px) {
        font-size: 50px;
        line-height: 60px;
        margin: 0 auto 48px;
    }

    @media(max-width: 764px) {
        font-size: 28px;
        line-height: 40px;
        margin: 0 auto 55px;
    }
`

const Subtitle = styled.p` 
    text-align: center;
    color: #6E7276;
    font-size: 28px;
    margin-bottom: 60px;
    max-width: 1080px;
    margin: 0 auto 60px;
    line-height:40px;

    @media(max-width: 1198px) {
        font-size: 22px;
        margin: 0 auto 55px;
        line-height: 30px;
    }

    @media(max-width: 764px) {
        font-size: 18px;
        margin: 0 auto 30px;
        line-height: 28px;
    }
`

const RepeaterTitle = styled.h2`
    text-align: center;
    margin-bottom: 40px;
    font-size: 28px;
    font-weight: bold;

`

const RepeaterText = styled.p`
    text-align: center;
    margin-bottom: 60px;
    font-size: 28px;
    line-height: 40px;

    @media (max-width:1198) {
        margin-bottom: 50px;
        font-size:14px;
        line-height: 24px;
    }

    @media (max-width:764px) {
        margin-bottom: 30px;
    }
`
const Flex = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    flex-wrap: wrap;
    margin-bottom: 70px;

    @media(max-width: 1198px) {
        justify-content: center;
    }

    img {
        max-width: 100%;
        max-height: 180px;
        width: auto;
    }
`
const FlexItem = styled.div`
    flex-basis: 230px;
    padding: 10px 30px;
    display: flex;
    justify-content: center;
    align-items: center;
`

const Arrow = styled.div`
    width: 0px;
    height: 0px;
    position: relative;
    left: 50%;
    margin: 40px 0;
    animation: 2.2s infinite ${Bounce};

@media(max-width: 1198px) {
    display: none;
}

    &::before,
    &::after{
        content: '';
        position: absolute;
        width: 20px;
        height: 1px;
        background-color: #377DFF;
        top: 0;
    }

    &::before{
        transform-origin: left;
        transform: rotateZ(-45deg);
        left: 0;
    }

    &::after{
        transform-origin: right;
        transform: rotateZ(45deg);
        right: 0;
    }
`

const TrustedBy = (props) => {
    return (
        <Article marginDesktop={props.marginDesktop} marginTablet={props.marginTablet} marginPhone={props.marginPhone}>
            <Container>
                <Title>{props.acf.title}</Title>
                <Subtitle>{props.acf.text}</Subtitle>

                <RepeaterTitle>{props.acf.repeater_title}</RepeaterTitle>
                <Flex>
                    {
                        props.acf.repeater.map((el, index) =>
                            <FlexItem key={index}><img alt={props.acf.img_alt} src={el.img} /></FlexItem>
                        )
                    }
                </Flex>

                {
                    props.acf.repeater_text
                        ?
                        <RepeaterText>
                            {props.acf.repeater_text}
                        </RepeaterText>
                        : null
                }

                {
                    props.acf.form
                        ?
                        <MiniForm acf={props.acf.form} />

                        : null
                }




            </Container>
            <Arrow>
            </Arrow>
        </Article >
    )
}

export default TrustedBy