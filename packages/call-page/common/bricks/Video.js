import React from 'react'
import { styled } from "frontity"
import { Container } from '../styles'
import Background from '../sprites/video_bg3.png'
import Youtube from 'react-lazyload-youtube'
import PlayIcon from '../sprites/play.svg'

const Article = styled.article`
    position: relative;
    height: 100%;
    padding-top: ${props => props.marginDesktop ? props.marginDesktop : 'clamp(140px, 12vw, 240px);'};
    @media(max-width: 1198px){
        padding-top: ${props => props.marginTablet ? props.marginTablet : 'clamp(90px, 12vw, 140px);'};
    }
    @media(max-width: 764px){
        padding-top: ${props => props.marginPhone ? props.marginPhone : '90px'};
    }
`

const Title = styled.h2`
    margin: 0 0 120px;
    text-align: center;
    font-size: 45px;
    font-weight: bold;
    line-height: 60px;
    @media(max-width: 1198px) {
        max-width: 510px;
        margin: 0 auto 60px;
    }
    @media(max-width: 767px) {
        margin: 0 auto 55px;
        font-size: 28px;
        line-height: 40px;
    }
`

const BottomBG = styled.div`
    position: absolute;
    background-image: url(${Background});
    background-repeat: no-repeat;
    background-size: cover;
    background-position: bottom;
    width: 100%;
    height: 450px;
    bottom: 0;
    transform: translateY(10%);
    left: 0;
    z-index: 0;
    @media(max-width: 1198px) {
        height:245px;
        /* transform: translateY(35%); */
    }
    @media(max-width: 764px) {
    background-size: 0;
    }
`

const IframeWrapper = styled.div`
    position: relative;
    max-width:1080px;
    height: 640px;
    margin: 0 auto;
    @media(max-width: 1198px) {
    height: 400px;
    }

    @media(max-width: 764px) {
        height: 300px;
    }

    img {
        position: absolute;
        left: 50%;
        bottom:50%;
        transform: translate(-50%, 50%);
        max-width: 100%;
        height: 100%;
        z-index: 1;
        @media(max-width: 764px) {
            display: none;
        }
    }

    div,
    iframe {
        cursor: pointer;
        background-repeat: no-repeat;
        background-size: cover;
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        border-radius: 12px;
        width:640px;
        height: 360px;
        max-width: 100%;
        border: 0;
        z-index: 2;

        @media(max-width: 1198px) {
            width:400px;
            height: 230px;
        }
        @media(max-width: 764px) {
            max-width: 100%;
            width:100%;
            height: 100%;
        }
        @media(max-width: 578px) {
            max-width: 100%;
            width:100%;
            height: 300px;
        }
    }  
    iframe{
        z-index: 90;
    }
`

const Play = styled.span`
    width: 120px;
    height:85px;
    position: absolute;
    z-index: 80;
    left: 50%;
    top: 50%;
    transform: translate(-50%, -50%);
    pointer-events: none;

    &:before{
        content: url(${PlayIcon});
        cursor: pointer;
    }
`

const Video = (props) => {
    debugger
    return (
        <Article id={props.acf.id} marginDesktop={props.marginDesktop} marginTablet={props.marginTablet} marginPhone={props.marginPhone}>
            <Container>
                <Title>
                    {props.acf.video_title}
                </Title>
                <IframeWrapper>
                    <div>
                        <Youtube videoId={props.acf.video} />
                        <Play></Play>
                    </div>
                    <img alt={props.acf.video_bg_alt} src={props.acf.video_bg} />
                </IframeWrapper>
            </Container>
            <BottomBG></BottomBG>
        </Article>
    )
}

export default Video