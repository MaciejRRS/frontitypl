import React from 'react'
import { styled } from "frontity"
import { Container } from '../styles'

const Article = styled.article`
    position: relative;
    padding-top: ${props => props.marginDesktop ? props.marginDesktop : 'clamp(140px, 12vw, 240px);'};
    @media(max-width: 1198px){
        padding-top: ${props => props.marginTablet ? props.marginTablet : 'clamp(90px, 12vw, 140px);'};
    }
    @media(max-width: 764px){
        padding-top: ${props => props.marginPhone ? props.marginPhone : '90px'};
    }
`

const Title = styled.h2`
    text-align: center;
    font-size: 45px;
    font-weight: bold;
    line-height: 60px;
    max-width: 1080px;
    margin: 0 auto 100px;
    @media(max-width: 1198px){
        margin-bottom: 75px;
    }
    @media(max-width: 764px){
        font-size: 28px;
        line-height: 40px;
        margin-bottom: 50px;
    }
`

const Grid = styled.div`
    display: grid;
    grid-template-columns: 1fr 1fr;
    grid-column-gap: 30px;
    grid-row-gap: 60px;
    @media(max-width: 1198px){
        grid-template-columns: 1fr;
    }
`

const BlockWrap = styled.div`
    @media(max-width: 1198px){
        width: 100%;
        max-width: 807px;
        margin: 0 auto;
    }
`

const Img = styled.img`
    max-width: 100%;
    width: 100%;
    margin-bottom: 20px;
`

const TitleBlock = styled.h3`
    font-size: 18px;
    color: #377DFF;
    line-height: 28px;
    margin-bottom: 60px;
    text-transform: uppercase;
    font-weight: bold;
    text-align: center;
    @media(max-width: 764px){
        font-size: 16px;
    }
`

const TextBlock = styled.p`
    color:#6E7276;
    font-size: 16px;
    line-height: 24px;
    @media(max-width: 764px){
        font-size: 14px;
    }
`

const VoiceChat = (props) => {
    return (
        <Article marginDesktop={props.marginDesktop} marginTablet={props.marginTablet} marginPhone={props.marginPhone}>
            <Container>
                <Title>{props.acf.title}</Title>
                <Grid>
                    {props.acf.repeater.map((el, index) =>
                        <BlockWrap key={index}>
                            <TitleBlock>{el.title}</TitleBlock>
                            <Img alt={el.img_alt} src={el.img} />
                            <TextBlock>{el.text}</TextBlock>
                        </BlockWrap>
                    )}
                </Grid>
            </Container>
        </Article>
    )
}

export default VoiceChat