import React from 'react'
import { styled } from "frontity"
import { Flex, Link } from './../styles'
import GoogleIcon from '../sprites/Google_icon.png'

const FormS = styled.form`
    display: flex;
    flex-direction: column;
    max-width: 800px;
    margin: 0 auto;
`

const LocFlex = styled(Flex)`
    max-width: 800px;
    margin: 0 auto;
    justify-content: space-between;

    @media(max-width: 900px){
        flex-direction: column;
    }
`

const FlexItem = styled.div`
    padding: 0 5px 0 25px;
    position: relative;

    &::before{
        content: '✓';
        position: absolute;
        left: 0;
        color: #377DFF;
    }

    @media(max-width: 764px){
        font-size: 16px;
        text-align: center;
    }
`

const FirstButton = styled(Link)`
    position: relative;
    padding: 12px 50px;
    border: 2px solid #377DFF;
    background-color: #377DFF;
    border-radius: 6px;
    font-weight: bold;
    font-size: 18px;
    color: #fff;
    margin-right: 30px;
    box-shadow: 0 3px 6px 0 #D7E5FF;
    cursor: pointer;
    /* border-bottom-left-radius: 0;
    border-top-left-radius: 0; */
    transition: all.2s linear;
    text-align: center;

    &:hover{
        background-color: #fff;
        color: #377DFF;
    }

    &:before{
        position: absolute;
        /* content: url(${GoogleIcon}); */
        left: 0px;
        top: -2px;
        height: 60px;
        /* transform: translateX(-100%); */
        background-color: #377DFF;
        /* border-bottom-left-radius: 6px;
        border-top-left-radius: 6px; */
    }
    @media(max-width: 820px){
     padding: 12px 35px;
    }

    @media(max-width: 764px){
      font-size: 16px;
      padding: 12px 20px
    }

    @media(max-width: 630px) {
        padding: 12px 15px;
        font-size: 14px;
    }

    @media(max-width: 540px){
      margin-right: 0;
      /* margin-left: 60px; */
      margin-bottom: 30px;
    }
`

const SecondButton = styled(Link)`
    padding: 12px 50px;
    border: 2px solid #377DFF;
    border-radius: 6px;
    font-weight: bold;
    font-size: 18px;
    box-shadow: 0 3px 6px 0 #D7E5FF;
    cursor: pointer;
    color: #000000;
    background-color: #ffffff;
    /* margin-right: -60px; */
    transition: all.2s linear;
    text-align: center;

&:hover {
    border-color: #377DFF;
    background-color: #377DFF;
    color: #FFFFFF;
}

    @media(max-width: 764px){
      font-size: 16px;
      padding: 12px 20px;
    }

    @media(max-width: 630px) {
        padding: 12px 15px;
        font-size: 14px;
    }

    @media(max-width: 540px){
      margin-right: 0;
    }
`

const ButtonFlex = styled.div`
    display: flex;
    justify-content: center;
    padding-bottom: 30px;

    @media(max-width: 540px){
        flex-direction: column;
    }
`

const Text = styled.p`
    text-align: center;
    max-width: 665px;
    width: 100%;
    margin: 30px auto 30px;
    font-size: 18px;
    font-weight: bold;
`

const Button = styled.a`
 position: relative;
    padding: 12px 50px;
    border: 2px solid #377DFF;
    border-radius: 6px;
    background-color: #377DFF;
    font-weight: bold;
    font-size: 18px;
    color: #fff;
    margin-right: 30px;
    box-shadow: 0 3px 6px 0 #D7E5FF;
    cursor: pointer;
    transition: all.2s linear;
    text-align: center;

    &:hover{
        background-color: #fff;
        color: #377DFF;
    }


    @media(max-width: 764px){
      font-size: 16px;
      padding: 12px 10px;
    }

    @media(max-width: 540px){
      margin-right: 0;
      margin-left: 0;
      margin-bottom: 30px;
    }
`

const MiniForm = (props) => {


    const queryForm = function (settings) {
        var reset = settings && settings.reset ? settings.reset : true;
        var self = window.location.toString();
        var querystring = self.split("?");
        if (querystring.length > 1) {
            var pairs = querystring[1].split("&");
            for (i in pairs) {
                var keyval = pairs[i].split("=");
                if (reset || sessionStorage.getItem(keyval[0]) === null) {
                    sessionStorage.setItem(keyval[0], decodeURIComponent(keyval[1]));
                }
            }
        }
        var hiddenFields = document.querySelectorAll("input[type=hidden], input[type=text]");
        console.log(hiddenFields)
        for (var i = 0; i < hiddenFields.length; i++) {
            var param = sessionStorage.getItem(hiddenFields[i].name);
            if (param) document.getElementsByName(hiddenFields[i].name)[0].value = param;
        }
    }

    function goToRegister(action) {
        queryForm();
        var form = document.getElementById('goToRegisterForm');
        form.action = action;
        form.submit();
    }



    return (
        <>
            {
                props.acf
                    ? <FormS>
                        {props.acf.text
                            ? <Text>{props.acf.text}</Text>
                            : null
                        }
                        <ButtonFlex>
                            {
                                props.acf.first_button_type
                                    ? <FirstButton link={props.acf.first_button_link}>
                                        {props.acf.first_button_text}
                                    </FirstButton>
                                    : <Button rel="noreferrer" onClick={() => { goToRegister(props.acf.first_button_link) }} >
                                        {props.acf.first_button_text}
                                    </Button>
                            }
                            {
                                props.acf.second_button_type
                                    ? <SecondButton link={props.acf.second_button_link}>
                                        {props.acf.second_button_text}
                                    </SecondButton>
                                    : <SecondButton rel="noreferrer" target='_blank' href={props.acf.second_button_link}>
                                        {props.acf.second_button_text}
                                    </SecondButton>
                            }
                        </ButtonFlex>
                        <LocFlex>
                            {
                                props.acf.repeater
                                    ? <>
                                        {
                                            props.acf.repeater.map(el =>
                                                <FlexItem>
                                                    {el.text}
                                                </FlexItem>
                                            )
                                        }
                                    </>
                                    : null
                            }

                        </LocFlex>
                    </FormS>
                    : null
            }
        </>
    )
}

export default MiniForm