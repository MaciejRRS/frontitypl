import React, { useState, useEffect } from 'react'
import { connect } from "frontity"
import Hero from './components/Hero'
import Content from './components/Content'
import SEO from '../../../common/SEO/seo'

/* eslint-disable */

const Blog = ({ state }) => {


  const blog = state.source.get("/blog-page/")
  const acfBlog = state.source[blog.type][blog.id]['acf']

  const posts = state.source.get(state.router.link)
  return (
    <>
      {
        posts.isReady && blog.isReady
          ? <main className='body'>
            <SEO seo={acfBlog.seo} date={state.source[blog.type][blog.id].date_gmt} />
            <Hero acf={acfBlog.hero} />
            <Content state={state} posts={posts} acf={acfBlog.filter} />
          </main>
          : null
      }
    </>
  )
}

export default connect(Blog)