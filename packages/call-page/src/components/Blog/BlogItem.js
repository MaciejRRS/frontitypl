import React, { useState, useEffect } from "react"
import { connect } from "frontity"
import { Container, Flex } from '../../../common/styles'
import { styled } from "frontity"
import SEO from '../../../common/SEO/seo'
import NavLink from "@frontity/components/link"
import RelatedPostItem from "./components/RelatedPostItem"

/* eslint-disable */

const Preview = styled.div`
`

const Grid = styled.div`
    display: flex;
    justify-content: space-evenly;
    padding-top: 100px;
    @media(max-width: 1198px){
        padding-top: 0px;
    }
`

const PreviewImg = styled.img`
    width: 100%;
    margin: 0 auto;
    display: block;
`

const PreviewTitle = styled.h1`
    font-size: 45px;
    line-height: 60px;
    font-weight: bold;
    text-align: center;
    max-width: 1082px;
    margin: 0 auto;
    padding: 0 0 50px;
    @media(max-width: 1198px){
        padding: 60px 0 30px;
        font-size: 28px;
        line-height: 40px;
    }
    @media(max-width: 764px){
        font-size: 22px;
        line-height: 30px;
    }
`

const PreviewAuthor = styled.div`
    font-size: 18px;
    line-height: 28px;
    color: #6E7276;
    padding-bottom: 60px;
    text-align: center;
`

const MiniContainer = styled(Container)`
    max-width: 387px;
    width: calc(100% - 30px);
    margin: 0 auto 0 0; 
    position: sticky;
    top: 120px;
    @media(max-width: 1396px){
        position: fixed;
        bottom: 0;
        top: unset;
        left: 0;
        right: 0;
        max-width: 100%;
        width: 100%;
        height: 90px;
        background-color: #fff;
        z-index: 90;
    }
`

const MiniItem = styled.div`
    width: 100%;
    box-shadow: 0 3px 6px 0 #00000016;
    border-radius: 3px;
    box-sizing: border-box;
    padding-bottom: 30px;
    background-color: #fff;

    img{
        max-width: 100%;
        max-height: 300px;
        display: block;
        margin: 0 auto;
    }

    h2{
        padding: 30px;
    }

    p{
        color: #000000;
        padding: 0 30px;
    }
    a{
        display: block;
        width: 250px;
        line-height: 60px;
        color: #fff;
        font-weight: bold;
        margin: 0 auto;
        background-color: #377DFF;
        border: 2px solid #377DFF;
        text-align: center;
        margin: 30px auto 0;
        border-radius: 6px;
        transition: .2s linear;

        &:hover{
            background-color: #fff;
            color: #377DFF;
        }
    }

    @media(max-width: 1396px){
        display: flex;
        justify-content: center;
        align-items: center;
        height: 100%;
        padding: 20px 0;

        h2{
            max-width: 220px;
            text-align: center;
            padding: 0 10px;
        }
        img{
            max-height: 100%;
            margin: 0;
        }
        p{
            display: none;
        }
        a{
            margin: 0;
            width: auto;
            line-height: 45px;
            padding: 0 10px;
            font-size: 14px;
        }
    }
    @media(max-width: 664px){
        img{
            display: none;
        }
    }

    @media(max-width:550px){
        h2{
            font-size:12px;
        }
        a{
            min-width: 140px;
        }
    }
`

const LocContainer = styled(Container)`
    max-width: 804px;
    width: calc(100% - 30px);
    margin: 0 auto 0 -90px;
    @media(max-width: 1520px){
        margin: 0 auto 0 -60px;
    }
    @media(max-width: 1439px){
        margin: 0 auto;
    }
`

const Content = styled.div`
    margin: 30px 0;
    h1,h2,h3,h4,h5,h6{
        font-size: 28px;
        font-weight: bold;
        line-height: 40px;
        color: #000;
        padding-bottom: 15px;
        padding-top: 90px;
        margin-top: -50px;
        
        @media(max-width: 764px){
            font-size: 22px;
            line-height: 30px;
        }
    }

    h2 {
        strong {
        font-size: 28px;
        font-weight: bold;
        line-height: 40px;
        color: #000;
        padding-bottom: 15px;
        padding-top: 40px;
            @media(max-width: 764px){
                font-size: 22px;
                line-height: 30px;
            }
        }
    }

    blockquote{
        box-sizing: border-box;
        padding: 30px 80px;
        border-radius: 12px;
        background-color: #377DFF;
        position: relative;
        &::before{
            content: '„';
            position: absolute;
            color: #fff;
            font-size: 60px;
            line-height: 80px;
            left: 10px;
            top: -40px;
        }
        p{
            color: #fff;
            font-size: 22px;
            line-height:30px;
            padding: 0;
            @media(max-width: 764px){
                font-size: 18px;
                line-height: 28px;
            }
        }
        cite{
            color: #fff;
            text-align: right;
        }

        @media(max-width: 764px){
            padding: 20px 40px;
        }
    }

    iframe{
        width: 100%;
    }

    figure{
        img{
            max-width: 100%;
        }
        blockquote{
            box-sizing: border-box;
            padding: 30px;
            border-radius: 12px;
            background-color: #fff;
            position: relative;
            border: 2px solid #377DFF;

            &::before{
                display: none;
            }

            p{
                color: #000;
                font-size: 22px;
                line-height:30px;
                padding: 0;
                @media(max-width: 764px){
                    font-size: 18px;
                    line-height: 28px;
                }

            }
            cite{
                color: #000;
                text-align: right;
            }
        }
    }

    p{
        padding: 15px 0;
        color: #000000;
        font-size: 17px;
        line-height: 30px;
        @media(max-width: 764px){
            font-size: 14px;
            line-height: 22px;
        }
    }
    img{
        margin-top: 45px;
        max-width: 804px;
        width: 100%;
        height: auto;
    }
    a{
        color: #377DFF;
        font-size: 17px;
        line-height: 30px;

        @media(max-width: 764px) {
            font-size: 14px;
            line-height: 22px;
        }
    }
    ol{
        li{
            list-style: decimal;
            margin-left: 20px;
        }
    }
    ul{
        box-sizing: border-box;
        padding: 30px;
        border: 2px solid #377DFF;
        border-radius: 12px;
        li{
            margin-left: 20px;
            &::before{
                content: "•";
                color: #377DFF;
                display: inline-block;
                width: 1em;
                margin-left: -1em;
            }
        }
        a{
            color: #000000;
            font-size: 22px;
            line-height: 30px;
            display: block;
            padding: 4px 0;
            margin: 4px 0;
            @media(max-width: 1198px){
                font-size: 18px;
                line-height: 28px;
            }
        }
    }
`

const TypesFlex = styled.div`
    display: flex;
    flex-wrap: wrap;
    padding: 15px 10px;
`

const TypesItem = styled.p`
    padding: 7px 15px;
    border: 2px solid #377DFF;
    border-radius: 3px;
    font-size: 14px;
    line-height: 21px;
    font-weight: bold;
    margin: 0 15px 15px 0;
    color: #000;
`

const RelatedPostsGrid = styled.div`
    display: grid;
    grid-template-columns: 1fr 1fr 1fr;
    grid-column-gap: 30px;
    @media(max-width: 1198px){
        grid-template-columns: 1fr;
    }
`

const RelatedPostsTitle = styled.h2`
    padding: 240px 0 60px;
    font-size: 45px;
    line-height: 60px;
    text-align: center;
    @media(max-width: 1198px){
        font-size: 28px;
        line-height: 40px;
        padding: 60px 0;
    }
`

const RelatedPosts = styled.div`
    max-width: 526px;
    border-radius: 12px;
    box-shadow: 0 3px 6px 0 #00000016;
    width: 100%;
    position: relative;

    a{
        color: #000000;
    }

    img{
        width: 100%;
        border-top-right-radius: 12px;
        border-top-left-radius: 12px;
    }

    h3{
        padding: 0 15px 120px;
    }

    @media(max-width: 1198px){
        margin: 0 auto 30px;
    }
`

const InfoFlex = styled(Flex)`
    padding: 0 15px;
    position: absolute;
    bottom: 30px;
    width: calc(100% - 60px);
    color: #6E7276;
`

const AfterPostInform = styled.div`
    padding-top: 20px;
    display: flex;
    align-items: center;
    img{
        margin-right: 15px;
        margin-top: 0;
        width: 64px;
    }
`

const Post = ({ state, libraries }) => {
    const Html2React = libraries.html2react.Component

    let options = state.source.get("acf-options-page")
    options = options.acf.block_kopia

    let data = state.source.get(state.router.link)

    const post = state.source[data.type][data.id]
    const otherPosts = state.source.get('/blog/')

    let [chosenBlogPages, changeChosenBlogPages] = useState([])

    if (!chosenBlogPages.length) {
        for (let i = 0; chosenBlogPages.length < 3; i++) {
            if (otherPosts.items[i].id != data.id) {
                chosenBlogPages.push(otherPosts.items[i])
            }
        }
    }

    let allCategories = state.source.get('categories')
    let currentCategories = []

    if (post.category_blog) {
        post.category_blog.forEach(el => {
            allCategories.items.forEach(innerEl => {
                if (innerEl.id == el) {
                    currentCategories.push(innerEl)
                }
            })
        })
    }


    return (
        <main className='body'>
            <SEO seo={post.acf.seo} date={state.source[data.type][data.id].date_gmt} />
            <Container>
                <Grid>
                    <MiniContainer>
                        <MiniItem>
                            {post.acf.block.img
                                ? <img alt={post.acf.block.img_alt} src={post.acf.block.img} />
                                : <img alt={options.img_alt} src={options.img} />
                            }
                            {post.acf.block.title
                                ? <h2>{post.acf.block.title}</h2>
                                : <h2>{options.title}</h2>
                            }
                            {post.acf.block.text
                                ? <p>{post.acf.block.text}</p>
                                : <p>{options.text}</p>
                            }
                            {post.acf.block.button_link && post.acf.block.button_text
                                ? <NavLink link={post.acf.block.button_link}>{post.acf.block.button_text}</NavLink>
                                : <NavLink link={options.button_link}>{options.button_text}</NavLink>
                            }
                        </MiniItem>
                    </MiniContainer>
                    <LocContainer>
                        <PreviewTitle>{post.acf.preview.title}</PreviewTitle>
                        <PreviewAuthor>
                            <span>Autor: {post.acf.preview.author}</span>
                            <span>  |  </span>
                            <span>Opuplikowano: {post.acf.preview.date}</span>
                            <span>  |  </span>
                            <span>Czas czytania: {post.acf.preview.time}m</span>
                        </PreviewAuthor>
                        <Preview>
                            <PreviewImg src={post.acf.preview.img} />
                            <TypesFlex>
                                {currentCategories
                                    ? currentCategories.map((el, index) =>
                                        <NavLink link={'category_blog/' + el.slug}>
                                            <TypesItem key={index}>
                                                {el.name}
                                            </TypesItem>
                                        </NavLink>
                                    )
                                    : null
                                }
                            </TypesFlex>
                        </Preview>
                        <Content>
                            <Html2React html={post.content.rendered} />
                            <AfterPostInform>
                                <img alt={post.acf.preview.author_img_alt} src={post.acf.preview.author_img} />
                                <div>
                                    <div>Autor: {post.acf.preview.author}</div>
                                    <div>{post.acf.preview.date}</div>
                                </div>
                            </AfterPostInform>
                        </Content>
                    </LocContainer>
                </Grid>
                <RelatedPostsTitle>{post.acf.related.title}</RelatedPostsTitle>
                <RelatedPostsGrid>
                    {chosenBlogPages.map(el =>
                        <RelatedPostItem allCategories={allCategories} el={el} state={state} />
                    )}
                </RelatedPostsGrid>
            </Container>
        </main>
    )
}

export default connect(Post)