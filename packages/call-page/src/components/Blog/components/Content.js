import React, { useState } from 'react'
import { Container, Flex } from '../../../../common/styles'
import { styled } from "frontity"
import Loupe from '../../../../common/sprites/loupe.svg'
import ContentItem from './ContentItem'
import NavLink from "@frontity/components/link"

/* eslint-disable */

const Article = styled.article`
    padding-top: 120px;

    @media(max-width: 1198px){
        padding-top: 70px;
    }

`

const Grid = styled.div`
    display: grid;
    grid-template-columns: 6fr;
    grid-column-gap: 30px;
`

const LocFlex = styled(Flex)`
    align-items: center;
    flex-wrap: wrap;
    @media(max-width: 816px){
        flex-direction: column;
    }
`

const ButtonPagination = styled(NavLink)`
    padding: 0 5px;
    margin: 0 5px;
    color: ${props => props.page ? '#377DFF' : '#000000'};
    border: none;
    background-color: transparent;
    cursor: ${props => props.page ? 'default' : 'pointer'};

    &:disabled{
        cursor: default;
        color: #C2C9D1;
    }
`

const ButtonPaginationDisabled = styled.button`
    padding: 0 5px;
    margin: 0 5px;
    color: ${props => props.page ? '#377DFF' : '#000000'};
    border: none;
    background-color: transparent;
    cursor: ${props => props.page ? 'default' : 'pointer'};

    &:disabled{
        cursor: default;
        color: #C2C9D1;
    }
`

const Pagination = styled.div`
    padding: ${props => props.bottom ? '30px 0 0 0' : '0 0 30px 0'};
`

const LineColor = styled.img`
    display: block;
`

const SearchWrapper = styled.div`
    padding-bottom: 30px;
    width: 665px;
    position: relative;

    button{
        position: absolute;
        right: 0px;
        line-height: 46px;
        padding: 0 20px;
        border: 2px solid #C2C9D1;
        border-radius: 6px;
        box-sizing: border-box;
        color: #000;
        transition: .2s linear;
        background:transparent;
    }
    a{
        position: absolute;
        right: 0px;
        line-height: 46px;
        padding: 0 20px;
        border: 2px solid #C2C9D1;
        border-radius: 6px;
        box-sizing: border-box;
        color: #000;
        transition: .2s linear;
        &:hover{
            border: 2px solid #377DFF;
        }
    }
    img{
        position: absolute;
        left: 12px;
        top: 12px;
        width: 30px;
        height: 30px;
        z-index: 2;
        pointer-events: none;
    }
    @media(max-width: 1198px){
        width: 333px;
        img{
            left: 14px;
            top: 14px;
            width: 24px;
            height: 24px;
        }
    }
    @media(max-width: 816px){
        width: 180px;
        img{
            left: 15px;
            top: 15px;
            width: 20px;
            height: 20px;
        }
    }

    @media(max-width: 816px){
        width: 100%;
    }
`

const Search = styled.input`
    border: 2px solid #C2C9D1;
    border-radius: 6px;
    box-sizing: border-box;
    width: 100%;
    padding: 8px 62px;
    cursor: pointer;
    position: relative;

    &:focus{
        border-color: #377DFF;
        outline: none;
    }

    @media(max-width: 1198px){
        width: 333px;
        padding: 8px 60px;

        &:input{
        font-size: 16px;
        }
    }

    @media(max-width: 816px){
        width: 180px;
        padding: 8px 5px 8px 45px;
    }

    @media(max-width: 816px){
        width: 100%;
    }
`

const Aside = styled.aside`
    width: 100%;
    height: 70px;
    box-shadow: 0 3px 6px 0 #00000016;
    margin-bottom: 120px;
    position: sticky;
    top: 100px;;
    background-color: #fff;
    z-index: 10;

    @media(max-width: 1198px){
        height: auto;
    }

`

const AsideContent = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    padding: 0 60px;
    max-width: 1640px;
    margin: 0 auto;
    height: 100%;

    @media(max-width: 1198px){
        transition: .3s linear;
        height: ${props => props.isOpen ? `70px` : '0px'};
        transform: ${props => props.isOpen ? "translateY(0)" : 'translateY(0)'};
        opacity: ${props => props.isOpen ? "1" : '0'};
        pointer-events: ${props => props.isOpen ? "all" : 'none'};
    }
    @media(max-width: 1198px){
        height: ${props => props.isOpen ? `${props.heightMenu}px` : '0px'};
        flex-direction: column;
        justify-content: space-evenly;
    }
    @media(max-width: 500px){
        padding: 0 ;
    }
`

const AsideTitle = styled.button`
    display: none;

    @media(max-width: 1198px){
        display: block;
        margin: 0 auto;
        border: none;
        background-color: transparent;
        height: 50px;
    }
`

const AsideItem = styled.button`
    margin: 0 30px;
    border: none;
    background-color: transparent;
    cursor: pointer;
    position: relative;
    transition: .2s linear;
    font-size: 16px;
    line-height: 24px;

    &:hover {
        color: ${props => props.hoverColor};
    }

    img {
        transition: .1s linear;
        position: absolute;
        bottom: -5px;
        left: 0;
        width: 100%;
    }
`

const ItemGrid = styled.div`
    display: grid;
    grid-template-columns: 1fr 1fr 1fr;
    grid-column-gap: 30px;
    grid-row-gap: 30px;
    @media(max-width: 1198px){
        grid-template-columns: 1fr 1fr;
    }
    @media(max-width: 816px){
        grid-template-columns: 1fr;
    }
`


const Content = (props) => {
    let arrays = [[], [], []]

    for (let i = 1; i <= props.posts.items.length; i++) {
        if (i % 3 == 0) {
            arrays[2].push(props.posts.items[i - 1])
        } else if (i % 2 == 0) {
            arrays[1].push(props.posts.items[i - 1])
        } else {
            arrays[0].push(props.posts.items[i - 1])
        }
    }

    const [isOpenedMobileMenu, isOpenedMobileMenuChange] = useState(false)

    let asideHeight = 0

    props.acf.repeater.forEach(el => { asideHeight += 50 })

    const OpenMenu = () => {
        isOpenedMobileMenuChange(!isOpenedMobileMenu)
    }

    const [searchUrl, ChangeSearchUrl] = useState('')

    return (
        <Article>
            <Aside>
                <AsideTitle onClick={() => { OpenMenu() }}>
                    {'All Categories'}
                </AsideTitle>
                <AsideContent isOpen={isOpenedMobileMenu} heightMenu={asideHeight}>
                    {props.acf.repeater.map((el, index) =>
                        <>
                            {el.type == 'all'
                                ? <NavLink link={'/blog/'}>
                                    <AsideItem key={index} hoverColor={el.underline_color_hover}>
                                        {el.name}
                                        <LineColor alt='underline' src={el.underline} />
                                    </AsideItem>
                                </NavLink>
                                : <NavLink link={'category_blog/' + el.type.replace(' ', '-')}>
                                    <AsideItem key={index} hoverColor={el.underline_color_hover}>
                                        {el.name}
                                        <LineColor alt='underline' src={el.underline} />
                                    </AsideItem>
                                </NavLink>
                            }

                        </>
                    )}
                </AsideContent>
            </Aside>
            <Container>
                <Grid>
                    <section>
                        <LocFlex>
                            <Pagination>
                                {
                                    props.posts.previous
                                        ? <ButtonPagination link={props.posts.previous}>{'<'}</ButtonPagination>
                                        : <ButtonPaginationDisabled disabled >{'<'}</ButtonPaginationDisabled>
                                }
                                <ButtonPaginationDisabled page>{props.posts.page}</ButtonPaginationDisabled>
                                {
                                    props.posts.next
                                        ? <ButtonPagination link={props.posts.next}>{'>'}</ButtonPagination>
                                        : <ButtonPaginationDisabled disabled>{'>'}</ButtonPaginationDisabled>
                                }
                            </Pagination>
                            <SearchWrapper>
                                <img alt={'loupe'} src={Loupe} />
                                <Search value={searchUrl} onChange={event => ChangeSearchUrl(event.target.value)} placeholder={props.acf.input_placeholder} />
                                {
                                    searchUrl
                                        ? <NavLink link={`/blog?s=${searchUrl}`}>Wyszukaj!</NavLink>
                                        : <button disabled>Wyszukaj</button>
                                }

                            </SearchWrapper>
                        </LocFlex>
                        <ItemGrid>
                            {props.posts.items.map((el, index) =>
                                <ContentItem key={index} el={el} state={props.state} />
                            )}
                        </ItemGrid>
                        <Pagination bottom>
                            {
                                props.posts.previous
                                    ? <ButtonPagination link={props.posts.previous}>{'<'}</ButtonPagination>
                                    : <ButtonPaginationDisabled disabled >{'<'}</ButtonPaginationDisabled>
                            }
                            <ButtonPaginationDisabled page>{props.posts.page}</ButtonPaginationDisabled>
                            {
                                props.posts.next
                                    ? <ButtonPagination link={props.posts.next}>{'>'}</ButtonPagination>
                                    : <ButtonPaginationDisabled disabled>{'>'}</ButtonPaginationDisabled>
                            }
                        </Pagination>
                    </section>
                </Grid>
            </Container>
        </Article>
    )
}

export default Content