import React from 'react'
import Clock from '../../../../common/sprites/clock.png'
import { styled } from "frontity"
import { Flex } from '../../../../common/styles'
import NavLink from "@frontity/components/link"


const TypesFlex = styled.div`
    display: flex;
    flex-wrap: wrap;
    padding: 15px;
`

const TypesItem = styled.p`
    padding: 7px 15px;
    border: 2px solid #377DFF;
    border-radius: 3px;
    font-size: 14px;
    line-height: 21px;
    font-weight: bold;
    margin: 0 15px 15px 0;
    cursor: pointer;
`

const InfoFlex = styled(Flex)`
    padding: 0 15px;
    position: absolute;
    bottom: 30px;
    width: calc(100% - 60px);
    color: #6E7276;
    align-items: center;
    img{
        width: 32px;
        height: 32x;
        margin-right: 5px;
    }
`
const Item = styled.div`
    max-width: 526px;
    width: 100%;
    box-sizing: border-box;
    box-shadow: 0 3px 6px 0 #00000016;
    border-radius: 6px;
    margin: 0 auto; 
    height: 100%;
    position: relative;

    img{
        max-width: 526px;
        width: 100%;
    }

    @media(max-width: 816px){
        max-width: 100%;

        img{
            max-width: 100%;
        }
    }
`

const Link = styled(NavLink)`
    color: #000000;
`

const ItemTitle = styled.h2`
    padding: 0px 15px 105px;
    font-size: 18px;

    @media(max-width: 1198px){
        font-size: 16px;
        line-height: 24px;
    }
`



const ContentItem = (props) => {
    const data = props.state.source[props.el.type][props.el.id]
    let allCategories = props.state.source.get('categories')
    let currentCategories = []

    if (data.category_blog) {
        data.category_blog.forEach(el => {
            allCategories.items.forEach(innerEl => {
                if (innerEl.id == el) {
                    currentCategories.push(innerEl)
                }
            })
        })
    }

    return (
        <Item>
            <Link link={'blog/' + data.slug}>
                <img alt={data.acf.img_alt} src={data.acf.preview.img} />
            </Link>
            <TypesFlex>
                {currentCategories
                    ? currentCategories.map((el, index) =>
                        <Link link={'category_blog/' + el.slug}>
                            <TypesItem key={index}>
                                {el.name}
                            </TypesItem>
                        </Link>
                    )
                    : null
                }
            </TypesFlex>
            <Link link={'blog/' + data.slug}>
                <ItemTitle>{data.acf.preview.title}</ItemTitle>
                <InfoFlex>
                    <div style={{ display: 'flex', 'align-items': 'center' }}>
                        <img style={{ width: '48px', height: '48px', 'margin-right': '10px' }} alt='post author' src={data.acf.preview.author_img} />
                        <div>
                            <p style={{ 'line-height': '24px' }}>Author</p>
                            <p style={{ 'line-height': '24px' }}>{data.acf.preview.author}</p>
                        </div>
                    </div>
                    <div style={{ display: 'flex', 'align-items': 'center' }}>
                        <img alt='clock' src={Clock} /><span>{data.acf.preview.time}{'m'}</span>
                    </div>
                </InfoFlex>
            </Link>
        </Item>
    )
}

export default ContentItem