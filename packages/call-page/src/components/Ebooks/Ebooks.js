import React, { useState, useEffect } from 'react'
import Hero from './components/Hero'
import Content from './components/Content'
import SEO from '../../../common/SEO/seo'
import { connect } from "frontity"

const Ebooks = ({ state }) => {

    const data = state.source.get(state.router.link)

    const acf = state.source.get('/ebooks-page/')
    let props = state.source[acf.type][acf.id]
    return (
        <main className='body'>
            {props.acf && data.isReady
                ? <>
                    <SEO seo={props.acf.seo}  date={props.date_gmt}/>
                    <Hero acf={props.acf.hero} />
                    <Content state={state} ebooks={data} totalEbooks={1} />
                </>
                : null
            }

        </main>
    )
}

export default connect(Ebooks)