import React from 'react'
import { Link } from '../../../../common/styles'
import NavLink from "@frontity/components/link"
import { styled } from "frontity"


const Item = styled.div`
position: relative;
`

const Img = styled.img`
    width: 100%;
`

const Title = styled.h2`
    padding: 40px 0 20px;
    font-size: 28px;
    line-height: 40px;
    color: #000;
    @media (max-width: 1096px){
        font-size: 22px;
        line-height: 30px;
        padding-top: 30px;
        padding-bottom: 10px;
    }
    @media(max-width: 764px){
        font-size: 18px;
        line-height: 28px;
    }
`

const Text = styled.p`
    font-size: 16px;
    line-height: 24px;
    color: #6E7276;
    padding-bottom: 50px;
    margin-bottom: 60px;
    @media (max-width: 1096px){
        font-size: 14px;
        line-height: 20px;
        padding-bottom: 30px;
    }
`

const LocLink = styled(Link)`
    font-size: 16px;
    position: absolute;
    bottom: 0;
`


const ContentItem = (props) => {
    const data = props.state.source.wiedza[props.props.id]

    return (
        <Item>
            <NavLink link={data.link}>
                <Img src={data.acf.preview.img} />
                <Title>{data.acf.preview.title}</Title>
                <Text>{data.acf.preview.text}</Text>
            </NavLink>
            <LocLink link={'wiedza/' + data.slug}>{data.acf.preview.button_text}</LocLink>
        </Item>
    )
}

export default ContentItem