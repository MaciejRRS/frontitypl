import React from 'react'
import { Container } from '../../../../common/styles'
import { styled } from "frontity"

const Title = styled.h1`
    font-size: clamp(45px, 3.5vw, 60px);
    line-height: clamp(60px, 3.5vw, 80px);
    padding: 50px 0 60px;
    text-align: center;
`

const SubTitle = styled.p`
    font-size: 28px;
    line-height: 40px;
    color: #6E7276;
    padding-bottom: 120px;
    text-align: center;
`

const Hero = (props) => {
    return (
        <article>
            <Container>
                <Title>{props.acf.title}</Title>
                <SubTitle>{props.acf.text}</SubTitle>
            </Container>
        </article>
    )
}

export default Hero