import React from 'react'
import { styled } from "frontity"
import { Flex, Link } from '../../../common/styles';
import NavLink from "@frontity/components/link"
import GoogleIcon from '../../../common/sprites/Google_icon.png'
import { connect } from "frontity"

const Article = styled.footer`
    position: relative;
    margin-top: ${props => props.marginDesktop ? props.marginDesktop : '150px'};
    @media(max-width: 1198px){
        margin-top: ${props => props.marginTablet ? props.marginTablet : '120px'};
    }
    @media(max-width: 764px){
        margin-top: ${props => props.marginPhone ? props.marginPhone : '90px'};
    }
`

const Title = styled.h3`
  text-align: center;
  font-size: clamp(45px, 3.5vw, 60px);
  line-height: clamp(60px, 3.5vw, 80px);
  font-weight: bold;
  max-width: 1000px;
  margin: 0 auto;

  @media(max-width: 994px){
    font-size: 45px;
    line-height: 60px;
  }

  @media(max-width: 764px){
    font-size: 28px;
    line-height: 40px;
  }
`

const SubTitle = styled.p`
  text-align: center;
  font-size: 28px;
  line-height: 40px;
  margin: 60px auto 70px;
  max-width: 665px;

  @media(max-width: 994px){
    font-size: 22px;
    line-height: 30px;
  }

  @media(max-width: 764px){
    font-size: 18px;
    line-height: 22px;
    margin: 20px auto 30px;
  }
`

const Container = styled.div`
  max-width: 1800px;
  width: calc(100% - 120px);
  margin: 0 auto;
  @media(max-width: 564px){
    width: calc(100% - 30px);
  }
`

const FormPart = styled.div`
  padding-bottom: 160px;
  @media(max-width: 1080px){
    padding-bottom: 80px;
  }
  @media(max-width: 764px){
    padding-bottom: 40px;
  }
`

const FooterPart = styled.div`
  background-color: #FCFCFC;
  background-size: cover;
  background-repeat: no-repeat;
  width: 100%;
  padding: 90px 0 20px;
  @media(max-width: 764px){
    padding-top: 40px;
  }
`

const LocFlex = styled(Flex)`
  max-width: 800px;
  margin: 0 auto;
  justify-content: space-evenly;

  @media(max-width: 764px){
    flex-direction: column;
  }
`

const FlexItem = styled.div`
  padding-left: 25px;
  position: relative;

  &::before{
    content: '✓';
    position: absolute;
    left: 0;
    color: #377DFF;
  }

  @media(max-width: 764px){
    font-size: 16px;
  }
`

const Grid = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr 1fr 1fr 2fr;
  grid-column-gap: 60px;

  @media(max-width: 1600px){
    grid-template-columns: 1fr 1fr 1fr;
  }

  @media(max-width: 1080px){
    grid-template-columns: 1fr 1fr;
  }

  @media(max-width: 764px){
    grid-template-columns: 1fr;
  }
`

const LogoAndSocial = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: flex-start;
  flex-direction: column;

  img {
    max-width: 250px;
    height: auto;
    @media(max-width: 1600px) {
      max-width: 140px;
    }

    @media(max-width: 764px) {
      max-width: 250px;
    }

    @media(max-width: 600px) {
      margin: 0 auto;
      display:block;
    }
  }
`

const Part = styled.div`
  li{
    padding: 5px 0;
  }
  @media(max-width: 1600px){
    margin: 15px 0;
  }
  @media(max-width: 764px){

    ul{
      height: 0px;
      opacity: 0;
      pointer-events: none;
      transition: .2s linear;


      li {
          pointer-events: none;
          padding: 0;
          height: 0;
          text-align: center;
      }
    }

    h2{
      background-color: #00000012;
      padding: 10px 0;
      text-align: center;
    }

    &:hover{
      li{
        height: auto;
        pointer-events: all;
        padding: 5px 0;
      }
      ul{
        height: auto;
        opacity: 1;
        pointer-events: all;
      }
    }
  }

  @media(max-width: 564px){

  }
`

const SocialIcon = styled.img`
  width: 30px!important;
  height: 30px!important;
`

const SocialContainer = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
  margin-bottom: -70px;
  @media(max-width: 1600px){
      /* margin: 15px 0; */
      /* display: none; */
      width: 140%;
  }
  @media(max-width: 764px){
    margin: 45px 0;
    width: 100%;
  }
`

const Copyrights = styled.div`
  background-color: #FCFCFC;
  padding-bottom: 50px;
  padding-top: 80px;
`

const LinkItem = styled(NavLink)`
  color: #434343;
  transition: .2s linear;

  &:hover{
    color: #377DFF;
  }
`

const FirstButton = styled(Link)`
    position: relative;
    padding: 12px 50px;
    border: 2px solid #377DFF;
    border-radius: 6px;
    background-color: #377DFF;
    font-weight: bold;
    font-size: 18px;
    color: #fff;
    margin-right: 30px;
    box-shadow: 0 3px 6px 0 #D7E5FF;
    cursor: pointer;
    transition: all.2s linear;
    text-align: center;

    &:hover{
        background-color: #fff;
        color: #377DFF;
    }


    @media(max-width: 764px){
      font-size: 16px;
      padding: 12px 10px;
    }

    @media(max-width: 540px){
      margin-right: 0;
      margin-left: 0;
      margin-bottom: 30px;
    }
`


const SecondButton = styled(Link)`
    padding: 12px 50px;
    border: 2px solid #377DFF;
    border-radius: 6px;
    font-weight: bold;
    font-size: 18px;
    box-shadow: 0 3px 6px 0 #D7E5FF;
    cursor: pointer;
    color: #000000;
    background-color: #ffffff;
    /* margin-right: -60px; */
    transition: all.2s linear;
    text-align: center;

&:hover {
    background-color: #377DFF;
    color: #FFFFFF;
}

    @media(max-width: 764px){
      font-size: 16px;
    }

    @media(max-width: 540px){
      margin-right: 0;
    }
`

const ButtonFlex = styled.div`
  display: flex;
  justify-content: center;
  padding-bottom: 30px;

  @media(max-width: 540px){
    flex-direction: column;
  }
`

const CopyrightLink = styled(NavLink)`
  color: #000;
  transition: .2s linear;

  &:hover{
    color: #377DFF;
  }
`

const LinkRRS = styled.div`
a {
  color: #000000;
  transition: .2s linear;
  &:hover{
    color: #377DFF;
  }
}
`

const GridBig = styled.div`
display: grid;
grid-template-columns: 1fr 4fr;
grid-column-gap: 30px;

@media(max-width: 764px) {
 grid-template-columns: 1.5fr 1fr;
 grid-column-gap: 60px;
}

@media(max-width: 600px) {
 grid-template-columns:1fr;
 grid-column-gap: 15px;
}
`

const CopyrightMobile = styled.div`
  display: none;

  @media(max-width:764px) {
    display: block;
  }

  @media(max-width:600px) {
    display: none;
  }
`


const CopyrightDesctop = styled.div`
  @media(max-width: 764px) {
    display: none;
  }
  @media(max-width:600px) {
    display: block;
  }
`

const Button = styled.a`
 position: relative;
    padding: 12px 50px;
    border: 2px solid #377DFF;
    border-radius: 6px;
    background-color: #377DFF;
    font-weight: bold;
    font-size: 18px;
    color: #fff;
    margin-right: 30px;
    box-shadow: 0 3px 6px 0 #D7E5FF;
    cursor: pointer;
    transition: all.2s linear;
    text-align: center;

    &:hover{
        background-color: #fff;
        color: #377DFF;
    }


    @media(max-width: 764px){
      font-size: 16px;
      padding: 12px 10px;
    }

    @media(max-width: 540px){
      margin-right: 0;
      margin-left: 0;
      margin-bottom: 30px;
    }
`

const Footer = ({ state }) => {

  const queryForm = function (settings) {
    var reset = settings && settings.reset ? settings.reset : true;
    var self = window.location.toString();
    var querystring = self.split("?");
    if (querystring.length > 1) {
      var pairs = querystring[1].split("&");
      for (i in pairs) {
        var keyval = pairs[i].split("=");
        if (reset || sessionStorage.getItem(keyval[0]) === null) {
          sessionStorage.setItem(keyval[0], decodeURIComponent(keyval[1]));
        }
      }
    }
    var hiddenFields = document.querySelectorAll("input[type=hidden], input[type=text]");
    console.log(hiddenFields)
    for (var i = 0; i < hiddenFields.length; i++) {
      var param = sessionStorage.getItem(hiddenFields[i].name);
      if (param) document.getElementsByName(hiddenFields[i].name)[0].value = param;
    }
  }

  function goToRegister(action) {
    queryForm();
    var form = document.getElementById('goToRegisterForm');
    form.action = action;
    form.submit();
  }


  const Footer = state.source.get("/footer/")
  const acfFooter = state.source[Footer.type][Footer.id]['acf']



  return (
    <Article>
      <FormPart>
        <Container >
          <Title>{acfFooter.form.title}</Title>
          <SubTitle>{acfFooter.form.text}</SubTitle>
          <ButtonFlex>
            {
              acfFooter.form.first_button_type
                ? <Button link={acfFooter.form.first_button_link}>
                  {acfFooter.form.first_button_text}
                </Button>
                : <Button rel="noreferrer" onClick={() => { goToRegister(acfFooter.form.first_button_link) }} >
                  {acfFooter.form.first_button_text}
                </Button>
            }
            {
              acfFooter.form.second_button_type
                ? <SecondButton link={acfFooter.form.second_button_link}>
                  {acfFooter.form.second_button_text}
                </SecondButton>
                : <SecondButton rel="noreferrer" target='_blank' href={acfFooter.form.second_button_link}>
                  {acfFooter.form.second_button_text}
                </SecondButton>
            }
          </ButtonFlex>
          <LocFlex>
            {
              acfFooter.form.repeater.map((el, index) =>
                <FlexItem key={index}>
                  {el.text}
                </FlexItem>
              )
            }
          </LocFlex>
        </Container>
      </FormPart>
      <FormPart>
        <form id="goToRegisterForm" method="GET" target="_blank" action="" display="hidden" encType="text/plain">
          <input type="hidden" name="utm_source" value=""></input>
          <input type="hidden" name="utm_medium" value=""></input>
          <input type="hidden" name="utm_campaign" value=""></input>
        </form>
      </FormPart>
      <FooterPart>
        <Container>
          <GridBig>
            <LogoAndSocial>
              <img alt={acfFooter.footer.logo_alt} src={acfFooter.footer.logo} />
              <SocialContainer>
                {
                  acfFooter.footer.repeater.map((el, index) =>
                    <a key={index} rel="noreferrer" target='_blank' href={el.link}>
                      <SocialIcon alt={el.icon_alt} src={el.icon} />
                    </a>
                  )
                }
              </SocialContainer>
              <CopyrightMobile>
                {acfFooter.footer.copyright}
                {
                  acfFooter.footer.copyright_links.map((el, index) =>
                    <React.Fragment key={index}>
                      {el.link_type
                        ? <> | <CopyrightLink link={el.link}>{el.name}</CopyrightLink></>
                        : <> | <CopyrightLink rel="noreferrer" target='_blank' href={el.link}>{el.name}</CopyrightLink></>
                      }
                    </React.Fragment>
                  )
                }
              </CopyrightMobile>
            </LogoAndSocial>
            <Grid>

              {
                acfFooter.footer.links.map((el, index) =>
                  <Part key={index}>
                    <h2>{el.title}</h2>
                    <ul>
                      {
                        el.links.map((innerEl, innerIndex) =>
                          <li key={innerIndex}>
                            {
                              innerEl.link_type
                                ? <LinkItem link={innerEl.link}>{innerEl.name}</LinkItem>
                                : <LinkItem rel="noreferrer" target='_blank' href={innerEl.link} >{innerEl.name}</LinkItem>
                            }
                          </li>
                        )
                      }
                    </ul>
                  </Part>
                )
              }
            </Grid>
          </GridBig>
        </Container>
      </FooterPart>

      <CopyrightDesctop>
        <Copyrights>


          <Container>
            {acfFooter.footer.copyright}
            {
              acfFooter.footer.copyright_links.map((el, index) =>
                <React.Fragment key={index}>
                  {el.link_type
                    ? <> | <CopyrightLink link={el.link}>{el.name}</CopyrightLink></>
                    : <> | <CopyrightLink rel="noreferrer" target='_blank' href={el.link}>{el.name}</CopyrightLink></>
                  }
                </React.Fragment>
              )
            }
          </Container>
          <Container>
            <LinkRRS>
              Realizacja: <a target="_blank" rel="noreferrer" href='https://redrocks.pl/'>RedRockS - Agencja Kreatywna</a>
            </LinkRRS>
          </Container>

        </Copyrights>
      </CopyrightDesctop>
    </Article>
  )
}

export default connect(Footer)