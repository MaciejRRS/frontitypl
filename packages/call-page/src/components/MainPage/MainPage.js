import React, { useState, useEffect } from 'react'
import Hero from './components/Hero'
import Clients from './components/Clients'
import Develope from './components/Develope'
import Increase from '../../../common/bricks/Increase'
import Integration from '../../../common/bricks/Integration'
import IncreaseList from '../../../common/bricks/Clients'
import Video from '../../../common/bricks/Video'
import About from '../../../common/bricks/About'
import Awards from '../../../common/bricks/Awards'
import Bussinesses from '../../../common/bricks/Bussinesses'
import SEO from '../../../common/SEO/seo'
import { connect } from "frontity"

const MainPage = ({ state }) => {

    const Main = state.source.get("/strona-glowna/")
    const acfMain = state.source[Main.type][Main.id]['acf']
    return (
        <main className='body'>
            <SEO seo={acfMain.seo} date={state.source[Main.type][Main.id].date_gmt} />
            <Hero acf={acfMain.first_hero} form_free_test={acfMain.form_free_test} />
            <Video acf={acfMain.second_video} />
            <Clients acf={acfMain.fifth_clients} />
            <IncreaseList acf={acfMain.increase_list} />
            <About acf={acfMain.third_about} />
            <Increase acf={acfMain.fourth_increase} />
            <Integration acf={acfMain.sixth_integration} />
            <Bussinesses acf={acfMain.bussinesses} />
            <Awards acf={acfMain.awards} />
            <Develope acf={acfMain.seventh_develope} />
        </main>
    )
}

export default connect(MainPage)