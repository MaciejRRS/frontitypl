import React from 'react'
import { styled } from "frontity"
import { Container, Flex, Text, Bounce } from '../../../../common/styles'
import Link from "@frontity/components/link"
import GoogleIcon from '../../../../common/sprites/Google_icon.png'
import ReactHtmlParser from 'react-html-parser';

const Title = styled.h1`
    font-weight: bold;
    display: none;
    
    @media(max-width: 1276px){
        display: block;
        padding-bottom: 60px;
        font-size: 45px;
        line-height: 50px;
    }
    @media(max-width: 896px){
        padding-bottom: 10px;
    }
    @media(max-width: 564px){
        font-size: 28px;
        line-height: 40px;
    }
    
`

const DesctopTitle = styled(Title)`
    display: block;
    font-size: clamp(45px, 3.5vw, 60px);
    line-height: clamp(60px, 3.5vw, 80px);
    margin-bottom: 20px;
    @media(max-width: 1276px){
        display: none;
        padding: 0;
    }
`

const LocFlex = styled(Flex)`
align-items: flex-end;
    @media(max-width: 1276px){
        align-items: flex-start;
    }
    @media(max-width: 896px){
        flex-direction: column;
    }
`

const Left = styled.div`
    width: 50%;
    padding-right: 30px;
    @media(max-width: 896px){
        width: 100%;
        padding: 0;
    }
`

const Right = styled.div`
    width: 50%;
    @media(max-width: 896px){
        width: 100%;
    }
`

const Img = styled.img`
    margin: 0 auto;
    display: block;
    @media(max-width: 1980px){
        max-height: 480px;
    }
    @media(max-width: 1378px){
        max-width: 400px;
    }
    @media(max-width: 896px){
        max-width: 270px;
        display: block;
        margin: 60px auto 0;
    }
`

const Box = styled.div`
    margin: 90px auto 0;
    text-align: center;
`

const Arrow = styled.div`
    width: 0px;
    height: 0px;
    position: relative;
    left: 50%;
    margin: 40px 0;
    animation: 2.2s infinite ${Bounce};

    @media(max-width: 1198px) {
        display: none;
    }

    &::before,
    &::after{
        content: '';
        position: absolute;
        width: 20px;
        height: 1px;
        background-color: #377DFF;
        top: 0;
    }

    &::before{
        transform-origin: left;
        transform: rotateZ(-45deg);
        left: 0;
    }

    &::after{
        transform-origin: right;
        transform: rotateZ(45deg);
        right: 0;
    }
`

const LocText = styled(Text)`
    margin-bottom: 50px;
    
    @media(max-width: 564px){
        font-size: 14px;
    }
`


const TextWraper = styled.div`
    p {
        color: #3F4143;
        line-height: 27px;
        font-size: 15px;
        margin-bottom: 50px;
        @media(max-width: 1198px){
            margin-bottom: 0;
        }
        @media(max-width: 564px){
            font-size: 14px;
        }

        strong {
            position: relative;
            &:after {
                position: absolute;
                content: '';
                z-index: -1;
                left: -6%;
                bottom: -8px;
                height: 4px;
                width: 110%;
                background-image: url(${props => props.background});
                background-repeat: no-repeat;
                background-size: 100% 4px;
                @media(max-width: 767px) {
                    background-size: 0;
                }
            }

            @media(max-width: 767px) {
                   color: #377DFF;
                }
        }
    }
`


const FirstButtonInner = styled(Link)`
    position: relative;
    padding: 13px 6px;
    border: 2px solid #377DFF;
    background-color: #377DFF;
    border-radius: 6px;
    font-weight: bold;
    font-size: 18px;
    color: #fff;
    margin-right: 30px;
    box-shadow: 0 3px 6px 0 #D7E5FF;
    cursor: pointer;
    border-bottom-left-radius: 0;
    border-top-left-radius: 0;
    text-align: center;
    transition: all.2s linear;

    &:hover{
        background-color: #fff;
        color: #377DFF;
    }

    @media(max-width: 1276px){
        margin: 0 90px 0 0;
        font-size: 16px;
    }

    @media(max-width: 896px){
        margin: 0 10px 0 0;
        width: 50%;
    }

    @media(max-width: 764px){
        font-size:13px;
    }



    &:before{
        position: absolute;
        content: url(${GoogleIcon});
        left: 0px;
        top: -2px;
        height: 60px;
        transform: translateX(-100%);
        background-color: #377DFF;
        border-bottom-left-radius: 6px;
        border-top-left-radius: 6px;
    }
`

const FirstButtonOuter = styled.a`
    position: relative;
    padding: 12px 50px;
    border: 2px solid #377DFF;
    border-radius: 6px;
    background-color: #377DFF;
    font-weight: bold;
    font-size: 18px;
    color: #fff;
    margin-right: 30px;
    box-shadow: 0 3px 6px 0 #D7E5FF;
    cursor: pointer;
    transition: all.2s linear;
    text-align: center;

    &:hover{
        background-color: #fff;
        color: #377DFF;
    }

    @media(max-width: 1276px){
        margin: 0 15px 0 0;
        font-size: 16px;
    }
    @media(max-width: 896px){
        margin: 0 10px 0 0;
        width: 50%;
    }

    @media(max-width: 764px){
        font-size:13px;
    }

    @media(max-width: 564px){
        /* width: calc(100% - 15px); */
        width: auto;
        margin: 0 0 0 0;
    }

    
`

const SecondButtonInner = styled(Link)`
    padding: 12px 50px;
    border: 2px solid #377DFF;
    border-radius: 6px;
    font-weight: bold;
    font-size: 18px;
    box-shadow: 0 3px 6px 0 #D7E5FF;
    cursor: pointer;
    color: #000000;
    transition: all.2s linear;
    text-align: center;

&:hover {
    background-color: #FFFFFF;
    color: #377DFF;
}

    @media(max-width: 1276px){
        padding: 12px 30px;
        margin-left: -60px;
        margin-top: 15px;
        margin-right: 90px;
        font-size: 16px;
    }
    @media(max-width: 896px){
        margin: 0;
        width: 50%;
    }

    @media(max-width: 764px){
        font-size:13px;
    }
    @media(max-width: 564px){
        padding: 12px 30px;
        margin-left: -60px;
        margin-top: 15px;
        margin-right: 0px;
        width: 100%;
    }
`

const SecondButtonOuter = styled.a`
    padding: 12px 50px;
    border: 2px solid #377DFF;
    border-radius: 6px;
    font-weight: bold;
    font-size: 18px;
    box-shadow: 0 3px 6px 0 #D7E5FF;
    cursor: pointer;
    color: #000;
    transition: all.2s linear;
    text-align: center;

&:hover {
    border-color: #377DFF;
    background-color: #377DFF;
    color: #FFFFFF;
}


    @media(max-width: 1276px){
        padding: 12px 30px;
        margin-left: 0;
        margin-top: 0;
        margin-right:0;
        font-size: 16px;
    }
    @media(max-width: 896px){
        margin: 0;
        width: 50%;
    }

    @media(max-width: 764px){
        font-size:13px;
    }

    @media(max-width: 564px){
        padding: 12px 30px;
        margin-left: 0;
        margin-top: 15px;
        margin-right: 0px;
        width: auto;
    }
`

const ButtonFlex = styled.div`
    display: flex;
    justify-content: flex-start;
    /* @media(max-width: 1276px){
        flex-direction: column;
    } */
    /* @media(max-width: 896px){
        flex-direction: row;
    } */
    @media(max-width: 564px){
        flex-direction: column;
    }
`

const List = styled.ul`
display: flex;
justify-content: center;
align-items: center;
flex-wrap: wrap;
margin-bottom: 30px;
`

const ListItem = styled.li`
flex-basis: 100px;
margin: 30px;
max-height: 60px;
display: flex;
align-items: center;
justify-content: center;

@media(max-width:764px) {
    margin: 15px; 
}

@media(max-width:764px) {
    flex-basis: 13%;  
}

img {
    max-width: 100%;
    height: auto;
}
`



const Hero = (props) => {

    const queryForm = function (settings) {
        var reset = settings && settings.reset ? settings.reset : true;
        var self = window.location.toString();
        var querystring = self.split("?");
        if (querystring.length > 1) {
            var pairs = querystring[1].split("&");
            for (i in pairs) {
                var keyval = pairs[i].split("=");
                if (reset || sessionStorage.getItem(keyval[0]) === null) {
                    sessionStorage.setItem(keyval[0], decodeURIComponent(keyval[1]));
                }
            }
        }
        var hiddenFields = document.querySelectorAll("input[type=hidden], input[type=text]");
        console.log(hiddenFields)
        for (var i = 0; i < hiddenFields.length; i++) {
            var param = sessionStorage.getItem(hiddenFields[i].name);
            if (param) document.getElementsByName(hiddenFields[i].name)[0].value = param;
        }
    }

    function goToRegister(action) {
        queryForm();
        var form = document.getElementById('goToRegisterForm');
        form.action = action;
        form.submit();
    }

    return (

        <article>
            <link
                rel="preload"
                as="image"
                href={props.acf.img_main}
            />
            <Container>
                <Title>{props.acf.title_main}</Title>
                <LocFlex>
                    <Left>
                        <DesctopTitle>{props.acf.title_main}</DesctopTitle>
                        <LocText>{props.acf.text_main}</LocText>
                        <ButtonFlex>
                            {
                                props.form_free_test.first_button_type
                                    ? <FirstButtonInner to={props.form_free_test.first_button_link}>
                                        {props.form_free_test.first_button_text}
                                    </FirstButtonInner>
                                    : <FirstButtonOuter rel="noreferrer" onClick={() => { goToRegister(props.form_free_test.first_button_link) }}>
                                        {props.form_free_test.first_button_text}
                                    </FirstButtonOuter>
                            }
                            {
                                props.form_free_test.second_button_type
                                    ? <SecondButtonInner to={props.form_free_test.second_button_link}>
                                        {props.form_.second_button_text}
                                    </SecondButtonInner>
                                    : <SecondButtonOuter as='a' rel="noreferrer" target='_blank' href={props.form_free_test.second_button_link}>
                                        {props.form_free_test.second_button_text}
                                    </SecondButtonOuter>
                            }
                        </ButtonFlex>
                    </Left>
                    <Right>
                        <Img alt={props.acf.img_alt} src={props.acf.img_main} />
                    </Right>
                </LocFlex>
                <Box>
                    <List>
                        {
                            props.acf.repeater.map((el, index) =>
                                <ListItem key={index}>
                                    <img alt={el.img_alt} src={el.img} />
                                </ListItem>
                            )
                        }
                    </List>
                    <TextWraper background={props.acf.background_line}>{ReactHtmlParser(props.acf.text_partners)}</TextWraper>
                </Box>
                <Arrow>
                </Arrow>
            </Container>
        </article>
    )
}

export default Hero