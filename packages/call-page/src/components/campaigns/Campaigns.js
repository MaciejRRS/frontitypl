import React, { useState, useEffect } from 'react'
import { connect } from "frontity"
import Hero from './components/Hero'
import Content from './components/Content'
import SEO from '../../../common/SEO/seo'

const Campaigns = ({ state }) => {

    const posts = state.source.get(state.router.link)
    const data = state.source.get('/campaigns-page/')
    let props = state.source[data.type][data.id]


    return (
        <>
            {data.isReady
                ? <main className='body'>
                    <SEO seo={props.acf.seo} date={props.date_gmt} />
                    <Hero acf={props.acf.hero} />
                    {posts.isReady
                        ? <Content state={state} posts={posts.items} postsData={posts} totalPosts={props.totalCampaigns} isFetchingAddData={props.isFetchingAddData} />
                        : null
                    }
                </main>
                : null
            }
        </>
    )
}

export default connect(Campaigns)