import React from 'react'
import { styled } from "frontity"
import Background from '../../../common/sprites/concrete_bg.png'
import SEO from '../../../common/SEO/seo'
import Hero from './components/HeroItem'
import Video from '../../../common/bricks/Video'
import About from '../../../common/bricks/About'
import Features from '../../../common/bricks/Features'
import Integration from '../../../common/bricks/Integration'
import Bussinesses from '../../../common/bricks/Bussinesses'
import Awards from '../../../common/bricks/Awards'
import ReactHtmlParser from 'react-html-parser'
import { connect } from "frontity"

const Title = styled.h1`
    padding: 50px 0 160px;
    text-align: center;
    font-size: clamp(45px, 3.5vw, 60px);
    line-height: clamp(60px, 3.5vw, 80px);
    max-width: 1200px;
    margin: 0 auto;
    @media(max-width: 1198px){
        font-size: 45px;
        line-height: 60px;
        padding: 50px 0 45px;
    }

    @media(max-width: 562px){
        font-size: 28px;
        line-height: 40px;
        padding: 50px 0 35px;
    }
`

const Container = styled.div`
    max-width: 1640px;
    width: 100%;
    margin: 0 auto;
    padding-bottom: 80px;
    @media(max-width: 1196px){
        width: calc(100% - 120px);
    }
    @media(max-width: 562px){
        width: calc(100% - 30px);
    }
`

const Item = styled.div`
    width: 387px;
    padding: 0 30px;
    box-sizing: border-box;
    box-shadow: 0 3px 6px 0 #00000016;
    border-radius: 6px;
    min-height: 100%;

    @media(max-width: 1740px){
        width: 540px;
        margin-bottom: 90px;
        margin: 0 15px;
        margin-bottom: 60px;
    }

    @media(max-width: 1196px){
        width: 100%;
    }

    @media (max-width: 562px){
        padding: 0 15px;
    }

    &:first-child{
        img {
            max-height: 120px;
        }
        @media(max-width: 1198px) {
            img {
                display: none;
            }
            div{
                align-items: center;
            }
        }
    }
`

const Flex = styled.div`
    display: flex;
    justify-content: space-between;
    flex-wrap: wrap;
    position: relative;

    @media(max-width: 1740px){
        justify-content: space-evenly;
    }
`

const InnerFlex = styled.div`
    display: flex;
    align-items: flex-end;
`

const CaseTypeWrapper = styled.div`
    display: flex;
    align-items: center;
    padding-top: 30px;
`

const ComapanyName = styled.h2`
font-size: 22px;
margin: 30px 0 0;
`

const CaseTypeText = styled.h3`
    /* padding-left: 15px; */
    color: #6E7276;
    font-size: 18px;
    line-height: 24px;
    font-weight: normal;
`

const Date = styled.h4`
    color: #377DFF;
    text-transform: uppercase;
    font-size: 18px;
    line-height: 24px;
    padding: 30px 0;

    @media(max-width: 1198px){
        font-size: 16px;
    }

`

const Target = styled.p`
    color: #6E7276;
    font-size: 18px;
    line-height: 28px;
    margin-bottom: -20px;
    font-weight: 400;
    @media(max-width: 1198px){
        font-size: 16px;
        line-height: 24px;
    }
`

const ItemText = styled.p`
    color: #6E7276;
    font-size: 16px;
    line-height: 24px;
    padding: 30px 0 0;

    @media (max-width: 768px){
        font-size: 14px;
        line-height: 20px;
    }
`

const ItemTitle = styled.h2`
    font-size: 28px;
    line-height: 40px;
    padding-left: 15px;

    @media (max-width: 768px){
        font-size: 22px;
        line-height: 30px;
    }
`

const ItemContent = styled.div`
    transform: translateY(-40px);
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    img{
        max-width: 100%;
    }
`

const Develope = styled.div`
    padding: 100px 0 0;
    position: relative;
    overflow: hidden;

    @media(max-width:1198px) {
        overflow: visible;
        padding: 0;
    }
`

const DevelopeFlex = styled.div`
    display: flex;
    justify-content: space-evenly;
    align-items: center;
    img {
        @media(max-width:1198px) {
            position: absolute;
            transform: translate(70%,60%);
        max-width: 300px;
        }

        @media(max-width: 764px){  
            max-width: 240px;
            bottom: 0;
            right: 0;
            transform: translate(0, 5%);
        }
        @media(max-width: 500px){  
            bottom: 0;
            right: 0;
            transform: translate(0, 15%);
        }
    }

`

const DevelopeItem = styled.div`
    width: 826px;
    padding: 45px 90px;
    box-sizing: border-box;
    background-color: #fff;
    box-shadow: 0px 3px 6px 0px #00000016;
    position: relative;
    &::before{
        position: absolute;
        content: '„';
        top: 15px;
        left: 40px;
        font-size: 60px;
        font-weight: bold;
        color: #FFE04F;

        @media(max-width:767px) {
        left: 15px;
    }
    }

    @media(max-width:1198px) {
        padding: 60px 65px;
    }
    @media(max-width:500px) {
        padding: 35px 45px;
    }
`

const DevelopeQuote = styled.p`
    font-size: 22px;
    line-height: 30px;

    @media(max-width:1198px) {
        font-size: 14px;
        line-height: 20px;
    }
    @media(max-width:767px) {
        padding-top: 30px;
    }
`

const DevelopeAuthor = styled.p`
    padding-top: 60px;
    font-weight: bold;
    font-size: 22px;
    line-height: 30px;

    @media(max-width:1198px) {
        padding-top: 30px;
    }
`


const BottomBG = styled.div`
    position: absolute;
    background-image: url(${Background});
    background-repeat: no-repeat;
    background-size: cover;
    height: 367px;
    width: 102%;
    bottom: 35%;
    left: -1%;
    z-index: -1;
    @media(max-width:1198px) {
        background-size: 0;
    }
`

const AdditionalContainer = styled(Container)`
    max-width: 804px;
    padding-bottom: 0;
    padding-top: 0;
    @media(max-width: 1198px){
        padding-top: 140px;
    }
`

const AdditionalContent = styled.article`
    h1,h2,h3,h4,h5,h6{
        font-size: 28px;
        font-weight: bold;
        line-height: 40px;
        color: #000;
        padding-bottom: 15px;
        padding-top: 50px;
        
        @media(max-width: 764px){
            font-size: 22px;
            line-height: 30px;
        }
    }


    blockquote{
        box-sizing: border-box;
        padding: 30px 80px;
        border-radius: 12px;
        background-color: #377DFF;
        position: relative;
        &::before{
            content: '„';
            position: absolute;
            color: #fff;
            font-size: 60px;
            line-height: 80px;
            left: 10px;
            top: -40px;
        }
        p{
            color: #fff;
            font-size: 22px;
            line-height:30px;
            padding: 0;
            @media(max-width: 764px){
                font-size: 18px;
                line-height: 28px;
            }
        }
        cite{
            color: #fff;
            text-align: right;
        }

        @media(max-width: 764px){
            padding: 20px 40px;
        }
    }

    figure{
        blockquote{
            box-sizing: border-box;
            padding: 30px;
            border-radius: 12px;
            background-color: #fff;
            position: relative;
            border: 2px solid #377DFF;

            &::before{
                display: none;
            }

            p{
                color: #000;
                font-size: 22px;
                line-height:30px;
                padding: 0;
                @media(max-width: 764px){
                    font-size: 18px;
                    line-height: 28px;
                }

            }
            cite{
                color: #000;
                text-align: right;
            }
        }
    }

    p{
        padding: 15px 0;
        color: #6E7276;
        font-size: 16px;
        line-height: 24px;
        @media(max-width: 764px){
            font-size: 14px;
            line-height: 22px;
        }
    }
    img{
        margin-top: 45px;
        max-width: 804px;
        width: 100%;
        height: auto;
    }
    a{
        color: #377DFF;
        font-size: 16px;
        line-height: 24px;
    }
    ol{
        li{
            list-style: decimal;
            margin-left: 20px;
        }
    }
    ul{
        border-radius: 12px;
        li{
            margin-left: 20px;
            &::before{
                content: "•";
                color: #377DFF;
                display: inline-block;
                width: 1em;
                margin-left: -1em;
            }
        }

        a{
            color: #000000;
            font-size: 22px;
            line-height: 30px;
            display: block;
            padding: 4px 0;
            margin: 4px 0;
            @media(max-width: 1198px){
                font-size: 18px;
                line-height: 28px;
            }
        }
    }
`



const CampaignsItem = ({ state, libraries }) => {
    const Html2React = libraries.html2react.Component
    const data = state.source.get(state.router.link)
    const post = state.source[data.type][data.id]
    // const props = state.source[data.type][data.id]

    return (
        <main className='body'>
            <SEO seo={post.acf.seo} date={post.date_gmt} />
            <Hero acf={post.acf.hero} form_free_test={post.acf.form_free_test} />
            <Video acf={post.acf.second_video} />
            <About acf={post.acf.third_about} />
            <Features acf={post.acf.features} />
            <Integration acf={post.acf.sixth_integration} />
            <Bussinesses acf={post.acf.bussinesses} />
            <Awards acf={post.acf.awards} />

        </main>
    )
}

export default connect(CampaignsItem)