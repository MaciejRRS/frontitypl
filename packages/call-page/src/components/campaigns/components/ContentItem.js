import React from 'react'
import { styled } from "frontity"
import NavLink from "@frontity/components/link"


const ItemTitle = styled.h2`
    padding: 20px 30px 20px;
    font-size: 18px;
    
    @media (min-width: 992px){
        font-size: 16px;
        line-height: 24px;
        padding-bottom: 10px;
    }
`

const ItemText = styled.p`
    margin: 0 30px;
    font-size: 16px;
    color: #6E7276;
    display: -webkit-box;
    -webkit-box-orient: vertical;
    -webkit-line-clamp: 3;
    overflow: hidden;
    margin-bottom: 130px;
    
    @media (min-width: 992px){
        font-size: 14px;
        line-height: 24px;
    }
`

const InnerLinkWrapper = styled.div`
    padding: 30px;
    position: absolute;
    bottom: 0;
    left: 0;
    right: 0;

`

const InnerLink = styled(NavLink)`
    color: #000000;
    font-size: 18px;
    line-height: 60px;
    font-weight: bold;
    box-shadow: 0 3px 6px 0 #D7E5FF;
    border: 2px solid #377DFF;
    border-radius: 6px;
    width: 248px;
    text-align: center;
    display: block;
    margin: 0 auto;
    transition: .2s linear;

    &:hover {
        background-color: #377DFF;
        color: #fff;
    }

    @media(max-width: 1198px){
        line-height: 45px;
    }
`

const Link = styled(NavLink)`
    color: #000000;
`

const Empty = styled.div`
    width: 526px;
    height: 100%;
    @media(max-width: 1760px){
        width: 100%;
    }
`

const Item = styled.div`
    width: 526px;
    height: 100%;
    box-sizing: border-box;
    padding-top: 20px;
    box-shadow: 0 3px 6px 0 #00000016;
    border-radius: 6px;
    position: relative;

    @media(max-width: 1760px){
        width: 100%;
        min-height: 510px;
    }
`

const ImgWrap = styled.div`
    padding: 0 50px;
    box-sizing: border-box;
    min-height: 100px;
    display: flex;
    align-items: center;
    justify-content: center;
    height:300px;
    width:100%;
    img{
        object-fit: cover;
        height: 100%;
        width: 100%;
    }
`

const ContentItem = (props) => {
    
    const data = props.state.source[props.el.type][props.el.id]

    return (
        <Empty>
            <Link link={'/campaigns/' + data.slug}>
                <Item>
                    <ImgWrap><img alt={data.acf.img_alt} src={data.acf.preview.img} /></ImgWrap>
                    <ItemTitle>{data.acf.preview.title}</ItemTitle>
                    <ItemText>{data.acf.preview.text}</ItemText>
                    <InnerLinkWrapper>
                        <InnerLink link={'/campaigns/' + data.slug}>{data.acf.preview.button_text}</InnerLink>
                    </InnerLinkWrapper>
                </Item>
            </Link>
        </Empty>
    )
}

export default ContentItem