import React from "react"
import { connect, Global, css } from "frontity"
import Switch from "@frontity/components/switch"
import { loadable } from "frontity"
import Header from "./Header/Header"
import Footer from "./Footer/Footer"
import MainPage from "./MainPage/MainPage"
import { styled } from "frontity"
import Error from "./sub-pages/error/Error"


const Blog = loadable(() => import('./Blog/Blog'))

const BlogItem = loadable(() => import('./Blog/BlogItem'))

const Ebooks = loadable(() => import('./Ebooks/Ebooks'))

const EbooksItem = loadable(() => import('./Ebooks/EbooksItem'))

const CallCenter = loadable(() => import('./sub-pages/vsCallcenter/CallCenter'))

const Livecall = loadable(() => import('./sub-pages/vsLivecall/Livecall'))

const Livechats = loadable(() => import('./sub-pages/vsLivechats/Livechats'))

const Popups = loadable(() => import('./sub-pages/vsPopups/Popups'))

const Novocall = loadable(() => import('./sub-pages/vsNovocall/Novocall'))

const Terms = loadable(() => import('./sub-pages/terms/Terms'))

const TermsNew = loadable(() => import('./sub-pages/termsnew/TermsNew'))

const Sequrity = loadable(() => import('./sub-pages/sequrity/Sequrity'))

const Privacy = loadable(() => import('./sub-pages/privacy/Privacy'))

const Increase1 = loadable(() => import('./sub-pages/increase-1/Increase1'))

const Increase2 = loadable(() => import('./sub-pages/increase-2/Increase2'))

const Increase3 = loadable(() => import('./sub-pages/increase-3/Increase3'))

const Pricing = loadable(() => import('./sub-pages/pricing/Pricing'))

const Partner = loadable(() => import('./sub-pages/partner/Partner'))

const About = loadable(() => import('./sub-pages/about/About'))

const ApiDevelopers = loadable(() => import('./sub-pages/api-developers/ApiDevelopers'))

const Careers = loadable(() => import('./sub-pages/careers/Careers'))

const Demo = loadable(() => import('./sub-pages/demo/Demo'))

const Contact = loadable(() => import('./sub-pages/contact/Contact'))

const HowWorks = loadable(() => import('./sub-pages/howWorks/HowWorks'))

const Integrations = loadable(() => import('./sub-pages/integrations/Integrations'))

const CountryCovering = loadable(() => import('./sub-pages/country-covering/CountryCovering'))

const Features = loadable(() => import('./sub-pages/features/Features'))

const Industry = loadable(() => import('./sub-pages/industry/Industry'))

const CaseStudies = loadable(() => import('./case-studies/CaseStudies'))

const CaseStudiesItem = loadable(() => import('./case-studies/CaseStudiesItem'))



const Campaigns = loadable(() => import('./campaigns/Campaigns'))

const CampaignsItem = loadable(() => import('./campaigns/CampaignsItem'))



const Concreteindustry = loadable(() => import('./sub-pages/concreate-industry/ConcreateIndustry'))

const Customers = loadable(() => import('./sub-pages/customers/Customers'))

const VirtualCallCenter = loadable(() => import('./sub-pages/virtual-call-center/VirtualCallCenter'))

const Wrapper = styled.div`
    
    @media(max-width: 1440px){
        header, footer,
        .body{ 
            zoom: .8;
        }
    }

    @media(max-width: 1198px){
        header, footer,
        .body{
            zoom: 1;
        }
    }

    code {
        font-family: source-code-pro, Menlo, Monaco, Consolas, 'Courier New',
            monospace;
    }

    * {
        margin: 0;
        padding: 0;
        font-family: 'Lato', sans-serif;
        font-size: 16px;
        line-height: 30px;
        list-style: none;
        text-decoration: none;
    }

    main {
        margin-top: 100px;
    }
`

const Root = ({ state }) => {

    let data = state.source.get(state.router.link)
    // debugger
    return (
        <Wrapper>
            <Global styles={css` * { padding: 0; margin: 0; scroll-behavior: smooth;}`} />
            <Header />
            <Switch>
                <Error when={data.is404} />

                <MainPage when={data.isHome} />
                <CaseStudies when={data.isCasestudiesArchive || data.isCategoryCasestudies} />
                <Ebooks when={data.isWiedzaArchive} />
                <EbooksItem when={data.isWiedza} />
                <CaseStudiesItem when={data.isCasestudies} />

                <Campaigns when={data.isCampaignsArchive || data.isCategoryCampaigns} />
                <CampaignsItem when={data.isCampaigns} />

                <Concreteindustry when={data.isBranze} />
                <BlogItem when={data.isBlog} />
                <Blog when={data.isBlogArchive || data.isCategoryBlog} />


                <Livechats when={state.router.link.includes('/livechats-alternative/')} />
                <CallCenter when={state.router.link.includes('/callcenter-alternative/')} />
                <Popups when={state.router.link.includes('/popups-alternative/')} />
                <Livecall when={state.router.link.includes('/livecall-alternative/')} />
                <Novocall when={state.router.link.includes('/novocall-alternative/')} />
                <Terms when={state.router.link.includes('/regulamin/')} />
                <TermsNew when={state.router.link.includes('/regulamin-new/')} />
                <Sequrity when={state.router.link.includes('/przechowywanie-danych/')} />
                <Privacy when={state.router.link.includes('/polityka-prywatnosci-2018/')} />
                <Increase1 when={state.router.link.includes('/zwiekszenie-sprzedazy/')} />
                <Increase2 when={state.router.link.includes('/generowanie-leadow/')} />
                <Increase3 when={state.router.link.includes('/ulepszenie-ux/')} />
                <Pricing when={state.router.link.includes('/cennik/')} />
                <Partner when={state.router.link.includes('/partners/')} />
                <About when={state.router.link.includes('/o-nas/')} />
                <ApiDevelopers when={state.router.link.includes('/dla-programistow/')} />
                <Careers when={state.router.link.includes('/praca-callpage/')} />
                <CountryCovering when={state.router.link.includes('/country-covering/')} />
                <Features when={state.router.link.includes('/wszystkie-funkcje/')} />
                <Contact when={state.router.link.includes('/kontakt/')} />
                <Demo when={state.router.link.includes('/prezentacja/')} />
                <HowWorks when={state.router.link.includes('/jak-to-dziala/')} />
                <Integrations when={state.router.link.includes('/integracje/')} />
                <Industry when={state.router.link.includes('/branze/')} />
                <Customers when={state.router.link.includes('/klienci/')} />
                <VirtualCallCenter when={state.router.link.includes('/wirtualna-centrala')} />
                <VirtualCallCenter when={state.router.link.includes('/call-tracking')} />

            </Switch>
            <Footer />
        </Wrapper>
    )
}

export default connect(Root)