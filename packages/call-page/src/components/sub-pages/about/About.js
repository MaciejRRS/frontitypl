import React from 'react'
import { connect } from "frontity"
import Hero from './components/Hero'
import Values from './components/Values'
import Awards from '../../../../common/bricks/Awards'
import GetPoint from './components/GetPoint'
import Numbers from '../../../../common/bricks/Numbers'
import Vision from './components/Vision'
import Steps from './components/Steps'
import Team from './components/Team'
import SEO from '../../../../common/SEO/seo'

const About = ({ state }) => {

    let props
    const data = state.source.get(state.router.link)
    if (data.isReady) {
        props = state.source[data.type][data.id]
    }

    return (
        <>
            {data.isReady
                ? <main className='body'>
                    <SEO seo={props.acf.seo}  date={props.date_gmt}/>
                    <Hero acf={props.acf.hero} marginPhone={'60px'} marginTablet={'60px'} marginDesktop={'clamp(120px, 10vw, 200px);'} />
                    <Values acf={props.acf.Values} marginPhone={'90px'} marginTablet={'120px'} marginDesktop={'120px'} />
                    <GetPoint acf={props.acf.get_point} />
                    <Awards acf={props.acf.awards} />
                    <Numbers acf={props.acf.numbers} />
                    <Vision acf={props.acf.vision} />
                    <Steps acf={props.acf.steps} />
                    <Team acf={props.acf.team} />
                </main>
                : null
            }
        </>
    )
}

export default connect(About)