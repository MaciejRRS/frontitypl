import React from 'react'
import { styled } from "frontity"
import { Container } from '../../../../../common/styles'

const Article = styled.article`
    position: relative;
    padding-top: ${props => props.marginDesktop ? props.marginDesktop : '90px'};
    @media(max-width: 1198px){
        padding-top: ${props => props.marginTablet ? props.marginTablet : '60px'};
    }
    @media(max-width: 764px){
        padding-top: ${props => props.marginPhone ? props.marginPhone : '60px'};
    }
`

const TextCenter = styled.div`
    text-align: center;
    max-width: 800px;
    margin: 0 auto;
`

const Title = styled.h1`
    font-size: clamp(45px, 3.5vw, 60px);
    line-height: clamp(60px, 3.5vw, 80px);
    font-weight: bold;
    margin-bottom: 80px;
    @media(max-width: 1198px){
        font-size: 45px;
        line-height: 60px;
        margin-bottom: 40px;
    }
    @media(max-width: 764px){
        font-size: 28px;
        line-height: 40px;
        margin-bottom: 25px;
    }
`

const Subtitle = styled.h2`
    font-size: 45px;
    line-height: 60px;
    margin-bottom: 120px;
    @media(max-width: 1198px){
        font-size: 28px;
        line-height: 40px;
        margin-bottom: 90px;
    }
    @media(max-width: 764px){
        font-size: 22px;
        line-height: 30px;
        margin-bottom: 30px;
    }
`

const Arrow = styled.div`
    width: 0px;
    height: 0px;
    position: relative;
    left: 50%;
    margin: 40px 0;

    &::before,
    &::after{
        content: '';
        position: absolute;
        width: 20px;
        height: 1px;
        background-color: #377DFF;
        top: 0;
    }

    &::before{
        transform-origin: left;
        transform: rotateZ(-45deg);
        left: 0;
    }

    &::after{
        transform-origin: right;
        transform: rotateZ(45deg);
        right: 0;
    }
`

const Hero = (props) => {
    return (
        <Article marginDesktop={props.marginDesktop} marginTablet={props.marginTablet} marginPhone={props.marginPhone}>
            <Container>
                <TextCenter>
                    <Title>{props.acf.title}</Title>
                    <Subtitle>{props.acf.subtitle}</Subtitle>
                </TextCenter>
                <Arrow>
                </Arrow>
            </Container>
        </Article>
    )
}

export default Hero