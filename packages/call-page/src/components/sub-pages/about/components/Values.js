import React from 'react'
import { Container } from '../../../../../common/styles'
import { styled } from "frontity"

const Article = styled.article`
    position: relative;
    padding-top: ${props => props.marginDesktop ? props.marginDesktop : 'clamp(140px, 12vw, 240px);'};
    @media(max-width: 1198px){
        padding-top: ${props => props.marginTablet ? props.marginTablet : 'clamp(90px, 12vw, 140px);'};
    }
    @media(max-width: 764px){
        padding-top: ${props => props.marginPhone ? props.marginPhone : '90px'};
    }
`

const Title = styled.h2`
    text-align: center;
    font-size: clamp(45px, 3.5vw, 60px);
    line-height: clamp(60px, 3.5vw, 80px);
    font-weight: bold;
    padding: 0 0 100px;
    @media(max-width: 1198px){
        padding-bottom: 60px;
        font-size: 45px;
        line-height: 60px;
    }
    @media(max-width: 764px){
        font-size: 28px;
        line-height: 40px;
    }
`

const Text = styled.p`
    text-align: center;
    font-weight: bold;
    padding-top: 20px;
    font-size: 22px;
    @media(max-width: 764px){
        font-size: 18px;
    }
`

const Grid = styled.div`
    display: grid;
    grid-template-columns: 1fr 1fr 1fr 1fr 1fr 1fr;
    @media(max-width: 1198px){
        grid-template-columns: 1fr 1fr 1fr;
        grid-row-gap: 60px;
    }
    @media(max-width: 764px){
        grid-template-columns: 1fr 1fr;
    }
`

const Item = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    img {
        max-width: 120px;
        height: auto;
    }
`

const Values = (props) => {
    return (
        <Article marginDesktop={props.marginDesktop} marginTablet={props.marginTablet} marginPhone={props.marginPhone}>
            <Container>
                <Title>{props.acf.title}</Title>
                <Grid>
                    {
                        props.acf.repeater.map((el, index) =>
                            <Item key={index}>
                                <img alt={el.img_alt} src={el.img} />
                                <Text>{el.text}</Text>
                            </Item>
                        )
                    }
                </Grid>
            </Container>
        </Article>
    )
}

export default Values