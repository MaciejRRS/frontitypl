import React from 'react'
import { connect } from "frontity"
import Steps from './components/Steps'
import SEO from '../../../../common/SEO/seo'


const ApiDevelopers = ({ state }) => {

    let props
    const data = state.source.get(state.router.link)
    if (data.isReady) {
        props = state.source[data.type][data.id]
    }

    return (
        <>
            {data.isReady
                ? <main className='body'>
                    <SEO seo={props.acf.seo} date={props.date_gmt} />
                    <Steps acf={props.acf.content} />
                </main>
                : null
            }
        </>
    )
}

export default connect(ApiDevelopers)