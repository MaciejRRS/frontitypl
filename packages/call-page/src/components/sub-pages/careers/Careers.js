import React from 'react'
import { connect } from "frontity"
import Cover from '../../../../common/bricks/Cover'
import Grid from './components/Grid'
import Awards from '../../../../common/bricks/Awards'
import Values from './components/Values'
import Branchers from './components/Branchers'
import Hiring from './components/Hiring'
import SEO from '../../../../common/SEO/seo'

const Careers = ({ state }) => {

    let props
    const data = state.source.get(state.router.link)
    if (data.isReady) {
        props = state.source[data.type][data.id]
    }

    return (
        <>
            {data.isReady
                ? <main className='body'>
                    <SEO seo={props.acf.seo}   date={props.date_gmt}/>
                    <Cover acf={props.acf.cover} />
                    <Awards marginDesktop='180px' marginTablet='120px' marginPhone='65px' acf={props.acf.awards} />
                    <Grid acf={props.acf.grid} />
                    <Values acf={props.acf.Values} />
                    <Branchers acf={props.acf.branchers} />
                    <Hiring acf={props.acf.hiring} />
                </main>
                : null
            }
        </>
    )
}

export default connect(Careers)