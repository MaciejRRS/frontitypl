import React from 'react'
import { Container } from '../../../../../common/styles'
import { styled } from "frontity"

const Article = styled.article`
    position: relative;
    padding-top: ${props => props.marginDesktop ? props.marginDesktop : 'clamp(140px, 12vw, 240px);'};
    @media(max-width: 1198px){
        padding-top: ${props => props.marginTablet ? props.marginTablet : 'clamp(90px, 12vw, 140px);'};
    }
    @media(max-width: 764px){
        padding-top: ${props => props.marginPhone ? props.marginPhone : '90px'};
    }
`

const Title = styled.h2`
    text-align: center;
    font-size: clamp(45px, 3.5vw, 60px);
    line-height: clamp(60px, 3.5vw, 80px);
    font-weight: bold;
    padding: 0 0 100px;
    @media(max-width: 1198px){
        padding-bottom: 60px;
        font-size: 45px;
        line-height: 60px;
    }
    @media(max-width: 764px){
        font-size: 28px;
        line-height: 40px;
    }
`

const Grid = styled.div`
display: grid;
grid-template-columns: 1fr 1fr;
max-width: 1200px;
margin: 0 auto;
gap: 150px;

@media (max-width: 1198px) {
    grid-template-columns: 1fr;
    max-width: 525px;
    gap: 60px;
}

@media (max-width: 768px) {
        gap: 45px;
    }
`

const Item = styled.div`
text-align: center;

img {
    max-width: 248px;
    height: auto;
    display: block;
    margin: 0 auto 30px;

    @media (max-width: 768px) {
        max-width: 180px;
    }
}
`

const TitleItem = styled.h3`
margin-bottom: 15px;
font-size: 22px;
line-height: 30px;
font-weight: normal;
`

const Subtitle = styled.h4`
margin-bottom: 30px;
font-size: 16px;
line-height: 24px;
color: #377DFF;
font-weight: bold;
`

const Text = styled.p`
font-size: 16px;
line-height: 24px;
color: #6E7276;
`

const Branchers = (props) => {
    return (
        <Article marginDesktop={props.marginDesktop} marginTablet={props.marginTablet} marginPhone={props.marginPhone}>
            <Container>
                <Title>{props.acf.title}</Title>
                <Grid>
                    {
                        props.acf.repeater.map((el, index) =>
                            <Item key={index}>
                                <img alt={el.img_alt} src={el.img} />
                                <TitleItem>{el.title}</TitleItem>
                                <Subtitle>{el.subtitle}</Subtitle>
                                <Text>{el.text}</Text>
                            </Item>
                        )
                    }
                </Grid>
            </Container>
        </Article>
    )
}

export default Branchers