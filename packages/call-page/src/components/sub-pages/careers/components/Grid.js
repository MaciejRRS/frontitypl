import React from 'react'
import { Container } from '../../../../../common/styles'
import { styled } from "frontity"
import Youtube from 'react-lazyload-youtube'
import PlayIcon from '../../../../../common/sprites/play.svg'

const Article = styled.article`
    position: relative;
    margin-top: 210px;
    @media(max-width: 1198px){
        margin-top: 105px;
    }
    @media(max-width: 764px){
        margin-top: 60px;
    }
`

const Box = styled.div`
    width: 100%;

    img {
        max-width: 100%;
        height: auto;

        &.hide764 {
            display: none;
            @media(max-width: 764px) {
                display: block
            }
        }
    }
    
`

const Flex = styled.div`
    display: flex;
    justify-content: space-between;
    max-width: 1000px;
    margin: 0 auto 30px;

    & div {
        &:first-child {

            @media (max-width: 764px) {
                margin-right:10px;
            }
        }

        &:last-child {
        display: none;

            @media (max-width: 764px) {
                margin-left:10px;
                display: block;
            }
        }
    }


    @media(max-width: 1640px) {
    justify-content: space-evenly;
    }

    @media(max-width: 1198px) {
        margin: 0 auto 15px;
    }

    @media(max-width: 764px) {
        justify-content: space-between;
        margin: 0 auto 10px;
    }
`

const FlexCenter = styled.div`
    display: flex;
    align-items: center;
    margin-bottom: 60px;
    width: 100%;
    justify-content: center;

@media(max-width: 1198px) {
    margin-bottom: 30px;
}

@media (max-width: 764px) {
    margin-bottom: 10px;
        & div {
            &:first-child {
            width: 0;
            height: 0;
            display: none;
            }


        &:last-child {
            width: 0;
            height: 0;
            display: none;
        }
    }
}


`
const FlexBottom = styled.div`
    display: flex;
    justify-content: center;
    max-width: 1000px;
    margin: 0 auto;

    @media(max-width: 764px) {
       justify-content: space-between;        
    }
    & div {
        &:first-child {

            @media (max-width: 764px) {
                margin-right:10px;
            }
        }
        &:last-child {
        display: none;

            @media (max-width: 764px) {
                margin-left:10px;
                display: block;
            }
        }
    }   
`


const ImgItem = styled.div`
    width: 420px;

    @media(max-width: 1640px) {
        width: 360px;
    }

    @media(max-width: 1198px) {
    width: 25%;
    }

    @media(max-width: 764px) {
        width: calc(33.33% - 5px);
    }


.img_top_left {
        margin-right: 140px;

        @media(max-width: 1198px) {
        margin-right: 100px;
        }

        @media(max-width: 764x) {
        margin-right: 0;
        }
    }

    .img_top_right {
        position: relative;
        bottom: 30px;
        margin-left: 30px;

        @media(max-width: 1198px) {
        margin-left: 15px;
        }

        @media(max-width: 764px) {
        margin-left: 0;
        bottom: 0;
        }
    }

    .img_right_center {
    }


    .img_bottom_right {
        margin-left: 140px;
        position: relative;
        top: -30px;

        @media(max-width: 1198px) {
            margin-left: 115px;
        }

        @media(max-width: 764px) {
            margin-left: 0;
            top: 0;        
        }
    }



    .img_bottom_left {
        margin-right: 30px;

    @media(max-width: 1198px) {
        margin-right: 15px;
        }
    }

    .img_left_center {
    }
`
const ImgItemCenter = styled.div`

    width: 640px;
    height: 370px;
    position: relative;
    overflow: hidden;

    @media(max-width: 1640px) {
        width: 450px;
        height: 320px;
    }
        margin: 0 60px;

    @media(max-width: 1198px) {
        margin: 0 15px 15px;
        width: 50%;
        height: 300px;
    }

    @media(max-width: 764px) {
        width: 100%;
        margin: 0;

}


  div {
      cursor: pointer;
        height: 100%!important;
        width: 100%!important;
        background-position: center;
        background-size: cover;
        background-repeat: no-repeat;
        position: relative;
        display: block!important;   
    }




        iframe {
            position: absolute;
            left: 0;
            top: 0;
            width: 100%;
            height:100%;
            z-index: 999;

        }
`

const Play = styled.span`
    width: 120px;
    height:85px;
    position: absolute;
    left: 50%;
    top: 50%;
    transform: translate(-50%, -50%);
    pointer-events: none;

    &:before{
        content: url(${PlayIcon});
        cursor: pointer;
    }
`




const Grid = (props) => {
    // debugger
    return (
        <Article marginDesktop={props.marginDesktop} marginTablet={props.marginTablet} marginPhone={props.marginPhone}>
            <Container>
                <Box>
                    <Flex>
                        <ImgItem><img className="img_top_left" alt={props.acf.img_top_left_alt} src={props.acf.img_top_left} /></ImgItem>
                        <ImgItem><img className="img_top_right" alt={props.acf.img_top_right_alt} src={props.acf.img_top_right} /></ImgItem>

                        <ImgItem><img className="img_left_center" alt={props.acf.img_left_center_alt} src={props.acf.img_left_center} /></ImgItem>
                    </Flex>

                    <FlexCenter>
                        <ImgItem><img className="img_left_center" alt={props.acf.img_left_center_alt} src={props.acf.img_left_center} /></ImgItem>
                        <ImgItemCenter><Youtube videoId={props.acf.video} />
                            <Play></Play>
                        </ImgItemCenter>

                        <ImgItem><img className="img_right_center" alt={props.acf.img_right_center_alt} src={props.acf.img_right_center} /></ImgItem>
                    </FlexCenter>

                    <FlexBottom>
                        <ImgItem><img className="img_bottom_left" alt={props.acf.img_bottom_left_alt} src={props.acf.img_bottom_left} /></ImgItem>
                        <ImgItem><img className="img_bottom_right" alt={props.acf.img_bottom_right_alt} src={props.acf.img_bottom_right} /></ImgItem>

                        <ImgItem><img className="img_right_center" alt={props.acf.img_right_center_alt} src={props.acf.img_right_center} /></ImgItem>

                    </FlexBottom>

                </Box>
            </Container>
        </Article>
    )
}

export default Grid