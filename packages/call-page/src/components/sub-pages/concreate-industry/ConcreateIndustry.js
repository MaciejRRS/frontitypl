import React from 'react'
import TrustedBy from '../../../../common/bricks/TrustedBy'
import Clients from '../../../../common/bricks/Clients'
import Increase from '../../../../common/bricks/Increase'
import About from '../../../../common/bricks/About'
import Features from '../../../../common/bricks/Features'
import Integration from '../../../../common/bricks/Integration'
import Awards from '../../../../common/bricks/Awards'
import Develope from '../../../../common/bricks/Develope'
import SEO from '../../../../common/SEO/seo'
import { connect } from "frontity"

const ConcreteIndustry = ({ state }) => {

    const data = state.source.get(state.router.link)
    const props = state.source[data.type][data.id]
    debugger
    return (
        <main className='body'>
            <SEO seo={props.acf.seo} date={props.date_gmt} />
            <TrustedBy acf={props.acf.hero} />
            <Clients acf={props.acf.clients} />
            <Increase acf={props.acf.fourth_increase} />
            <About acf={props.acf.third_about} />
            <Features acf={props.acf.features} />
            <Integration acf={props.acf.sixth_integration} />
            <Develope acf={props.acf.develope} />
            <Awards marginDesktop='240px' marginTablet='260px' marginPhone='180px' acf={props.acf.awards} />
        </main>
    )
}

export default connect(ConcreteIndustry)