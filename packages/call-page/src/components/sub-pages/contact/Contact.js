import React from 'react'
import { connect } from "frontity"
import Form from './components/Form'
import SEO from '../../../../common/SEO/seo'

const Contact = ({ state }) => {

    let props
    const data = state.source.get(state.router.link)
    if (data.isReady) {
        props = state.source[data.type][data.id]
    }

    return (
        <>
            {data.isReady
                ? <main>
                    <SEO seo={props.acf.seo} date={props.date_gmt} />
                    <Form acf={props.acf} />
                </main>
                : null
            }
        </>
    )
}

export default connect(Contact)