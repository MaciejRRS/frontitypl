import React from 'react'
import { styled } from "frontity"


const Article = styled.article`
    padding-top: ${props => props.marginDesktop ? props.marginDesktop : '120px'};
    @media(max-width: 1198px){
        padding-top: ${props => props.marginTablet ? props.marginTablet : '60px'};
    }
    @media(max-width: 764px){
        padding-top: ${props => props.marginPhone ? props.marginPhone : '60px'};
    }
`

const Title = styled.h1`
    font-size: clamp(45px, 3.5vw, 60px);
    line-height: clamp(60px, 3.5vw, 80px);
    text-align: center;
    @media(max-width: 1198px){
        font-size: 45px;
        line-height: 60px;
    }
    @media(max-width: 764px){
        font-size: 28px;
        line-height: 40px;
    }
`

const SubTitle = styled.h2`
    font-size: 28px;
    line-height: 40px;
    color: #6E7276;
    font-weight: normal;
    text-align: center;
    padding: 50px 0 120px;
    @media(max-width: 1198px){
        font-size: 28px;
        line-height: 40px;
        padding: 30px 0 90px;
    }
    @media(max-width: 764px){
        font-size: 28px;
        line-height: 40px;  
    }
`

const Grid = styled.div`
    display: grid;
    grid-template-columns: 2fr 1fr 1fr 1fr;
    grid-column-gap: 30px;
    max-width: 1640px;
    width: calc(100% - 120px);
    margin: 0 auto;
    @media(max-width: 1760px){
        margin: 0 60px;
    }
    @media(max-width: 1198px){
        grid-template-columns: 1fr 1fr 1fr 1fr;
    }
    @media(max-width: 764px){
        grid-column-gap: 10px;
        margin: 0 15px;
        width: calc(100% - 30px);
        
    }
`

const NameGrid = styled(Grid)`
    position: sticky;
    top: 105px;
    @media(max-width: 764px){
        top: 100px;
        background-color: #fff;
        margin: 0;
        padding: 0 15px;
        font-size: 12px;
    }
`


const ContainerGrid = styled.div`
    pre:nth-child(even){
        background-color: #F0F0F0;
    }
`

const ColumnName = styled.div`
    text-align: center;
    border-radius: 3px;
    border: 2px solid #377DFF;
    line-height: 30px;
    height: 60px;
    font-weight: bold;
    display: flex;
    justify-content: center;
    align-items: center;
    box-sizing: border-box;
    color: ${props => props.first ? '#FFFFFF' : '#000000'};
    background-color: ${props => props.first ? '#377DFF' : '#FFFFFF'};
    @media(max-width: 1640px){
        display: ${props => props.add ? 'none' : 'flex'};
    }
    @media(max-width: 764px){
        font-size: 12px;
        border: unset;
        background-color: transparent;
        color: ${props => props.first ? '#377DFF' : '#000000'};
        line-height: 20px;
        height: 60px;
    }
`

const ColumnCale = styled.div`
    text-align: center;
    height: 60px;
    font-size: ${props => props.first ? '22px' : '16px'};
    line-height: 60px;
    display: flex;
    justify-content: center;
    align-items: center;
    color: ${props => props.color};
    font-weight: ${props => props.first ? 'bold' : 'normal'};
    @media(max-width: 1640px){
    }
    @media(max-width: 1198px){
        font-size: 18px;
    }
    @media(max-width: 764px){
        font-size: 10px;
        padding-left: 15px;
    }
`


const CaleGrid = styled(Grid)`

`

const AdditionalInform = styled.div`
    display: none;
    @media(max-width: 1640px) {
        transition: .3s linear;
        display: block;
        height: 0;
        opacity: 0;
        pointer-events: none;
        grid-column: 1 / -1;
        div{
            padding: 6px 0;
        }
    @media(max-width: 1198px){
        b, div{
            font-size: 16px;
        }
    }
    @media(max-width: 764px){
        b, div{
            font-size: 12px;
        }
    }
}
`

const True = styled.span`
    font-size: 16px;
    line-height: 60px;
    color: #377DFF;
    font-weight: bold;
    @media(max-width: 1198px){
        font-size: 18px;
        line-height: 40px;
    }
    @media(max-width: 764px){
        font-size: 12px;
        line-height: 30px;
    }
`

const False = styled.span`
    font-size: 16px;
    line-height: 60px;
    color: #6E7276;
    font-weight: bold;
    @media(max-width: 1198px){
        font-size: 18px;
        line-height: 40px;
    }
    @media(max-width: 764px){
        font-size: 12px;
        line-height: 30px;
    }
`

const Content = (props) => {
    return (
        <Article>
            <Title>{props.acf.title}</Title>
            <SubTitle>{props.acf.text}</SubTitle>
            <div>
                <NameGrid>
                    {props.acf.column_names.map((el, index) =>
                        <React.Fragment key={index}>
                            <ColumnName first={true}>
                                {el.column_1}
                            </ColumnName>
                            <ColumnName>
                                {el.column_2}
                            </ColumnName>
                            <ColumnName>
                                {el.column_3}
                            </ColumnName>
                            <ColumnName>
                                {el.column_4}
                            </ColumnName>
                        </React.Fragment>
                    )}
                </NameGrid>
                <ContainerGrid>
                    {props.acf.column_content.map((el, index) =>
                        <pre key={index}>
                            <CaleGrid>
                                <ColumnCale first={true}>
                                    {el.column_1}
                                </ColumnCale>
                                <ColumnCale>
                                    {
                                        el.column_2 ? <True>{el.column_2}</True> : <False>✗</False>
                                    }
                                </ColumnCale>
                                <ColumnCale>
                                    {
                                        el.column_3 ? <True>{el.column_3}</True> : <False>✗</False>
                                    }
                                </ColumnCale>
                                <ColumnCale>
                                    {
                                        el.column_4 ? <True>{el.column_4}</True> : <False>✗</False>
                                    }
                                </ColumnCale>
                            </CaleGrid>
                        </pre>
                    )}
                </ContainerGrid>
            </div>
        </Article>
    )
}


export default Content