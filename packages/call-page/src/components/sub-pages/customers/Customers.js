import React, { useState, useEffect } from 'react'
import { connect } from "frontity"

import Hero from './components/Hero'
import UseStates from './components/UseStates'
import BenefitsIndustry from './components/BenefitsIndustry'
import Awards from '../../../../common/bricks/Awards'
import Numbers from './components/Numbers'
import BlogPart from './components/BlogPart'
import SEO from '../../../../common/SEO/seo'

const Customers = ({ state }) => {

    let props
    const posts = state.source.get('/casestudies/')
    const data = state.source.get(state.router.link)

    if (data.isReady) {
        props = state.source[data.type][data.id]
    }

    const [productsPrototype, productsPrototypeChange] = useState([...posts.items])
    const [products, productsChange] = useState([...productsPrototype]);

    const FilterByIntegrations = (value) => {
        productsChange(productsPrototype.filter(el => {
            const data = state.source[el.type][el.id]
            return data.acf.type === value
        }))
    }

    debugger
    let caseStudies = []
    for (let i = 0; i < 4; i++) {
        if (products[i]) {
            caseStudies.push(products[i])
        }
    }

    return (
        <>
            {
                data.isReady
                    ? <main className='body'>
                        <SEO seo={props.acf.seo} date={props.date_gmt} />
                        <Hero acf={props.acf.hero} />
                        <UseStates FilterByIntegrations={FilterByIntegrations} acf={props.acf.examples} />
                        <BlogPart state={state} caseStudies={caseStudies} />
                        <BenefitsIndustry acf={props.acf.benefits_industry} />
                        <Awards marginDesktop='180px' marginTablet='120px' marginMobile='90px' acf={props.acf.awards} />
                        <Numbers acf={props.acf.numbers} />
                    </main>
                    : null
            }
        </>
    )
}

export default connect(Customers)