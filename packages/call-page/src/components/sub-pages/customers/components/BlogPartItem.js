import React from 'react'
import { styled } from "frontity"
import NavLink from "@frontity/components/link"


const Item = styled.div`
    width: 387px;
    box-sizing: border-box;
    box-shadow: 0 3px 6px 0 #00000016;
    border-radius: 6px;
    padding: 15px 0 30px 0;
    height: 100%;
    position: relative;
    div{
        min-height: 100px;
        display: flex;
        align-items: center;
        justify-content: center;
        padding: 0 50px 0;
        box-sizing: border-box;
        img{
            max-width: 100%;
            max-height: 100px;
        }
    }

    @media(max-width: 1760px){
        margin: 0 auto 30px;
        width: 587px;
    }
    
    @media(max-width: 1198px){
        width: 387px;
    }

    @media(max-width: 1000px){
        max-width: 587px;
        width: 100%;
    }
`


const ItemTitle = styled.h2`
    padding: 20px 30px 20px;
    font-size: 18px;
    @media(max-width: 764px){
        font-size: 16px;
        line-height: 24px;
    }
`

const ItemText = styled.p`
    padding: 0 30px;
    font-size: 16px;
    line-height: 24px;
    margin-bottom: 120px;
    display: -webkit-box;
    -webkit-box-orient: vertical;
    -webkit-line-clamp: 5;
    overflow: hidden;

    @media(max-width: 764px){
        font-size: 14px;
        line-height: 20px;
    }
`

const Link = styled(NavLink)`
    color: #000000;
    @media(max-width:1760px) {
        margin-bottom: 30px;
    }
`

const InnerLink = styled(NavLink)`
    margin: 0 auto;
    display: block;
    padding: 15px 0;
    width: 248px;
    text-align: center;
    color: #000000;
    border: 2px solid #377DFF;
    border-radius: 6px; 
    font-weight: bold;
    transition: .1s linear;
    position: absolute;
    bottom: 30px;
    left: 0;
    right: 0;

    &:hover {
        border-color: #377DFF;
        background-color: #377DFF;
        color: #FFFFFF;
    }



    @media(max-width: 764px){
        padding: 10px 0;
        font-size: 16px;
    }
`

const BlogPartItem = (props) => {

    const data = props.state.source[props.el.type][props.el.id]

    return (
        <Link link={'/casestudies/' + data.slug}>
            <Item>
                <div><img alt={data.acf.preview.img_alt} src={data.acf.preview.img} /></div>
                <ItemTitle>{data.acf.preview.title}</ItemTitle>
                <ItemText>{data.acf.preview.text}</ItemText>
                <InnerLink link={'/casestudies/' + data.slug}>{'Czytaj dalej >>'}</InnerLink>
            </Item>
        </Link>
    )
}

export default BlogPartItem