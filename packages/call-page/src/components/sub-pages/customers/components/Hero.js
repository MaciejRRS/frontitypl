import React from 'react'
import { styled } from "frontity"
import { Flex, Link } from '../../../../../common/styles';
import GoogleIcon from './../../../../../common/sprites/Google_icon.png'

const HeroBG = styled.div`
    background-image: url(${props => props.BackGround});
    background-repeat: no-repeat;
    background-size: contain;
    background-position: center;
    width: 100%;
    height: calc(100% - 50px);
    padding-top: 50px;
    @media(max-width: 1198px){
        background: unset;
    }
`

const MobileImg = styled.img`
    display: none;
    @media(max-width: 1198px){
        display: block;
        margin: 60px auto 0;
        width: 100%;
        max-width: 700px;
    }
`

const TextCenter = styled.div`
    text-align: center;
    max-width: 800px;
    margin: 0 auto;
`

const Title = styled.h1`
    font-size: clamp(45px, 3.5vw, 60px);
    line-height: clamp(60px, 3.5vw, 80px);
    font-weight: bold;
    margin-bottom: 120px;
    @media(max-width: 1198px){
        font-size: 45px;
        line-height: 60px;
        margin-bottom: 60px;
    }
    @media(max-width: 768px){
        font-size: 28px;
        line-height: 40px;
    }
`

const Subtitle = styled.h2`
    font-size: 45px;
    line-height: 60px;
    margin-bottom: 60px;
    font-weight: bold;
    @media(max-width: 1198px){
        font-size: 28px;
        line-height: 40px;
        margin-bottom: 30px;
    }
    @media(max-width: 768px){
        font-size: 22px;
        line-height: 30px;
    }
`

const Text = styled.p`
    font-size: 28px;
    line-height: 40px;
    color: #6E7276;
    margin-bottom: 60px;
    @media(max-width: 1198px){
        font-size: 18px;
        line-height: 28px;
    }
    @media(max-width: 768px){
        font-size: 14px;
        line-height: 20px;
    }
    
`

const Container = styled.div`
    max-width: 1780px;
    width: calc(100% - 140px);
    margin: 0 auto;
    @media(max-width: 564px){
        width: calc(100% - 30px);
    }
`

const LocFlex = styled(Flex)`
    max-width: 800px;
    margin: 0 auto;
    justify-content: space-evenly;

    @media(max-width: 886px){
        flex-direction: column;
    }
`

const FlexItem = styled.div`
    padding-left: 25px;
    position: relative;
    @media(max-width: 1198px){
        font-size: 14px;
    }

    &::before{
        content: '✓';
        position: absolute;
        left: 0;
        color: #377DFF;
    }
`

const FirstButton = styled(Link)`
    position: relative;
    padding: 12px 50px;
    border: 3px solid #377DFF;
    background-color: #377DFF;
    border-radius: 6px;
    font-weight: bold;
    font-size: 18px;
    color: #fff;
    margin-right: 30px;
    box-shadow: 0 3px 6px 0 #D7E5FF;
    cursor: pointer;
    border-radius: 6px;
    text-align: center;
    &:before{
        position: absolute;
        /* content: url(${GoogleIcon}); */
        left: -3px;
        top: -3px;
        height: 60px;
        transform: translateX(-100%);
        background-color: #377DFF;
        border-bottom-left-radius: 6px;
        border-top-left-radius: 6px;
    }

    @media(max-width: 1198px){
        font-size: 16px;
    }

    @media(max-width: 764px){
        padding: 12px 10px;
    }

    @media(max-width: 640px){
        /* margin-left: 60px; */
        margin-right: 0;
        margin-bottom: 30px;
    }
`

const SecondButton = styled(Link)`
    padding: 12px 50px;
    border: 2px solid #377DFF;
    background-color: #FFFFFF;
    color: #000000;
    border-radius: 6px;
    font-weight: bold;
    font-size: 18px;
    box-shadow: 0 3px 6px 0 #D7E5FF;
    cursor: pointer;
    /* margin-right: -60px; */
    transition: all.2s linear;
    text-align: center;

&:hover {
    border-color: #377DFF;
    background-color: #377DFF;
    color: #FFFFFF;
}
    @media(max-width: 1198px){
        font-size: 16px;
    }
    @media(max-width: 764px){
        padding: 12px 10px;
    }
    @media(max-width: 640px){
        margin-right: 0px;
    }
`

const ButtonFlex = styled.div`
    display: flex;
    justify-content: center;
    padding-bottom: 30px;
    @media(max-width: 640px){
        flex-direction: column;
    }
`


const Grid = styled.div`
display: grid;
grid-template-columns: 1fr 2fr 1fr;
align-items: center;
gap: 15px;

@media(max-width: 1198px) {
    grid-template-columns: 1fr;
}
`

const ImgLeft = styled.div`
img {
    max-width: 100%;
    height: auto;
}

@media (max-width:1198px) {
    display: none;
}
`

const ImgRight = styled.div`
img {
    max-width: 100%;
    height: auto;
}
@media (max-width:1198px) {
    display: none;
}
`




const Button = styled.a`
    position: relative;
    padding: 12px 50px;
    border: 3px solid #377DFF;
    background-color: #377DFF;
    border-radius: 6px;
    font-weight: bold;
    font-size: 18px;
    color: #fff;
    margin-right: 30px;
    box-shadow: 0 3px 6px 0 #D7E5FF;
    cursor: pointer;
    border-radius: 6px;
    text-align: center;
    &:before{
        position: absolute;
        /* content: url(${GoogleIcon}); */
        left: -3px;
        top: -3px;
        height: 60px;
        transform: translateX(-100%);
        background-color: #377DFF;
        border-bottom-left-radius: 6px;
        border-top-left-radius: 6px;
    }

    @media(max-width: 1198px){
        font-size: 16px;
    }

    @media(max-width: 764px){
        padding: 12px 10px;
    }

    @media(max-width: 640px){
        /* margin-left: 60px; */
        margin-right: 0;
        margin-bottom: 30px;
    }
`



const Hero = (props) => {

    const queryForm = function (settings) {
        var reset = settings && settings.reset ? settings.reset : true;
        var self = window.location.toString();
        var querystring = self.split("?");
        if (querystring.length > 1) {
            var pairs = querystring[1].split("&");
            for (i in pairs) {
                var keyval = pairs[i].split("=");
                if (reset || sessionStorage.getItem(keyval[0]) === null) {
                    sessionStorage.setItem(keyval[0], decodeURIComponent(keyval[1]));
                }
            }
        }
        var hiddenFields = document.querySelectorAll("input[type=hidden], input[type=text]");
        console.log(hiddenFields)
        for (var i = 0; i < hiddenFields.length; i++) {
            var param = sessionStorage.getItem(hiddenFields[i].name);
            if (param) document.getElementsByName(hiddenFields[i].name)[0].value = param;
        }
    }

    function goToRegister(action) {
        queryForm();
        var form = document.getElementById('goToRegisterForm');
        form.action = action;
        form.submit();
    }


    return (
        <article>
            <Container>
                <Grid>
                    <ImgLeft><img alt={props.acf.img_left_alt} src={props.acf.img_left} /></ImgLeft>

                    <HeroBG BackGround={props.acf.background_img}>
                        <TextCenter>
                            <Title>{props.acf.title}</Title>
                            <Subtitle>{props.acf.subtitle}</Subtitle>
                            <Text>{props.acf.text}</Text>
                        </TextCenter>
                        <ButtonFlex>
                            {
                                props.acf.first_button_type
                                    ? <FirstButton link={props.acf.first_button_link}>
                                        {props.acf.first_button_text}
                                    </FirstButton>
                                    : <Button rel="noreferrer" onClick={() => { goToRegister(props.acf.first_button_link) }}>
                                        {props.acf.first_button_text}
                                    </Button>
                            }
                            {
                                props.acf.second_button_type
                                    ? <SecondButton link={props.acf.second_button_link}>
                                        {props.acf.second_button_text}
                                    </SecondButton>
                                    : <SecondButton as='a' rel="noreferrer" target='_blank' href={props.acf.second_button_link}>
                                        {props.acf.second_button_text}
                                    </SecondButton>
                            }
                        </ButtonFlex>
                        <LocFlex>
                            {
                                props.acf.repeater.map((el, index) =>
                                    <FlexItem key={index}>
                                        {el.text}
                                    </FlexItem>
                                )
                            }
                        </LocFlex>
                        <MobileImg alt={props.acf.img_alt} src={props.acf.mobile_img} />
                    </HeroBG>
                    <ImgRight><img alt={props.acf.img_right_alt} src={props.acf.img_right} /></ImgRight>
                </Grid>


            </Container>

        </article >
    )
}

export default Hero