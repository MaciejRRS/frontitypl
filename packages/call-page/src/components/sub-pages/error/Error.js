import React, { useState, useEffect } from 'react'
import { styled } from "frontity"
import { Container, Flex, Link } from '../../../../common/styles'
import SEO from '../../../../common/SEO/seo'

const LocFlex = styled(Flex)`
    justify-content: space-evenly;

    @media(max-width: 996px){
        justify-content: center;
        padding-top: 120px;
        img{
            display: none;
        }
    }
`

const Title = styled.h1`
    font-size: clamp(45px, 3.5vw, 60px);
    line-height: clamp(60px, 3.5vw, 80px);
    @media(max-width: 996px){
        text-align: center;
        font-size: 45px;
        line-height: 60px;
    }
    @media(max-width: 470px){
        font-size: 28px;
        line-height: 40px;
    }
`

const Text = styled.p`
    font-size: 28px;
    line-height: 40px;
    padding-top: 20px;
    padding-bottom: 80px;
    color: #6E7276;
    max-width: 387px;
    @media(max-width: 996px){
        text-align: center;
        font-size: 22px;
        line-height: 30px;
        padding-bottom: 60px;
    }
    @media(max-width: 470px){
        font-size: 18px;
        line-height: 24px;
        padding-bottom: 40px;
    }
`

const LocLink = styled(Link)`
    @media(max-width: 996px){
        display: block;
        text-align: center;
    }
    @media(max-width: 470px){
        font-size: 16px;
    }
`

const Error = () => {

    const [acf, changeAcf] = useState(null)
    useEffect(() => {
        fetch("https://wp.callpage.pl/wp-json/acf/v3/pages/1858")
            .then(res => res.json())
            .then(res => changeAcf(res))
    }, [])

    return (
        <>
            {acf
                ? <>
                    <main className='body'>
                        <SEO seo={acf.acf.seo}   date={acf.date_gmt}/>
                        <Container>
                            <LocFlex>
                                <div>
                                    <Title>{acf.acf.title}</Title>
                                    <Text>{acf.acf.text}</Text>
                                    <LocLink to={acf.acf.button_link}>{acf.acf.button_text}</LocLink>
                                </div>
                                <div>
                                    <img alt={acf.acf.img_alt} src={acf.acf.img} />
                                </div>
                            </LocFlex>
                        </Container>
                    </main>
                </>
                : null}
        </>

    )
}

export default Error