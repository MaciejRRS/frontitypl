import React from 'react'
import Hero from './components/Hero'
import Params from './components/Params'
import { connect } from "frontity"
import SEO from '../../../../common/SEO/seo'

const Features = ({ state }) => {

    let props
    const data = state.source.get(state.router.link)
    if (data.isReady) {
        props = state.source[data.type][data.id]
    }

    return (
        <>
            {data.isReady
                ? <main className='body'>
                    <SEO seo={props.acf.seo}   date={props.date_gmt}/>
                    <Hero acf={props.acf.hero} />
                    <Params acf={props.acf.params} />
                </main>
                : null
            }
        </>
    )
}

export default connect(Features)