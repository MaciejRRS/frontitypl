import React from 'react'
import { connect } from "frontity"
import Hero from './components/Hero'
import Steps from './components/Steps'
import Features from './components/Features'
import Callback from './components/Callback'
import System from './components/System'
import Repeater from './components/Repeater'
import Video from '../../../../common/bricks/Video'
import SEO from '../../../../common/SEO/seo'

const HowWorks = ({ state }) => {

    let props
    const data = state.source.get(state.router.link)
    if (data.isReady) {
        props = state.source[data.type][data.id]
    }

    return (
        <>
            {data.isReady
                ? <main className='body'>
                    <SEO seo={props.acf.seo}   date={props.date_gmt}/>
                    <Hero acf={props.acf.hero} anchor={props.acf.navigation.repeater} />
                    <Video acf={props.acf.hero} />
                    <Steps acf={props.acf.steps} />
                    <Features acf={props.acf.features} />
                    <Callback acf={props.acf.callback} />
                    <System acf={props.acf.system} />
                    <Repeater acf={props.acf.Repeater} />
                </main>
                : null
            }
        </>
    )
}

export default connect(HowWorks)