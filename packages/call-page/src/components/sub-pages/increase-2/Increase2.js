import React from 'react'
import { connect } from "frontity"
import TrustedBy from '../../../../common/bricks/TrustedBy'
import Clients from '../../../../common/bricks/Clients'
import Generation from '../../../../common/bricks/Generation'
import Features from '../../../../common/bricks/Features'
import Greater from '../../../../common/bricks/Greater'
import Moreandbetter from '../../../../common/bricks/Moreandbetter'
import Develope from '../../../../common/bricks/Develope'
import Dominating from '../../../../common/bricks/Dominating'
import Questions from '../../../../common/bricks/Questions'
import SEO from '../../../../common/SEO/seo'

const Increase2 = ({ state }) => {

    let props
    const data = state.source.get(state.router.link)
    if (data.isReady) {
        props = state.source[data.type][data.id]
    }

    return (
        <>
            {data.isReady
                ? <main className='body'>
                    <SEO seo={props.acf.seo}   date={props.date_gmt}/>
                    <TrustedBy acf={props.acf.hero} />
                    <Greater acf={props.acf.greater} />
                    <Clients acf={props.acf.clients} />
                    <Generation acf={props.acf.generation} />
                    <Moreandbetter acf={props.acf.more_and_better} />
                    <Dominating acf={props.acf.dominating_channel} />
                    <Features acf={props.acf.features} />
                    <Develope marginDesktop='' marginTablet='' marginPhone='' marginBottomDesktop='' marginBottomTablet='120px' marginBottomPhone='80px' acf={props.acf.develope} />
                    <Questions acf={props.acf.questions} />
                </main>
                : null
            }
        </>
    )
}

export default connect(Increase2)