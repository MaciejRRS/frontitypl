import React from 'react'
import { connect } from "frontity"
import Hero from './components/Hero'
import Content from './components/Content'
import SEO from '../../../../common/SEO/seo'


const Industry = ({ state }) => {

    const pages = state.source.get(state.router.link)

    let props
    const data = state.source.get('/branze-page/')

    if (data.isReady) {
        props = state.source[data.type][data.id]
    }

    return (
        <>
            {data.isReady
                ? <main className='body'>
                    <SEO seo={props.acf.seo}  date={props.date_gmt} />
                    <Hero acf={props.acf.hero} />
                    <Content acf={props.acf.content} />
                </main>
                : null
            }
        </>
    )
}

export default connect(Industry)