import React from 'react'
import NavLink from "@frontity/components/link"
import { styled } from "frontity"

const Flex = styled.div`
    padding-top: ${props => props.marginDesktop ? props.marginDesktop : 'clamp(180px, 15vw, 240px)'};
    @media(max-width: 1198px){
        padding-top: ${props => props.marginTablet ? props.marginTablet : '120px'};
    }
    @media(max-width: 764px){
        padding-top: ${props => props.marginPhone ? props.marginPhone : '60px'};
    }
    display: flex;
    justify-content: space-between;
    align-items: center;
    grid-column-gap: 170px;
    flex-direction: row-reverse;

    @media(max-width: 1500px){
        grid-column-gap: 60px;
    }

    @media(max-width: 1240px){
        flex-direction: column;
    }

    @media(max-width: 1240px){
        flex-direction: column;
    }
`

const Container = styled.div`
    max-width: 1360px;
    margin: 0 auto;
    width: calc(100% - 120px);
    @media(max-width: 564px){
        width: calc(100% - 30px);
    }

    div:nth-child(odd){
        flex-direction: row;

        @media(max-width: 1240px){
            flex-direction: column;
        }
    }
`

const TextPart = styled.div`
    max-width: 665px;
    width: 100%;
`

const Title = styled.h2`
    font-size: 28px;
    line-height: 40px;
    font-weight: bold;

    @media(max-width: 1240px){
        padding-top: 40px;
    }

    @media(max-width: 764px){
        font-size: 22px;
        line-height: 28px;
    }

    @media(max-width: 476px){
        font-size: 16px;
        line-height: 20px;
        padding-top: 20px;
    }
`

const Text = styled.p`
    font-size: 16px;
    line-height: 24px;
    padding: 25px 0 45px;
    @media(max-width: 764px){
        font-size: 14px;
        line-height: 20px;
    }
    @media(max-width: 476px){
        padding-top: 10px;
    }
`

const InnerLink = styled(NavLink)`
    color: #000000;
    font-size: 18px;
    font-weight: bold;
    padding: 20px 35px;
    background-color: #FFFFFF;
    border-radius: 6px;
    border: 2px solid #377DFF;
    border-radius: 3px;
    box-shadow: 0 3px 6px 0 #D7E5FF;
    transition: all.2s linear;

    &:hover {
        border-color: #377DFF;
        background-color: #377DFF;
        color: #FFFFFF;
    }


    @media(max-width: 764px){
        font-size: 16px;
        line-height: 20px;
    }
`

const OuterLink = styled.a`
    color: #000000;
    font-size: 18px;
    font-weight: bold;
    padding: 20px 35px;
    background-color: #FFFFFF;
    border: 2px solid #377DFF;
    border-radius: 6px;
    box-shadow: 0 3px 6px 0 #D7E5FF;
    transition: all.2s linear;

    &:hover {
        border-color: #377DFF;
        background-color: #377DFF;
        color: #FFFFFF;
    }


    @media(max-width: 764px){
        font-size: 16px;
        padding: 10px 0;
        margin: 0 auto;
        width: 248px;
        display: block;
        text-align: center;
    }
`

const ImgWrapper = styled.div`
    width: 665px;
    img{
        width: 100%;
    }

    @media(max-width: 764px){
        width: 100%;
    }
`

const Content = (props) => {
    return (
        <article>
            <Container>
                {
                    props.acf.repeater.map((el, index) =>
                        <Flex key={index} id={el.id}>
                            <ImgWrapper>
                                <img alt={el.img_alt} src={el.img} />
                            </ImgWrapper>
                            <TextPart>
                                <Title>{el.title}</Title>
                                <Text>{el.text}</Text>
                                {el.button_type
                                    ? <InnerLink link={el.button_link}>{el.button_text}</InnerLink>
                                    : <OuterLink rel="noreferrer" target='_blank' href={el.button_link}>{el.button_text}</OuterLink>}
                            </TextPart>
                        </Flex>
                    )
                }
            </Container>
        </article>
    )
}

export default Content