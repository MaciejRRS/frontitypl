import React, { useState, useEffect } from 'react'
import { connect } from "frontity"
import Content from './components/Content';
import Hero from './components/Hero';
import SEO from '../../../../common/SEO/seo'

const FilterIntegrations = {
    'title': 'Integrations',
    'list': [
        {
            title: 'CMS',
            value: 'cms'
        },
        {
            title: 'Native Integrations',
            value: 'native_Integrations'
        },
        {
            title: 'Zapier Integrations',
            value: 'zapier_integrations'
        }
    ]
}

const FilterPopular = {
    'title': 'Popular For',
    'list': [
        {
            title: 'For Marketing Teams',
            value: 'marketing'
        },
        {
            title: 'For Support Teams',
            value: 'support'
        },
        {
            title: 'For Sales Teams',
            value: 'sales'
        },
        {
            title: 'All',
            value: ''
        }
    ]
}

const FilterCategories = {
    'title': 'Category',
    'list': [
        {
            title: 'CMS',
            value: 'cms'
        },
        {
            title: 'Analytics',
            value: 'analytics'
        },
        {
            title: 'Customer Support',
            value: 'customer_support'
        },
        {
            title: 'CRM',
            value: 'crm'
        },
        {
            title: 'Data & Enrichment',
            value: 'data_enrichment'
        },
        {
            title: 'Collaboration',
            value: 'collaboration'
        },
        {
            title: 'Marketing Automation',
            value: 'marketing_automation'
        }
    ]
}

const Integrations = ({ state }) => {


    const [productsPrototype, changePeoductsPrototype] = useState(null)
    const [products, productsChange] = useState(null)

    let props
    const data = state.source.get(state.router.link)
    if (data.isReady) {
        props = state.source[data.type][data.id]
    }

    useEffect(() => {
        if (data.isReady) {
            changePeoductsPrototype([...props.acf.integrations_products])
            productsChange([...props.acf.integrations_products])
        }
    }, [data.isReady])

    const [filtredBy, changeFiltredBy] = useState(false)

    const FilterByIntegrations = (value, name) => {
        productsChange(productsPrototype.filter(el => el.integrations_type.includes(value)))
        changeFiltredBy(name)
    }

    const FilterByPopular = (value, name) => {
        productsChange(productsPrototype.filter(el => el.popular_for.includes(value)))
        if (value) {
            changeFiltredBy(name)
        } else {
            changeFiltredBy(false)
        }
    }

    const FilterByCategory = (value, name) => {
        productsChange(productsPrototype.filter(el => el.category.includes(value)))
        changeFiltredBy(name)
    }

    const FilterByTitle = (value) => {
        productsChange(productsPrototype.filter(el => {
            let text = el.title.toLowerCase()
            let val = value.toLowerCase()
            return text.includes(val)
        }))
    }

    return (
        <>
            {data.isReady && products
                ? <main className='body'>
                    <SEO seo={props.acf.seo}   date={props.date_gmt}/>
                    <Hero FilterByTitle={FilterByTitle} hero={props.acf.hero} />
                    <Content
                        FilterIntegrations={FilterIntegrations}
                        FilterPopular={FilterPopular}
                        FilterCategories={FilterCategories}
                        FilterByIntegrations={FilterByIntegrations}
                        FilterByPopular={FilterByPopular}
                        FilterByCategory={FilterByCategory}
                        products={products}
                        filtredBy={filtredBy}
                    />
                </main>
                : null
            }
        </>
    )
}

export default connect(Integrations)