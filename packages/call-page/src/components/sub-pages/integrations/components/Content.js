import React, { useState } from 'react'
import { Container, Text } from '../../../../../common/styles'
import { styled } from "frontity"

const Grid = styled.div`
    position: relative;
    display: grid;
    grid-template-columns: 1fr 1fr 1fr;
    height: min-content;
    grid-column-gap: 30px;
    grid-row-gap: 30px;
    @media(max-width: 1199px){
        grid-template-columns: 1fr 1fr;
    }
    @media(max-width: 767px){
        grid-template-columns: 1fr;
    }
`

const Item = styled.div`
    box-sizing: border-box;
    padding: 15px;
    max-width: 387px;
    width: 100%;
    min-height: 280px;
    background-color: #fff;
    box-shadow: 0px 3px 10px 6px #00000016;
    cursor: pointer;
    margin: 0 auto;
    position: relative;
    height: 100%;
    transition: .2s linear;

    @media(max-width: 1198px){
        max-width: 100%;
    }

    img{
        height: 60px;
        max-width: 100%;
        width: auto
    }

    &:hover{
        transform: translate(2px, -4px);
        h3{
            color: #377DFF;
        }
    }
`

const ItemTitle = styled.h3`
    padding: 10px 0;
    color: #000;
    transition: .2s linear;
`

const ItemText = styled(Text)`
    margin-bottom: 70px;
`

const PageContent = styled.div`
    display: grid;
    grid-template-columns: 1fr 3fr;
    @media(max-width: 1198px) {
        grid-template-columns: 1fr;
    }
`

const Img = styled.img`
    height: 30px;
    margin-left: 15px;
`

const AddInform = styled.div`
    display: flex;
    align-items: center;
    padding: 30px 0;
    position: absolute;
    bottom: 0;
    width: 100%;
    font-weight: bold;

    p{
        color: #000;
    }

    img{
        height: 30px;
    }
`

const FilterTitle = styled.h3`
    padding-bottom: 15px;
    color: #377DFF;
    font-size: 22px;
    font-weight: bold;
    background-color: transparent;
    border: transparent;
    @media(max-width: 1198px) {
        padding: 0;
        color: #000000;
        font-size: 18px;
        text-transform: uppercase;
        ::after{
            content: '>';
            font-size: 18px;
            position: absolute;
            transform: rotate(90deg);
            transform-origin: center;
            margin-left: 15px;
        }
    }
`

const ListItem = styled.li`
    padding-top: 10px;
    @media(max-width: 767px){
        padding: 10px;
    }
`

const FilterBox = styled.div`
    padding-bottom: 60px;
    @media(max-width: 1198px) {
        padding-bottom: 0;
    }
`

const Button = styled.button`
    border: none;
    background-color: transparent;
    cursor: pointer;
`

const ContainerFilter = styled.div`
    @media(max-width: 1198px) {
        display: flex;
        justify-content: space-evenly;
        text-align: center;
        position: sticky;
        top: 100px;;
        width: calc(100% + 120px);
        transform: translateX(-60px);
        background: #fff;
        box-shadow: 0 3px 6px #00000016;
        z-index: 1;
        overflow: hidden;
        transition: .3s linear;
        margin-bottom: 90px;
        padding: 10px 0;
    }
    @media(max-width: 767px) {
        padding: 0;
        justify-content: flex-start;
        flex-direction: column;
        box-shadow: unset;
        top: 100px;;
        height: ${props => props.isOpened ? "auto" : "0px"};
    }
    @media(max-width: 564px){
        width: calc(100% + 30px);
        transform: translateX(-15px);

    }
`

const List = styled.ul`
    @media(max-width: 1198px) {
       height: ${props => props.isOpened ? "auto" : "0"};
       opacity: ${props => props.isOpened ? "1" : "0"};
       overflow: hidden;
       transition: opacity .4s linear;
       z-index: 2;
       background: #FFFFFF;
    }
`
const MobileMenu = styled.button`
    display: none;
    @media(max-width: 767px) {
        display: block;
        position: sticky;
        width: calc(100% + 120px);
        transform: translateX(-60px);
        top: 100px;;
        font-size: 18px;
        font-weight: bold;
        text-align: center;
        text-transform: uppercase;
        margin-bottom: 30px;
        line-height: 28px;
        background: transparent;
        border: 0;
        box-shadow: 0 3px 6px #00000016;
        padding: 8px 0;
        background-color: #fff;
        z-index: 100;
    }

    @media(max-width:574px) {
        width: calc(100% + 30px);
        transform: translateX(-15px);
    }
`

const FiltredBy = styled.div`
    position: absolute;
    left: 0;
    top: -50px;
    font-size: 18px;
`



const Content = (props) => {
    const [isMobileIntegrationOpened, isMobileIntegrationOpenedChange] = useState(false)
    const [isMobilePopularOpened, isMobilePopularOpenedChange] = useState(false)
    const [isMobileCategoriesOpened, isMobileCategoriesOpenedChange] = useState(false)
    const [isMobileMenuOpened, isMobileMenuOpenedChange] = useState(false)

    const OpenMobileMenu = (value, func) => {
        func(!value)
    }

    return (
        <article>
            <Container>
                <MobileMenu onClick={() => { OpenMobileMenu(isMobileMenuOpened, isMobileMenuOpenedChange) }}>
                    Filter
                </MobileMenu>
                <PageContent>
                    <ContainerFilter isOpened={isMobileMenuOpened}>
                        <FilterBox>
                            <FilterTitle onClick={() => { OpenMobileMenu(isMobileIntegrationOpened, isMobileIntegrationOpenedChange) }}>{props.FilterIntegrations.title}</FilterTitle>
                            <List isOpened={isMobileIntegrationOpened}>
                                {
                                    props.FilterIntegrations.list.map((el, index) =>
                                        <ListItem key={index}>
                                            <Button onClick={() => { props.FilterByIntegrations(el.value, el.title) }}>{el.title}</Button>
                                        </ListItem>
                                    )
                                }
                            </List>
                        </FilterBox>
                        <FilterBox>
                            <FilterTitle onClick={() => { OpenMobileMenu(isMobilePopularOpened, isMobilePopularOpenedChange) }}>{props.FilterPopular.title}</FilterTitle>
                            <List isOpened={isMobilePopularOpened}>
                                {
                                    props.FilterPopular.list.map((el, index) =>
                                        <ListItem key={index}>
                                            <Button onClick={() => { props.FilterByPopular(el.value, el.title) }}>{el.title}</Button>
                                        </ListItem>
                                    )
                                }
                            </List>
                        </FilterBox>
                        <FilterBox>
                            <FilterTitle  onClick={() => { OpenMobileMenu(isMobileCategoriesOpened, isMobileCategoriesOpenedChange) }}>{props.FilterCategories.title}</FilterTitle>
                            <List isOpened={isMobileCategoriesOpened}>
                                {
                                    props.FilterCategories.list.map((el, index) =>
                                        <ListItem key={index}>
                                            <Button onClick={() => { props.FilterByCategory(el.value, el.title) }}>{el.title}</Button>
                                        </ListItem>
                                    )
                                }
                            </List>
                        </FilterBox>
                    </ContainerFilter>
                    <Grid>
                        <FiltredBy>{props.filtredBy}</FiltredBy>
                        {
                            props.products.map((el, index) =>
                                <a key={index} rel="noreferrer" target='_blank' href={el.link_url}>
                                    <Item>
                                        <img alt={el.icon_alt} src={el.icon} />
                                        <ItemTitle>{el.title}</ItemTitle>
                                        <ItemText>{el.text}</ItemText>
                                        <AddInform>
                                            <p>{el.powered_by}</p>
                                            <Img alt={el.powered_by_img_alt} src={el.powered_by_img} />
                                        </AddInform>
                                    </Item>
                                </a>
                            )
                        }
                    </Grid>
                </PageContent>
            </Container>
        </article>
    )
}

export default Content