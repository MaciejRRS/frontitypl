import React from 'react'
import { Container, Flex, Input } from '../../../../../common/styles'
import { styled } from "frontity"
import SearchIcon from '../../../../../common/sprites/lupa.svg'


const Article = styled.article`
    padding: 90px 0 120px;

    @media(max-width: 1198px) {
        padding-bottom: 60px;
    }

    @media(max-width: 764px) {
        padding-bottom: 60px;
    }
`

const LocFlex = styled(Flex)`
    @media(max-width: 992px) {
        flex-direction: column-reverse;
        align-items:flex-start;
        img {
            display: none!important;
        }
    }
`

const Title = styled.h1`
    font-size: clamp(45px, 3.5vw, 60px);
    line-height: clamp(60px, 3.5vw, 80px);
    font-weight: bold;
    padding-bottom: 45px;

    @media(max-width: 1199px) {
        font-size: 45px;
        line-height: 55px;
        padding-bottom: 20px;
        max-width: 500px;
    }
    @media(max-width: 767px) {
        font-size: 28px;
        line-height: 40px;
        max-width: 100%;
        padding-bottom: 25px;
    }
`

const Text = styled.p`
    font-weight: normal;
    color: #6E7276;
    font-size: 28px;
    line-height: 40px;
    margin-bottom: 60px;
    @media(max-width: 1199px) {
        max-width: 335px;
        font-size: 14px;
        line-height: 24px;
        margin-bottom: 30px;
    }

    @media(max-width: 767px) {
        max-width: 100%;
    }
`

const TextPart = styled.div`
    display: flex;
    flex-direction: column;
    width: 665px;
    padding-right: 30px;

    @media(max-width: 1199px) {
        width: 60%;
        padding-right: 0;
    }

    @media(max-width: 1199px) {
        width: 555px;
    }
    @media(max-width: 1199px) {
        width: 100%;
    }
`

const ImgPart = styled.div`
    img {
        max-width: 100%;
    }
    @media(max-width: 1199px) {
        width: 100%;

        img {
        display: block;
        margin: 0 auto 60px;
        }
    }
    `

const InputLock = styled(Input)`
        box-sizing: border-box;
        border: 2px solid #C2C9D1;
        box-shadow: none;
        padding-left: 50px;
    
    @media(max-width: 1199px) {
        max-width: 335px;
    }

    @media(max-width: 767px) {
        max-width: 280px;
    }
`

const Lupe = styled.span`
background-image: url(${SearchIcon});
width: 30px;
    height: 30px;
    background-repeat: no-repeat;
    background-size: contain;
    margin-bottom: -47px;
    position: relative;
    margin-left: 15px;
`



const Hero = (props) => {
    return (
        <Article>
            <Container>
                <LocFlex>
                    <TextPart>
                        <Title>{props.hero.title}</Title>
                        <Text>{props.hero.text}</Text>

                        <Lupe></Lupe>
                        <InputLock onChange={event => props.FilterByTitle(event.target.value)} placeholder={props.hero.input_placeholder} />

                    </TextPart>
                    <ImgPart>
                        <img alt={props.hero.img_alt} src={props.hero.img} />
                    </ImgPart>
                </LocFlex>
            </Container>
        </Article>
    )
}

export default Hero