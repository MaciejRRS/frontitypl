import React from 'react'
import { connect } from "frontity"
import Hero from './components/Hero'
import CreatedFor from './components/CreatedFor'
import Using from './components/Using'
import Steps from './components/Steps'
import Form from './components/Form'
import Numbers from '../../../../common/bricks/Numbers'
import Awards from '../../../../common/bricks/Awards'
import PartnersType from '../../../../common/bricks/PartnersType'
import SEO from '../../../../common/SEO/seo'

const Partner = ({ state }) => {

    let props
    const data = state.source.get(state.router.link)
    if (data.isReady) {
        props = state.source[data.type][data.id]
    }

    return (
        <>
            {data.isReady
                ? <main>
                    <SEO seo={props.acf.seo}  date={props.date_gmt} />
                    <div className='body'>
                        <Hero acf={props.acf.hero} />
                        <CreatedFor acf={props.acf.created_for} />
                        <Using acf={props.acf.using} />
                        <Numbers acf={props.acf.numbers} />
                        <Awards acf={props.acf.awards} />
                        <PartnersType acf={props.acf.partners_type} />
                        <Steps acf={props.acf.steps} />
                    </div>
                    <Form acf={props.acf.form} />
                </main>
                : null
            }
        </>
    )
}

export default connect(Partner)