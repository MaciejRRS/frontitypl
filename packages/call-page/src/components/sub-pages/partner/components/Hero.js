import React from 'react'
import { styled } from "frontity"
import { Container, Link } from '../../../../../common/styles'
import ReactHtmlParser from 'react-html-parser';

const Article = styled.article`
    position: relative;
    padding-top: ${props => props.marginDesktop ? props.marginDesktop : '120px'};
    @media(max-width: 1198px){
        padding-top: ${props => props.marginTablet ? props.marginTablet : '60px'};
    }
`

const Title = styled.h1`
    font-size: clamp(45px, 3.5vw, 60px);
    line-height: clamp(60px, 3.5vw, 80px);
    font-weight: bold;
    @media(max-width: 1198px){
        font-size: 45px;
        line-height: 60px;
        text-align: center;
    }  
    @media(max-width: 764px){
        font-size: 28px;
        line-height: 40px;
    }
`

const SubTitle = styled.h2`
 font-size: 28px;
 color: #377DFF;
    font-weight: bold;
    line-height: 40px;
    max-width: 665px;
    margin: 50px 0 60px;
    @media(max-width: 1198px){
        font-size: 22px;
        line-height: 30px;
        margin: 30px auto 40px;
        text-align: center;
    }  
    @media(max-width: 764px){
        font-size: 18px;
        line-height: 28px;
        margin: 20px auto 30px;
    }
`

const Text = styled.p`
    font-size: 16px;
    font-weight: normal;
    line-height: 28px;
    color: #6E7276;
    max-width: 665px;
    margin: 0 0 60px;
    @media(max-width: 1198px) {
    text-align: center;
    margin: 0 auto 60px;
    display: block;
    }

    @media(max-width: 764px) {
    margin: 0 auto 30px;
    }
`

const LocLink = styled(Link)`
    display: block;
    width: 250px;
    text-align: center;
    transition: .2s linear;
    @media(max-width: 1198px){
        padding: 10px 0;
        margin: 0 auto;
    }

    &:hover{
        color: #377DFF;
        background-color: #FFFFFF;
        border-color: #377DFF;
    }
`

const UnderText = styled.div`
 p {
    color: #6E7276;
    padding-top: 30px;
    font-size: 16px;
    @media(max-width: 1198px){
        font-size: 14px;
        text-align: center;
    }
    @media(max-width: 764px){
        padding-top: 15px;
    }
 }
 a {
     color: #377DFF;
     transition: all.2s linear;
     &:hover {
         color: #ff4800;
     }
 }
`


const Flex = styled.div`
display: flex;
justify-content: space-between;

@media(max-width:1198px) {
    flex-direction: column;
}
`

const LeftColumn = styled.div`
flex-basis: 50%;
`

const RightColumn = styled.div`
flex-basis: 50%;
img {
    max-height: 600px;
    width: auto;
    max-width: 100%;
}
`



const Hero = (props) => {
    return (
        <Article>
            <Container>
                <Flex>
                    <LeftColumn>
                        <Title>{props.acf.title}</Title>
                        <SubTitle>{props.acf.subtitle}</SubTitle>
                        <Text>{props.acf.text}</Text>

                        {props.acf.button_type
                            ? <LocLink to={props.acf.button_link}>{props.acf.button_text}</LocLink>
                            : <LocLink as='a' rel="noreferrer" target='_blank' href={props.acf.button_link}>{props.acf.button_text}</LocLink>
                        }


                        <UnderText>{ReactHtmlParser(props.acf.under_text)}</UnderText>
                    </LeftColumn>

                    <RightColumn>
                        <img alt={props.acf.img_alt} src={props.acf.img} />
                    </RightColumn>
                </Flex>
            </Container>
        </Article>
    )
}

export default Hero