import React from 'react'
import { Container, Link, Text } from '../../../../../common/styles'
import { styled } from "frontity"

const LocContainer = styled(Container)`
    max-width: 1082px;
`

const Article = styled.article`
    position: relative;
    padding-top: ${props => props.marginDesktop ? props.marginDesktop : 'clamp(140px, 12vw, 240px);'};
    @media(max-width: 1198px){
        padding-top: ${props => props.marginTablet ? props.marginTablet : 'clamp(90px, 12vw, 140px);'};
    }
    @media(max-width: 764px){
        padding-top: ${props => props.marginPhone ? props.marginPhone : '90px'};
    }
`

const Title = styled.h2`
    text-align: center;
    font-size: clamp(45px, 3.5vw, 60px);
    line-height: clamp(60px, 3.5vw, 80px);
    font-weight: bold;
    padding-bottom: 100px;
    @media(max-width: 1198px){
        font-size: 45px;
        line-height: 60px;
        padding-bottom: 60px;
    }
    @media(max-width: 764px){
        font-size: 28px;
        line-height: 40px;
        padding-bottom: 30px;
    }
`

const Grid = styled.div`
    position: relative;
    div:nth-child(even){
        div{
            margin: 0 0 0 auto;
        }
    }

    @media(max-width: 764px){
        div:nth-child(even){
            div{
            margin: 0 auto;
            }
        }
    }
`

const Item = styled.div`
    width: 100%;
    height: 240px;
    div{
        margin: 0 auto 0 0;
        width: 387px;
    } 
    @media(max-width: 1198px){
        div{
            max-width: 387px;
            width: 40%;
        }
    }
    @media(max-width: 764px){
        display: flex;
        flex-direction: column;
        height: auto;
        div{
            margin: 0 auto;
            text-align: center;
            width: 80%;
        }
    }
`


const Line = styled.div`
    height: 100%;
    width: 2px;
    background-color: #FFB6C0;
    position: absolute;
    left: 50%;
    top: 0;
    z-index: 10;
    @media(max-width: 764px){
        display: none;   
    }
`

const Dot = styled.span`
    position: absolute;
    width: 30px;
    height: 30px;
    background-color: #FFB6C0;
    border-radius: 50%;
    left: 50%;
    transform: translateX(-50%);
    z-index: 1;
    @media(max-width: 764px){
        position: unset;
        margin: 30px auto 30px;
        transform: unset;
        width: 15px;
        height: 15px;
    }
`

const ItemTitle = styled.h3`
    font-weight: bold;
    font-size: 45px;
    line-height: 60px;
    padding-bottom: 30px;
    @media(max-width: 1198px){
        font-size: 28px;
        line-height: 40px;
        padding-bottom: 20px;
    }
    @media(max-width: 764px){
        font-size: 22px;
        line-height: 30px;
    }
`

const LocText = styled(Text)`
    font-size: 16px;
    line-height: 24px;
    @media(max-width: 1198px){
        font-size: 14px;
        line-height: 20px;
    }
`


const LocLink = styled(Link)`
    margin: 150px auto 0;
    display: block;
    max-width: 500px;
    width: 100%;
    box-sizing: border-box;
    text-align: center;
`

const Steps = (props) => {
    return (
        <Article>
            <LocContainer>
                <Title>{props.acf.title}</Title>
                <Grid>
                    {
                        props.acf.repeater.map((el, i) =>
                            <Item key={i}>
                                <Dot />
                                <div>
                                    <ItemTitle>{el.title}</ItemTitle>
                                    <LocText>{el.text}</LocText>
                                </div>
                            </Item>
                        )
                    }
                    <Line />
                </Grid>
                <LocLink to="/">Become our Referral Partner</LocLink>
            </LocContainer>
        </Article>
    )
}

export default Steps