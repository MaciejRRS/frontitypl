import React, { useState } from 'react'
import { Container } from '../../../../../common/styles'
import { styled } from "frontity"
import Youtube from 'react-lazyload-youtube'

const Article = styled.article`
    position: relative;
    padding-top: ${props => props.marginDesktop ? props.marginDesktop : 'clamp(140px, 12vw, 240px);'};
    @media(max-width: 1198px){
        padding-top: ${props => props.marginTablet ? props.marginTablet : 'clamp(90px, 12vw, 140px);'};
    }
    @media(max-width: 764px){
        padding-top: ${props => props.marginPhone ? props.marginPhone : '90px'};
    }
`

const LocLink = styled.button`
    transition: .2s linear;
    cursor: pointer;
    padding: 15px 60px;
    font-weight: bold;
    background-color: #377DFF;
    color: #ffffff;
    border: none;
    border-radius: 6px;
    border: 2px solid #377DFF;
    box-shadow: 0px 3px 6px 0px #D7E5FF;
    &:hover{
        background-color: #fff;
        color: #377DFF;
    }
`

const Flex = styled.div`
    display: flex;
    max-width: 80%;
    justify-content: center;
    margin: 0 auto;

    @media(max-width: 1280px){
        align-items: center;
    }

    @media(max-width: 1196px){
        flex-direction: column-reverse;
    }
`

const Title = styled.h2`
    text-align: center;
    font-size: clamp(45px, 3.5vw, 60px);
    line-height: clamp(60px, 3.5vw, 80px);
    font-weight: bold;
    margin-bottom: 100px;
    @media(max-width: 1196px){
        font-size: 45px;
        line-height: 60px;
        margin-bottom: 80px;
    }
    @media(max-width: 764px){
        font-size: 28px;
        line-height: 40px;
        margin-bottom: 50px;
    }
`

const ListItem = styled.li`
    padding: 0 0 30px 45px;
    position: relative;
    font-size: 22px;
    line-height: 24px;
    @media(max-width: 1198px){
        font-size: 14px;
        line-height: 20px;
        padding: 0 0 30px 30px;
    }

    &::before{
        content: '•';
        color: #FFE04F;
        position: absolute;
        left: 10px;
        
    }
`

const List = styled.ul`
    margin-bottom: 40px;
`

const LeftColumn = styled.div`
    max-width: 50%;
    padding-right: 60px;
    @media(max-width: 1196px){
        padding: 0; 
        max-width: 800px;
        width: 100%;
    }

    a {
        max-width:225px;
        text-align: center;

        @media(max-width: 1198px) {
            padding: 10px 30px;
            margin: 0 auto;
            display: block;

        }
    }
`

const RightColumn = styled.div`
    max-width: 50%;
    img{
        width: 100%;
        max-width: 650px;
        margin: 0 auto;
        display: block;
        cursor: pointer;
    }
    @media(max-width: 1196px){
        max-width: 800px;
        width: 100%;
        
        img{
            margin-bottom: 40px;
        }
    }
`

const Popup = styled.div`
    opacity: ${props => props.isPopupOpened ? '1' : '0'};
    pointer-events: ${props => props.isPopupOpened ? 'all' : 'none'};
    transition: .2s linear;
    width: 70vw;
    height: 70vh;
    position: fixed;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    background-color: #fff;
    z-index: 1000;
    border-radius: 12px;
    box-sizing: border-box;
    display: flex;
    align-items: center;
    iframe{
    }
    div{
        width: 100%;
        height: 100%;
        background-repeat: no-repeat;
        background-size: contain;
        background-position: center;
        border-radius: 12px;
    }
    @media(max-width: 998px){
        width: 85vw;
        height: 75vh;
    }
`

const PopupBackground = styled.div`
    opacity: ${props => props.isPopupOpened ? '1' : '0'};
    pointer-events: ${props => props.isPopupOpened ? 'all' : 'none'};
    transition: .2s linear;
    position: fixed;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    z-index: 999;
    background-color: #00000099;
`

const ClosePopup = styled.button`
    opacity: ${props => props.isPopupOpened ? '1' : '0'};
    pointer-events: ${props => props.isPopupOpened ? 'all' : 'none'};
    transition: .2s linear;
    position: fixed;
    right: 10vw;
    top: 10vh;
    width: 50px;
    height: 50px;
    border: none;
    background: transparent;
    z-index: 10001;
    cursor: pointer;
    &::before,
    &::after{
        width: 100%;
        height: 3px;
        content: '';
        position: absolute;
        z-index: 10002;
        left: 0;
        top: 0;
        background-color: #fff;
        transform: rotateZ(45deg);
    }

    &::after{
        transform: rotateZ(-45deg);
    }

    @media(max-width: 998px){
        right: 2vw;
        top: 6vh;
        
    }
`

const Using = (props) => {

    const [isPopupOpened, changePopupStatus] = useState(false)

    return (
        <Article>
            <PopupBackground onClick={() => { changePopupStatus(false) }} isPopupOpened={isPopupOpened} />
            <ClosePopup onClick={() => { changePopupStatus(false) }} isPopupOpened={isPopupOpened} />
            <Popup isPopupOpened={isPopupOpened}>
                <Youtube videoId={props.acf.video_id} />
            </Popup>

            <Container>
                <Title>{props.acf.title}</Title>
                <Flex>
                    <LeftColumn>
                        <List>
                            {
                                props.acf.repeater.map((el, index) =>
                                    <ListItem key={index}>{el.text}</ListItem>
                                )
                            }
                        </List>
                        <LocLink onClick={() => { changePopupStatus(true) }} >{props.acf.button_text}</LocLink>
                    </LeftColumn>
                    <RightColumn>
                        <img onClick={() => { changePopupStatus(true) }} alt={props.acf.img_alt} src={props.acf.img} />
                    </RightColumn>
                </Flex>
            </Container>
        </Article>
    )
}

export default Using