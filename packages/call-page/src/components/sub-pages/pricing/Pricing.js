import React from 'react'
import { connect } from "frontity"
import SEO from '../../../../common/SEO/seo'
import MoreEffective from '../../../../common/bricks/MoreEffective'
import Questions from '../../../../common/bricks/Questions'
import Price from './components/Price'
import MoreThen from './components/MoreThen'
import Countries from './components/Countries'
import Solution from './components/Solution'
import Table from './components/Table'

const Pricing = ({ state }) => {

    let props
    const data = state.source.get(state.router.link)
    if (data.isReady) {
        props = state.source[data.type][data.id]
    }
    
    return (
        <>
            {data.isReady
                ? <main className='body'>
                    <SEO seo={props.acf.seo}  date={props.date_gmt} />
                    <MoreEffective marginDesktop='60px' marginTablet='60px' marginPhone='60px' acf={props.acf.hero} />
                    <Price acf={props.acf.price} />
                    <MoreThen acf={props.acf.more_then} />
                    <Solution acf={props.acf.solution} />
                    <Table acf={props.acf.table} />
                    <Questions acf={props.acf.questions} />
                </main>
                : null
            }
        </>
    )
}

export default connect(Pricing)