import React from 'react'
import { styled } from "frontity"
import { Container, Flex } from '../../../../../common/styles'
import NavLink from "@frontity/components/link"
import ReactHtmlParser from 'react-html-parser'


const Article = styled.article`
    padding-top: ${props => props.marginDesktop ? props.marginDesktop : '120px'};
    @media(max-width: 1198px){
        padding-top: ${props => props.marginTablet ? props.marginTablet : '90px'};
    }
    @media(max-width: 764px){
        padding-top: ${props => props.marginPhone ? props.marginPhone : '60px'};
    }
`

const Title = styled.div`
    h2{
        padding-bottom: 40px;
        font-size: 28px;
        line-height: 36px;
        font-weight: 700;
        @media(max-width: 1198px){
            padding-bottom: 30px;
        }
        @media(max-width: 764px){
            font-size: 22px;
            line-height: 30px;
            padding-bottom: 20px;
        }
    }
    strong{
        font-size: 28px;
        line-height: 36px;
        font-weight: 700;
        border-bottom: 2px solid #000;
    }
`

const LocFlex = styled(Flex)`
    justify-content: space-evenly;
    @media(max-width: 1198px){
        flex-direction: column;
    }
`

const TextPart = styled.div`
    max-width: 526px;
    margin-left: 30px;
    @media(max-width: 1198px){
        margin-left: 0;
        margin-top: 60px;
    }
    @media(max-width: 764px){
        margin-top: 30px;
    }
`

const ImgPart = styled.div`
    max-width: 526px;
    img{
        width: 100%;
    }

`

const ListTitle = styled.h3`
    padding-bottom: 40px;
    @media(max-width: 1198px){
        padding-bottom: 30px;
    }
    @media(max-width: 764px){
        padding-bottom: 20px;
    }
`

const List = styled.ul`
    display: grid;
    grid-row-gap: 20px;
    @media(max-width: 1198px){
        grid-row-gap: 15px;
    }
    li{
        padding-left: 30px;
        font-size: 14px;
        line-height: 22px;
        position: relative;
        &::before{
            content: '✓';
            font-weight: bold;
            position: absolute;
            left: 0;
            color: #377DFF;
            font-size: 22px;
            line-height: 22px;
        }
    }
`

const Link = styled(NavLink)`
    display: block;
    margin-top: 30px;
    width: 248px;
    text-align: center;
    font-size: 14px;
    line-height: 40px;
    border-radius: 2px;
    background-color: #377DFF;
    color: #FFFFFF;
    box-shadow: 0px 3px 8px 4px #00000016;
    @media(max-width: 764px){
        margin: 30px auto 0;
    }
`

const MoreThen = (props) => {
    return (
        <Article>
            <Container>
                <LocFlex>
                    <ImgPart>
                        <img alt={props.acf.img_alt} src={props.acf.img} />
                    </ImgPart>
                    <TextPart>
                        <Title>{ReactHtmlParser(props.acf.title)}</Title>
                        <ListTitle>{props.acf.list_title}</ListTitle>
                        <List>
                            {props.acf.list_items.map((el, index) =>
                                <li key={index}>{el.text}</li>
                            )}
                        </List>
                        {props.acf.button_type
                            ? <Link link={props.acf.button_link}>{props.acf.button_text}</Link>
                            : <Link as='a' rel="noreferrer" target='_blank' href={props.acf.button_link}>{props.acf.button_text}</Link>
                        }
                    </TextPart>
                </LocFlex>
            </Container>
        </Article>
    )
}

export default MoreThen