import React, { useState } from 'react'
import { Container } from '../../../../../common/styles'
import { styled } from "frontity"
import NavLink from "@frontity/components/link"
import ReactHtmlParser from 'react-html-parser'

const Article = styled.article`
    padding-top: ${props => props.marginDesktop ? props.marginDesktop : '0'};
    @media(max-width: 1198px){
        padding-top: ${props => props.marginTablet ? props.marginTablet : '0'};
    }
    @media(max-width: 764px){
        padding-top: ${props => props.marginPhone ? props.marginPhone : '0'};
    }
`

const LocContainer = styled(Container)`
    @media(max-width: 1198px){
        width: calc(100% - 90px);
    }
    @media(max-width: 564px){
        width: calc(100% - 30px);
    }
`

const ButtonsBox = styled.div`
    display: flex;
    justify-content: space-evenly;
    border-radius: 50px;
    background-color: #F9F9FA;
    width: 504px;
    height: 80px;
    margin: 0 auto 120px;
    position: relative;

    &::before{
        content: '';
        width: calc(50% - 20px);
        height: calc(100% - 20px);
        transform: translate(10px, 10px);
        position: absolute;
        z-index:1;
        background-color: #377DFF;
        border-radius: 50px;
        transition: .3s linear;
        left: ${props => props.first ? '0' : '50%'};
    }

    button{
        width: 50%;
        border-radius: 40px;
        border: none;
        font-weight: bold;
        font-size: 20px;
        line-height: 28px;
        background-color: transparent;
        transition: .2s linear;
        position: relative;
        z-index: 2;

        &.first{
            color: ${props => props.first ? '#FFFFFF' : '#000000'};
            cursor: ${props => props.first ? 'unset' : 'pointer'};
        }

        &.second{
            color: ${props => props.first ? '#000000' : '#FFFFFF'};
            cursor: ${props => props.first ? 'pointer' : 'unset'};
        }

        &:focus{
            outline: none;
        }
    }
    @media(max-width: 1198){
        margin: 0 auto 90px;
    }

    @media(max-width: 764px){
        
        margin: 0 auto 60px;
        button{
            font-size: 16px;
        }
    }

    @media(max-width: 604px){
        width: 100%;
    }
`

const MiniButtonBox = styled(ButtonsBox)`
    width: 100%;
    margin: 30px auto 0;
    position: relative;
    z-index: 2;
    border-radius: 2px;
    &::before{
        border-radius: 2px;
        left: unset;
        right: ${props => props.first ? '0' : '50%'};
        transform: translate(-10px, 10px);
    }
`

const TabContent = styled.div`
    display: grid;
    grid-template-columns: 1fr 1fr 1fr 1fr;
    grid-column-gap: 30px;
    box-sizing: border-box;
    position: relative;
    @media(max-width: 1760px){
        zoom: .8;
    }
    @media(max-width: 1440px){
        zoom: 1;
    }
    @media(max-width: 1198px){
        grid-template-columns: 1fr 1fr;
        grid-column-gap: 30px;
    }
    @media(max-width: 796px){
        grid-template-columns: 1fr;
    }
`

const Item = styled.div`
    padding: 0 30px;
    border-radius: 4px;
    border: 2px solid #F2F2F2;  
    position: relative;
    width: 387px;
    max-width: 100%;
    box-sizing: border-box;
    @media(max-width: 1760px){
        width: 100%;
    }
    @media(max-width: 796px){
        max-width: 504px;
        margin: 0 auto;
        min-width: unset;
        padding: 0 15px;
    }
`

const ItemLine = styled.div`
    position: absolute;
    background-color: ${props => props.color};
    border-radius: 4px;
    border-bottom-left-radius: unset;
    border-bottom-right-radius: unset;
    width: calc(100% + 4px);
    min-height: 4px;
    line-height: 60px;
    text-align: center;
    color: #fff;
    font-weight: bold;
    top: ${props => props.text ? '-56px' : '-2px'};
    left: -2px;
`

const ItemTitle = styled.h2`
    padding-top: 55px;
    padding-bottom: 15px;
    color: #377DFF;
    font-size: 28px;
    line-height: 40px;
    font-weight: bold;
`

const Country = styled.h4`
    font-size: 12px;
    line-height: 20px;
    color: #377DFF;
    padding: 15px 0 5px;
`

const CountryItem = styled.div`
    p{
        font-size: 12px;
        line-height: 20px;
        padding: 5px 0;
        color: #737373;
        strong{
            color: #000;
            font-size: 12px;
            line-height: 20px;
        }
    }
`

const Flex = styled.div`
    display: flex;
    align-items: center;
`

const Cost = styled.h3`
    padding-right: 15px;
    font-size: ${props => props.individual ? "50px" : "64px"};
    line-height: 104px;
    font-weight: 700;
    font-family: 'Raleway', sans-serif;
    font-variant-numeric: lining-nums; 
    @media(max-width: 1760px){
        font-size: 64px;
    }
    @media(max-width: 1440px){
        font-size: ${props => props.individual ? "42px" : "48px"};
    }
    @media(max-width: 1198px){
        font-size: ${props => props.individual ? "50px" : "64px"};
    }
    @media(max-width: 876px){
        font-size: ${props => props.individual ? "36px" : "48px"};
        line-height: 74px;
    }
`

const PriceText = styled.p`
    color: #6E7276;
    font-size: 18px;
    line-height: 28px;
    @media(max-width: 1198px){
    }
`

const TextFirst = styled.p`
    font-size: 16px;
    line-height: 24px;
    @media(max-width: 1198px){
    }
`

const TextSecond = styled.p`
    color: #6E7276;
    font-size: 12px;
    line-height: 20px;
    @media(max-width: 1198px){
    }
`

const TextThird = styled.p`
    font-size: 16px;
    line-height: 24px;
    padding-top: 3px;
    @media(max-width: 1198px){
    }
`

const ListTitle = styled.h4`
    margin-top: 25px;   
    padding-top: 20px;
    padding-bottom: 10px;
    position: relative;
    font-size: 16px;
    line-height: 26px;
    @media(max-width: 1198px){
    }

    &::before{
        content: '';
        width: 100%;
        border: 1px solid #F0F0F0;  
        position: absolute;
        left: 0;
        right: 0;
        top: 0;
    }
`

const List = styled.ul`
    padding: 0 0 170px;
    display: grid;
    grid-row-gap: 5px;

    li{
        position: relative;
        padding-left: 20px;
        font-size: 14px;
        line-height: 25px;
        color: #3F3F3F;

        &::before {
            content: '✓';
            color: #000;
            font-weight: bold;
            position: absolute;
            left: 0;
        }

        @media(max-width: 1198px){     
            font-size: 14px;
            line-height: 20px;
        }
    }
`

const MiniLink = styled.a`
    color: #377DFF;
    display: block;
    padding-bottom: 25px;
    font-size: 18px;
    line-height: 24px;
    @media(max-width: 1198px){
        font-size: 16px;
        line-height: 24px;
    }
`

const Link = styled(NavLink)`
    display: block;
    width: 100%;
    margin: 0 auto;
    text-align: center;
    line-height: 60px;
    border-radius: 6px;
    background-color: #377DFF;
    color: #fff;
    font-weight: bold;
    margin-bottom: 30px;
    font-size: 14px;
    @media(max-width: 1198px){
    }
`

const LinksBox = styled.div`
    position: absolute;
    bottom: 0;
    width: calc(100% - 60px);
    @media(max-width: 796px){
        width: calc(100% - 30px);
    }
`

const Price = (props) => {

    const [isFirstOpened, changeOpened] = useState(true)

    const [isBasic, changeBasic] = useState(true)

    return (
        <Article>
            <LocContainer>
                <ButtonsBox first={isFirstOpened}>
                    <button onClick={() => { changeOpened(true) }} className="first">
                        {props.acf.tab_names.first_tab_name}
                    </button>
                    <button onClick={() => { changeOpened(false) }} className="second">
                        {props.acf.tab_names.second_tab_name}
                    </button>
                </ButtonsBox>
                <div>
                    <TabContent>
                        {isBasic
                            ? <>
                                {props.acf.first_item_alt.map((el, index) =>
                                    <Item key={index}>
                                        {el.line_text
                                            ? <ItemLine color={el.line_color} text={true}>{el.line_text}</ItemLine>
                                            : <ItemLine color={el.line_color}></ItemLine>
                                        }
                                        <MiniButtonBox first={isBasic}>
                                            <button onClick={()=> { changeBasic(false) }} className="second">
                                                {el.radio_left_text}
                                            </button>
                                            <button onClick={()=> { changeBasic(true) }} className="first">
                                                {el.radio_right_text}
                                            </button>
                                        </MiniButtonBox>
                                        <Flex>
                                            {isFirstOpened
                                                ? <><Cost>{el.price_basic.pln}</Cost>
                                                    <PriceText>{el.price_text}</PriceText>
                                                </>
                                                : <>
                                                    <Cost>{el.price_basic_alt.pln}</Cost>
                                                    <PriceText>{el.price_text_alt}</PriceText>
                                                </>
                                            }
                                        </Flex>
                                        <TextFirst>{el.text_1}</TextFirst>
                                        <TextSecond>{el.text_2}</TextSecond>
                                        {el.text_3.map((el, index) => 
                                            <TextThird key={index}>{el.text}</TextThird>
                                        )}
                                        <Country>{el.countries_title}</Country>
                                        <CountryItem>{ReactHtmlParser(el.countries_text)}</CountryItem>
                                        <ListTitle>{el.list_title}</ListTitle>
                                        <List>
                                            {el.list.map((innerEl, innerIndex) =>
                                                <li key={innerIndex}>{innerEl.text}</li>
                                            )}
                                        </List>
                                        <LinksBox>
                                            <MiniLink href={el.link_url}>{el.link_text}</MiniLink>

                                            {el.button_type
                                                ? <Link link={el.button_link}>{el.button_text}</Link>
                                                : <Link as='a' rel="noreferrer" target='_blank' href={el.button_link}>{el.button_text}</Link>
                                            }
                                        </LinksBox>
                                    </Item>
                                )}
                            </>
                            : <>
                                {props.acf.first_item.map((el, index) =>
                                    <Item key={index}>
                                        {el.line_text
                                            ? <ItemLine color={el.line_color} text={true}>{el.line_text}</ItemLine>
                                            : <ItemLine color={el.line_color}></ItemLine>
                                        }
                                        <MiniButtonBox first={isBasic}>
                                            <button onClick={()=> { changeBasic(false) }} className="second">
                                                {el.radio_left_text}
                                            </button>
                                            <button onClick={()=> { changeBasic(true) }} className="first">
                                                {el.radio_right_text}
                                            </button>
                                        </MiniButtonBox>
                                        <Flex>
                                            {isFirstOpened
                                                ? <>
                                                    <Cost>{el.price_basic.pln}</Cost>
                                                    <PriceText>{el.price_text}</PriceText>
                                                </>
                                                : <>
                                                    <Cost>{el.price_basic_alt.pln}</Cost>
                                                    <PriceText>{el.price_text_alt}</PriceText>
                                                </>
                                            }
                                        </Flex>
                                        <TextFirst>{el.text_1}</TextFirst>
                                        <TextSecond>{el.text_2}</TextSecond>
                                        {el.text_3.map((el, index) => 
                                            <TextThird key={index}>{el.text}</TextThird>
                                        )}
                                        <TextThird>{el.text_4}</TextThird>
                                        <Country>{el.countries_title}</Country>
                                        <CountryItem>{ReactHtmlParser(el.countries_text)}</CountryItem>
                                        <ListTitle>{el.list_title}</ListTitle>
                                        <List>
                                            {el.list.map((innerEl, innerIndex) =>
                                                <li key={innerIndex}>{innerEl.text}</li>
                                            )}
                                        </List>
                                        <LinksBox>
                                            <MiniLink href={el.link_url}>{el.link_text}</MiniLink>

                                            {el.button_type
                                                ? <Link link={el.button_link}>{el.button_text}</Link>
                                                : <Link as='a' rel="noreferrer" target='_blank' href={el.button_link}>{el.button_text}</Link>
                                            }
                                        </LinksBox>
                                    </Item>
                                )}
                            </>
                        }
                        <>
                            {props.acf.repeater.map((el, index) =>
                                <Item key={index}>
                                    {el.line_text
                                        ? <ItemLine color={el.line_color} text={true}>{el.line_text}</ItemLine>
                                        : <ItemLine color={el.line_color}></ItemLine>
                                    }
                                    <ItemTitle>{el.title}</ItemTitle>
                                    <Flex>
                                        {isFirstOpened
                                            ? <>
                                                <Cost individual={!el.price_text}>{el.price.pln}</Cost>
                                                <PriceText>{el.price_text}</PriceText>
                                            </>
                                            : <>
                                                <Cost individual={!el.price_text}>{el.price_alt.pln}</Cost>
                                                <PriceText>{el.price_text_alt}</PriceText>
                                            </>
                                        }
                                    </Flex>
                                    <TextFirst>{el.text_1}</TextFirst>
                                    <TextSecond>{el.text_2}</TextSecond>
                                    {el.text_3.map((el, index) => 
                                        <TextThird key={index}>{el.text}</TextThird>
                                    )}
                                    <ListTitle>{el.list_title}</ListTitle>
                                    <List>
                                        {el.list.map((innerEl, innerIndex) =>
                                            <li key={innerIndex}>{innerEl.text}</li>
                                        )}
                                    </List>
                                    <LinksBox>
                                        <MiniLink href={el.link_url}>{el.link_text}</MiniLink>

                                        {el.button_type
                                            ? <Link link={el.button_link}>{el.button_text}</Link>
                                            : <Link as='a' rel="noreferrer" target='_blank' href={el.button_link}>{el.button_text}</Link>
                                        }
                                    </LinksBox>
                                </Item>
                            )}
                        </>
                    </TabContent>
                </div>
            </LocContainer>
        </Article >
    )
}

export default Price