import React from 'react'
import { styled } from "frontity"


const Article = styled.article`
    overflow: scroll;
    padding-top: ${props => props.marginDesktop ? props.marginDesktop : '180px'};
    margin-top: -60px;
    @media(max-width: 1198px){
        padding-top: ${props => props.marginTablet ? props.marginTablet : '150px'};
    }
    @media(max-width: 764px){
        padding-top: ${props => props.marginPhone ? props.marginPhone : '120px'};
    }
`

const Container = styled.div`
    max-width: 100%;
    width: calc(100% - 120px);
    min-width: 900px;
    padding: 0 60px;
    margin: 0 auto;
    @media(max-width: 564px){
        width: calc(100% - 30px);
        padding: 0 15px;
        min-width: 600px;
    }
`

const Grid = styled.pre`
    display: grid;
    grid-template-columns: 2fr 1fr 1fr 1fr 1fr;

    @media(max-width: 992px){
        grid-template-columns: 1fr 1fr 1fr 1fr;
    }
`

const NamesGrid = styled(Grid)`
    @media(max-width: 992px){
        h2{
            grid-column: 1 / -1;
        }
    }
`

const NamesTitle = styled.h2`
    font-size: 48px;
    line-height: 72px;
    @media(max-width: 992px){
        display: none;
    }
`

const NamesItem = styled.h3`
    font-size: 24px;
    line-height: 72px;
    color: #377DFF;
    @media(max-width: 992px){
        text-align: center;
    }
`

const RowGrid = styled(Grid)`
    box-sizing: border-box;
    padding: 15px;

    
    div{
        font-size: 14px;
        color: #3f3f3f;
    }
    @media(max-width: 992px){
        div{
            text-align: center;
        }
        .title{
            text-align: left;
            grid-column: 1 / -1;
            font-size: 16px;
            font-weight: bold;
            padding-bottom: 10px;
        }
    }
`

const Content = styled.div`
    border: 1px solid #efefef;

    pre:nth-child(even){
        background-color: #F9F9FA;
    }
`

const Part = styled.div`
    padding-top: 40px;

    h2{
        padding: 10px;
        font-size: 16px;
    }
`

const True = styled.p`
    color: #377DFF;
    font-weight: bold;
`

const False = styled.p`

`

const Icon_Info = styled.span`
font-size: 20px;
color: #377dff;
margin-left: 10px;
font-weight: normal;
`


const Table = (props) => {
    return (
        <Article id='table'>
            <Container>
                {props.acf.names.map((el, index) =>
                    <NamesGrid key={index}>
                        <NamesTitle>
                            {el.column_name_1}
                        </NamesTitle>
                        <NamesItem>
                            {el.column_name_2}
                        </NamesItem>
                        <NamesItem>
                            {el.column_name_3}
                        </NamesItem>
                        <NamesItem>
                            {el.column_name_4}
                        </NamesItem>
                        <NamesItem>
                            {el.column_name_5}
                        </NamesItem>
                    </NamesGrid>
                )}
                {props.acf.table_text_part.map((el, index) =>
                    <Part key={index}>
                        <h2>{el.title}</h2>
                        <Content>
                            {el.row_repeater.map((innerEl, innerIndex) =>
                                <RowGrid key={innerIndex}>
                                    <div className={'title'}>
                                        {innerEl.column_1} 
                                    </div>
                                    <div>
                                        {innerEl.column_2} 
                                    </div>
                                    <div>
                                        {innerEl.column_3} 
                                    </div>
                                    <div>
                                        {innerEl.column_4}
                                    </div>
                                    <div>
                                        {innerEl.column_5}
                                    </div>
                                </RowGrid>
                            )}
                        </Content>
                    </Part>
                )}
                {props.acf.table_boolean_part.map((el, index) =>

                    <Part key={index}>
                        {props.acf.names.map((innerEl, innerIndex) =>
                            <NamesGrid key={innerIndex}>
                                <h2>{el.title}</h2>
                                <NamesItem>
                                    {innerEl.column_name_2}
                                </NamesItem>
                                <NamesItem>
                                    {innerEl.column_name_3}
                                </NamesItem>
                                <NamesItem>
                                    {innerEl.column_name_4}
                                </NamesItem>
                                <NamesItem>
                                    {innerEl.column_name_5}
                                </NamesItem>
                            </NamesGrid>
                        )}
                        <Content>
                            {el.row_repeater.map((innerEl, innerIndex) =>
                                <RowGrid key={innerIndex}>
                                    <div className={'title'}>
                                        {innerEl.column_1}
                                    </div>
                                    <div>
                                        {innerEl.column_2 ? <True>✓</True> : <False>-</False>}
                                    </div>
                                    <div>
                                        {innerEl.column_3 ? <True>✓</True> : <False>-</False>}
                                    </div>
                                    <div>
                                        {innerEl.column_4 ? <True>✓</True> : <False>-</False>}
                                    </div>
                                    <div>
                                        {innerEl.column_5 ? <True>✓</True> : <False>-</False>}
                                    </div>
                                </RowGrid>
                            )}
                        </Content>
                    </Part>
                )}
            </Container>
        </Article>
    )
}

export default Table