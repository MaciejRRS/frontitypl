import React from 'react'
import { connect } from "frontity"
import ReactHtmlParser from 'react-html-parser'
import Content from './components/Content'
import SEO from '../../../../common/SEO/seo'

const Privacy = ({ state }) => {

    let props
    let html

    const data = state.source.get(state.router.link)
    if (data.isReady) {
        props = state.source[data.type][data.id]
        html = ReactHtmlParser(props.acf.text)
    }

    return (
        <>
            {data.isReady
                ? <main className='body'>
                    <SEO seo={props.acf.seo}  date={props.date_gmt} />
                    <Content html={html} />
                </main>
                : null
            }
        </>
    )
}

export default connect(Privacy)