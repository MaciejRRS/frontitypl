import React from 'react'
import Develope from '../../../../common/bricks/Develope'
import Clients from '../../../../common/bricks/Clients'
import Features from '../../../../common/bricks/Features'
import Awards from '../../../../common/bricks/Awards'
import Generation from '../../../../common/bricks/Generation'
import TrustedBy from '../../../../common/bricks/TrustedBy'
import ProblemsResolving from '../../../../common/bricks/ProblemsResolving'
import Questions from '../../../../common/bricks/Questions'
import PartnersType from '../../../../common/bricks/PartnersType'
import SEO from '../../../../common/SEO/seo'
import { connect } from "frontity"

const CallCenter = ({ state }) => {

    let props
    const data = state.source.get(state.router.link)
    if (data.isReady) {
        props = state.source[data.type][data.id]
    }

    return (
        <>
            {data.isReady
                ? <main className='body'>
                    <SEO seo={props.acf.seo}  date={props.date_gmt} />
                    <TrustedBy acf={props.acf.hero} />
                    <PartnersType acf={props.acf.partners_type} />
                    <ProblemsResolving acf={props.acf.problem_resolving} />
                    <Generation acf={props.acf.generation} />
                    <Clients acf={props.acf.clients} />
                    <Features acf={props.acf.features} />
                    <Develope acf={props.acf.develope} />
                    <Awards marginDesktop='240px' marginTablet='260px' marginPhone='280px' acf={props.acf.awards} />
                    <Questions acf={props.acf.questions} />
                </main>
                : null
            }
        </>
    )
}

export default connect(CallCenter)