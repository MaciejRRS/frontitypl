import React from 'react'
import MoreEffective from '../../../../common/bricks/MoreEffective'
import BestAlternative from '../../../../common/bricks/BestAlternative'
import TopReasons from '../../../../common/bricks/TopReasons'
import ConversationRate from '../../../../common/bricks/ConversationRate'
import MoreIntegrations from '../../../../common/bricks/MoreIntegrations'
import OtherFeatures from '../../../../common/bricks/OtherFeatures'
import Develope from '../../../../common/bricks/Develope'
import Awards from '../../../../common/bricks/Awards'
import SEO from '../../../../common/SEO/seo'
import { connect } from "frontity"

const Livecall = ({ state }) => {

    let props
    const data = state.source.get(state.router.link)
    if (data.isReady) {
        props = state.source[data.type][data.id]
    }
    
    return (
        <>
            {data.isReady
                ? <main className='body'>
                    <SEO seo={props.acf.seo}  date={props.date_gmt} />
                    <MoreEffective acf={props.acf.hero} />
                    <BestAlternative acf={props.acf.best_alternative} />
                    <TopReasons acf={props.acf.top_reasons} />
                    <ConversationRate acf={props.acf.conversation_rate} />
                    <MoreIntegrations acf={props.acf.more_integrations} />
                    <OtherFeatures acf={props.acf.other_features} />
                    <Develope acf={props.acf.develope} />
                    <Awards marginDesktop='240px' marginTablet='260px' marginMobile='280px' acf={props.acf.awards} />
                </main>
                : null
            }
        </>
    )
}

export default connect(Livecall)