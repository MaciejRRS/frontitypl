import React from 'react'
import CustomerMotivation from '../../../../common/bricks/CustomerMotivation'
import Develope from '../../../../common/bricks/Develope'
import Awards from '../../../../common/bricks/Awards'
import Dominating from '../../../../common/bricks/Dominating'
import Features from '../../../../common/bricks/Features'
import TrustedBy from '../../../../common/bricks/TrustedBy'
import Generation from '../../../../common/bricks/Generation'
import Mixes from '../../../../common/bricks/Mixes'
import Questions from '../../../../common/bricks/Questions'
import VoiceChat from '../../../../common/bricks//VoiceChat'
import SEO from '../../../../common/SEO/seo'
import { connect } from "frontity"

const Livechats = ({ state }) => {

    let props
    const data = state.source.get(state.router.link)
    if (data.isReady) {
        props = state.source[data.type][data.id]
    }
    
    return (
        <>
            {data.isReady
                ? <main className='body'>
                    <SEO seo={props.acf.seo}  date={props.date_gmt} />
                    <TrustedBy acf={props.acf.hero} />
                    <Generation acf={props.acf.generation} />
                    <VoiceChat acf={props.acf.voice_chat} />
                    <CustomerMotivation acf={props.acf.customer_motivation} />
                    <Dominating acf={props.acf.dominating_channel} />
                    <Mixes acf={props.acf.mix_of_chat} />
                    <Features acf={props.acf.features} />
                    <Develope acf={props.acf.develope} />
                    <Awards marginDesktop='240px' marginTablet='260px' marginPhone='280px' acf={props.acf.awards} />
                    <Questions acf={props.acf.questions} />
                </main>
                : null
            }
        </>
    )
}

const mapStateToProps = (state) => {
    return {
        acf: state.wpAcfReducer.pages[10].acf // vsLivechats
    }
}

export default connect(Livechats)