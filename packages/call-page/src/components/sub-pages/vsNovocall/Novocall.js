import React from 'react'
import MoreEffective from '../../../../common/bricks/MoreEffective'
import ReceiveCalls from './components/ReceiveCalls'
import TopReasons from '../../../../common/bricks/TopReasons'
import ConversationRate from '../../../../common/bricks/ConversationRate'
import OtherFeatures from '../../../../common/bricks/OtherFeatures'
import Develope from '../../../../common/bricks/Develope'
import SEO from '../../../../common/SEO/seo'
import { connect } from "frontity"

const Novocall = ({ state }) => {

    let props
    const data = state.source.get(state.router.link)
    if (data.isReady) {
        props = state.source[data.type][data.id]
    }

    return (
        <>
            {data.isReady
                ? <main className='body'>
                    <SEO seo={props.acf.seo}  date={props.date_gmt} />
                    <MoreEffective acf={props.acf.hero} />
                    <TopReasons acf={props.acf.top_reasons} />
                    <ReceiveCalls acf={props.acf.receive_calls} />
                    <ConversationRate acf={props.acf.conversation_rate} />
                    <OtherFeatures acf={props.acf.other_features} />
                    <Develope marginBottomTablet='280px' marginBottomPhone='220px' acf={props.acf.develope_kopia} />
                </main>
                : null
            }
        </>
    )
}

export default connect(Novocall)