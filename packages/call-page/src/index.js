import Root from "./components"
import image from "@frontity/html2react/processors/image";
import link from "@frontity/html2react/processors/link";

const acfOptionsHandler = {
  pattern: "acf-options-page",
  func: async ({ route, state, libraries }) => {
    const response = await libraries.source.api.get({
      endpoint: `/acf/v3/options/options`
    });
    const option = await response.json();

    const data = state.source.get(route);
    Object.assign(data, { ...option, isAcfOptionsPage: true });
  }
}

const categoriesHandler = {
  pattern: "categories",
  func: async ({ route, state, libraries }) => {
    const response = await libraries.source.api.get({
      endpoint: `/wp/v2/category_blog?per_page=99`
    });
    const items = await response.json();

    const data = state.source.get(route);
    Object.assign(data, { items, isCategories: true });
  }
}

export default {
  name: "call-page",
  roots: {
    theme: Root
  },
  state: {
    theme: {},
    source: {
      params: {
      },
    },
    wpSource: {
      postsPage: '/blog',
    }
  },
  actions: {
    theme: {
      beforeSSR: async ({ state, actions }) => {
        await Promise.all([
          actions.source.fetch("/footer/"),
          actions.source.fetch("/header/"),
          actions.source.fetch('/strona-glowna/'),
          actions.source.fetch('/blog-page/'),
          actions.source.fetch('/casestudies/'),
          actions.source.fetch('/case-studies/'),
          actions.source.fetch('/campaigns/'),
          actions.source.fetch('/campaigns-page/'),
          actions.source.fetch('/ebooks-page/'),
          actions.source.fetch('/branze-page/'),
          actions.source.fetch('/blog/'),
          actions.source.fetch("acf-options-page"),
          actions.source.fetch("categories")
        ]);
      }
    }
  },
  libraries: {
    html2react: {
      processors: [image, link]
    },
    source: {
      handlers: [acfOptionsHandler, categoriesHandler]
    }
  }
}

